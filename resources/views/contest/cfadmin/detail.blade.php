@extends('.layouts.cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $contest->name }}
                <small>Edit Contest</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/contest') }}">Contest</a></li>
                <li class="active">{{ $contest->name }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>ขออภัย!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contest Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'PUT' , 'action' => ['ContestController@update', $contest],'id' => 'form-addmusic']) !!}
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('name') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Name <span style="color: red">*required</span></label>
                                        {!! Form::text('name',old('name', $contest->name),array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('filepath') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Poster Image (w:264 x h:373) <span style="color: red">*required</span></label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                                    <i class="fa fa-picture-o"></i> Choose
                                                </a>
                                            </span>
                                            {!! Form::text('filepath',old('filepath',$contest->original_image),array('class' => 'form-control','id'=>'thumbnail')) !!}
                                        </div>
                                        </div>
                                        {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('start') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Start Date <span style="color: red">*required</span></label>
                                        <input class="form-control" name="start" id="lottostart" value="{{ old('start', \Carbon\Carbon::parse($contest->start)->format('m/d/Y')) }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('end') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">End Date <span style="color: red">*required</span></label>
                                        <input class="form-control" name="end" id="eventenddate"value="{{ old('end', \Carbon\Carbon::parse($contest->end)->format('m/d/Y')) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label >Quota</label>
                                        <input class="form-control" name="quota" type="number" value="{{ old('quota', $contest->quota) }}">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('reward') ? ' has-error' : '' )}}">
                                        <label >Reward <span style="color: red">*required</span></label>
                                        <input class="form-control" name="reward" type="text" value="{{ old('reward', $contest->reward) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" value="0" name="link_req">
                                        <label >Link field name <input type="checkbox" name="link_req" value="1" {{ (old('link_req',$contest->link_req)) ? 'checked' : '' }}> Required?</label>
                                        <input class="form-control" name="link_name" type="text" value="{{ old('link_name', $contest->link_name) }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="hidden" value="0" name="file_req">
                                        <label >File field name <input type="checkbox" name="file_req" value="1" {{ (old('file_req',$contest->file_req)) ? 'checked' : '' }}> Required?</label>
                                        <input class="form-control" name="file_name" type="text" value="{{ old('file_name', $contest->file_name) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" value="0" name="content_req">
                                        <label >Detail field name <input type="checkbox" name="content_req" value="1" {{ (old('content_req',$contest->content_req)) ? 'checked' : '' }}> Required?</label>
                                        <input class="form-control" name="content_name" type="text" value="{{ old('content_name', $contest->content_name) }}">
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{($errors->has('content') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Description <span style="color: red">*required</span></label>
                                        <div {{($errors->has('content') ? 'class=ckeditor-error' : '' )}}>
                                        <textarea id="my-editor" name="content"
                                                  class="form-control">{!! old('content', $contest->content) !!}</textarea>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Remark</label>
                                        <textarea name="remark" class="form-control">{!! old('remark', $contest->remark) !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Tags (use Tab button or " , ")</label><br>
                                        <input id="tags" name="tags" class="form-control" type="text" data-role="tagsinput" value="{{ old('tags', implode(',',$contest->tags()->pluck('name')->all())) }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input id="status" name="status" type="hidden" value="1">
                            <button type="submit" class="btn btn-success" id="btn-publish">Publish</button>
                            @if($contest->status == 2)
                            <button type="button" class="btn btn-warning" id="btn-draft" style="margin-left: 5px;">Draft</button>
                            @endif
                            <a href="{{ url('cfadmin/contest') }}">
                                <button type="button" class="btn btn-default pull-right">Cancel</button>
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')

    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- page script -->
    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            // filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            //filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removeButtons: 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Flash,Anchor,Language,BidiLtr,BidiRtl,ShowBlocks',
            contentsCss : [ '{{ asset('css/bootstrap.css') }}', '{{ asset('style.css') }}' ],
            height:300,
        };

        $(document).ready(function() {
            $('#btn-draft').click(function () {
                $('#status').val(2);
                $(this).prop("disabled", true);
                $('#btn-publish').prop("disabled", true);
                $('#form-addmusic').submit();
            });

            $('#btn-publish').click(function () {
                $(this).prop("disabled", true);
                $('#btn-draft').prop("disabled", true);
                $('#form-addmusic').submit();
            });
            // If you want inserted images in a CKEditor to be responsive
// you can use the following code. It creates a htmlfilter for the
// image tag that replaces inline "width" and "style" definitions with
// their corresponding attributes and add's (in this example) the
// Bootstrap "img-responsive" class.
            CKEDITOR.on('instanceReady', function (ev) {
                ev.editor.dataProcessor.htmlFilter.addRules( {
                    elements : {
                        img: function( el ) {
                            // Add bootstrap "img-responsive" class to each inserted image
                            el.addClass('img-responsive');

                            // Remove inline "height" and "width" styles and
                            // replace them with their attribute counterparts.
                            // This ensures that the 'img-responsive' class works
                            var style = el.attributes.style;

                            if (style) {
                                // Get the width from the style.
                                var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
                                    width = match && match[1];

                                // Get the height from the style.
                                match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                                var height = match && match[1];

                                // Replace the width
                                if (width) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, '');
                                    // el.attributes.width = width;
                                }

                                // Replace the height
                                if (height) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, 'max-height:'+height+'px;');
                                    // el.attributes.height = height;
                                }
                            }

                            // Remove the style tag if it is empty
                            if (!el.attributes.style)
                                delete el.attributes.style;
                        }
                    }
                });
            });
            CKEDITOR.replace('my-editor', options);
            $('#lfm').filemanager('image');

            $('.bootstrap-tagsinput input').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#tags").tagsinput({
                cancelConfirmKeysOnEmpty: true,
                confirmKeys: [13],
            });

            $("#tags").parent().find('input').on('keydown',function (e) {

                if (e.which == 9)
                {
                    $("#tags").tagsinput('add',$(this).val());
                    $(this).val("");
                    e.preventDefault();
                }

            });

            $('#lottostart').datepicker({
                autoclose: true,
                zIndexOffset: 1050
            })

            $('#eventenddate').datepicker({
                autoclose: true,
                zIndexOffset: 1050
            })
        });

    </script>
@endsection
