<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use \Spatie\Tags\HasTags;
    //
    protected $table = 'events';

    protected $guarded = [];

    public function getTicketURLAttribute()
    {
        $url = strpos($this->ticket, 'http') !== 0 ? "http://$this->ticket" : $this->ticket;

        if(filter_var($url, FILTER_VALIDATE_URL)) {
            //valid
            return $url;
        } else {
            //not valid
            return abort(404);
        }
    }
    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Published';
        }
        elseif($this->status == 2){
            return 'Draft';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Published</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Draft</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public static function lastOrderFeatured()
    {
        $event = (new static)::where('featured_flag' , 1)->orderBy('featured_order' , 'desc')->first();

        if(empty($event)){
            return 0;
        }
        else{
            return $event->featured_order;
        }

    }

    public function club()
    {
        return $this->belongsTo('App\Models\Club');
    }
}
