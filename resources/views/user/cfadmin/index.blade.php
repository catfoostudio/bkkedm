@extends('layouts.cfadmin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: auto">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
            <small>Manage Users</small>
            <a class="btn btn-app" href="{{ url('cfadmin/user/create') }}">
                <i class="fa fa-plus"></i> Add
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
            {{--<li><a href="#">โรงแรม</a></li>--}}
            <li class="active">Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">User list</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>ID</th>
                                <th>Status</th>
                                <th>Role</th>
                                <th>Name</th>
                                <th>Updated date</th>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <th>ID</th>
                                <th>Status</th>
                                <th>Role</th>
                                <th>Name</th>
                                <th>Updated date</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Action</th>
                                <th>ID</th>
                                <th>Status</th>
                                <th>Role</th>
                                <th>Name</th>
                                <th>Updated date</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('cfadmin/user/'.$user->id) }}"><button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button></a>
                                            @if($user->status == 1)
                                                {!! Form::open(['method' => 'DELETE' , 'action' => ['LifestyleController@destroy' , $user] , 'style' => 'display:inline', 'id' => 'post_'.$user->id]) !!}
                                                {!! Form::hidden('post_id' , $user->id) !!}
                                                <button type="button" class="btn btn-danger btn-flat btn-delete-post" data-postid="{{ $user->id }}"><i class="fa fa-trash"></i></button>
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['method' => 'POST' , 'action' => ['LifestyleController@active' , $user] , 'style' => 'display:inline', 'id' => 'post_'.$user->id]) !!}
                                                {!! Form::hidden('post_id' , $user->id) !!}
                                                <button type="button" class="btn btn-success btn-flat btn-active-post" data-postid="{{ $user->id }}"><i class="fa fa-check"></i></button>
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ $user->id }}</td>
                                    <td>{!! $user->statusBadge !!}</td>
                                    <td>{!! $user->rolesBadge !!}</td>
                                    <td style="width: 30%;">{{ $user->name }}</td>
                                    <td style="width: 100px;">{{ $user->updated_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="delete-modal">
    <div class="modal modal-danger" id="confirm-delete-post-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Danger Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-outline pull-left" id="confirm-delete">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.delete-modal -->

<div class="active-modal">
    <div class="modal modal-success" id="confirm-active-post-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Active Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-outline pull-left" id="confirm-active">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.delete-modal -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>

    <script>
        $(function () {

            $('.btn-delete-post').on('click', function () {
                $('#confirm-delete-post-modal').modal('show');
                $('#confirm-delete-post-modal').find('.modal-title').text('Warning!');
                $('#confirm-delete-post-modal').find('.modal-body').text('Are you sure you want to delete this record?');

                var form = $(this).parent();
                $hotel_id = $(this).data('postid');
                $('#confirm-delete').on('click' , function () {
                    form.submit();
                });
            });

            $('.btn-active-post').on('click', function () {
                $('#confirm-active-post-modal').modal('show');
                $('#confirm-active-post-modal').find('.modal-title').text('Warning!');
                $('#confirm-active-post-modal').find('.modal-body').text('Are you sure you want to active this record?');

                var form = $(this).parent();
                $hotel_id = $(this).data('postid');
                $('#confirm-active').on('click' , function () {
                    form.submit();
                });
            });


            // Setup - add a text input to each header cell
            $('#example1 thead tr:eq(1) th').each( function () {
                var title = $('#example1 thead tr:eq(0) th').eq( $(this).index() ).text();
                if(title == 'ID')
                    $(this).html( '<input type="text" placeholder="'+title+'" style="width: 50px;" />' );
                else
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#example1').DataTable({
                orderCellsTop: true,

                "bScrollCollapse": true,
                "pageLength": 50,
                "columnDefs": [
                    { "width": "5px", "targets": 0 },
                    {
                        "width": "5%",
                        "targets": [ 1 ],
                        "visible": true
                    },
                    { "width": "50px", targets: 2 },
                    { "width": "10%", targets: 3 },
                ],
                "order": [[ 4, "desc" ]],
            });

            // Apply the search
            table.columns().every(function (index) {
                $('#example1_wrapper thead tr:eq(1) th:eq(' + index + ') input').on('keyup change', function () {

                    table.column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
                });
            });

            $('#example1 tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#example1 tbody').on('dblclick', 'tr', function () {

                var data = table.row( this ).data();
                window.location = '{{ url('cfadmin/user/') }}/'+data[1];
//                alert( 'You clicked on '+data[0]+'\'s row' );
            } );

        });
    </script>

@endsection
