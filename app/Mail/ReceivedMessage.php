<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReceivedMessage extends Mailable
{
    use Queueable, SerializesModels;

    protected $input;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Contact - '.$this->input['template-contactform-subject'])
            ->view('contact.sendmessage')
            ->with([
            'name' => $this->input['template-contactform-name'],
            'email' => $this->input['template-contactform-email'],
            'subject' => $this->input['template-contactform-subject'],
            'content' => $this->input['template-contactform-message'],
        ]);
    }
}
