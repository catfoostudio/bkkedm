@foreach($votes as $vote)
    <div class="entry clearfix">
        <div class="entry-image" style="margin-bottom: 10px;">
            <a href="{{ url('vote/'.$vote->id) }}">
                <img class="image_fade" style="object-fit: cover"
                                                          src="{{ asset('images/votes/'.$vote->thumbnail_image) }}" alt="Image"></a>
        </div>
        <div class="entry-title" style="height: 51px;overflow: hidden;">
            <h2><a href="{{ url('vote/'.$vote->id) }}">{{$vote->name}}</a></h2>
        </div>
        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;">
            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($vote->start)->format('dS M Y') }}</li>
            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($vote->end)->format('dS M Y') }}</li>
        </ul>
    </div>
@endforeach