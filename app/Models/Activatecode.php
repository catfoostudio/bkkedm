<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Activatecode extends Model
{

    protected $table = 'activatecodes';

    protected $guarded = [];
    //
    function gambler()
    {
        return $this->belongsTo('App\Models\Gambler');
    }

    public function prize()
    {
        return $this->belongsTo('App\Models\Prize');
    }

    public function generatecode()
    {
        $maxDuplicate = 5;

        $code = $this->randomCouponString();

        $validator = Validator::make(['code'=> $code],[
            'code' => 'required|unique:activatecodes,code|max:19'
        ]);

        $i = 0;
        while($validator->fails()) {

            if($i == $maxDuplicate)
            {
                throw new \Exception('test');
            }
            else{
                $code = $this->randomCouponString();
                $validator = Validator::make(['code'=> $code],[
                    'code' => 'required|unique:activatecodes,code|max:19'
                ]);
            }

            $i++;

        }

        $this->code = $code;
    }

    public function randomCouponString()
    {
        $input = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $strength = 16;

        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[random_int(0, $input_length - 1)];

            if($i%4 == 0 && $i != 0)
            {
                $random_string .= '-';
            }

            $random_string .= $random_character;

        }
        return $random_string;
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Available';
        }
        elseif($this->status == 2){
            return 'Redeemed';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Available</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Redeemed</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public function getStatusFrontendBadgeAttribute(){

        if($this->status == 1){
            return '<span class="badge badge-success">Available</span>';
        }
        elseif($this->status == 2){
            return '<span class="badge badge-warning">Redeemed</span>';
        }
        elseif($this->status == 3){
            return '<span class="badge badge-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="badge badge-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="badge badge-danger">Inactive</span>';
        }
        else{
            return '<span class="badge badge-danger">Error Status</span>';
        }
    }
}
