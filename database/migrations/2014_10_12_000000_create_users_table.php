<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->string('avatar')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->text('quote')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('line_id')->nullable();
            $table->string('tel')->nullable();
            $table->string('mobile')->nullable();
            $table->text('remark')->nullable();
            $table->string('original_image')->nullable();
            $table->string('activate_code')->nullable();
            $table->tinyInteger('gender')->default(1);
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->dateTime('birthday');
            $table->integer('club_id')->unsigned()->nullable();
            $table->integer('event_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
