@extends('.layouts.cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $post->name }}
                <small>Edit Video</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/video') }}">Videos</a></li>
                <li class="active">{{ $post->name }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            {{--@if (count($errors) > 0)--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>ขออภัย!</strong><br><br>--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        {!! Form::open(['method' => 'PUT' , 'action' => ['VideoController@update' , $post],'id' => 'form-editmusic']) !!}
                        <div class="box-header with-border">
                            <input type="hidden" name="topstory_flag" value="0">
                            <h3 class="box-title">Video Info.</h3>
                            <input style="margin-left: 20px;" id="topstory_flag" type="checkbox" name="topstory_flag" value="1" {{ old('topstory_flag',$post->topstory_flag) == 1  ? 'checked' : '' }}> <label for="topstory_flag">Top Video</label>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('name') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Name <span style="color: orangered">*required</span></label>
                                        {!! Form::text('name',old('name',$post->name),array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('youtube_id') ? ' has-error' : '' )}}">
                                            <label for="inputEmail3">Youtube ID <span style="color: orangered">*required</span></label>
                                            {!! Form::text('youtube_id',old('youtube_id',$post->youtube_id),array('class' => 'form-control')) !!}
                                        </div>
                                        {{--<div class="form-group{{($errors->has('filepath') ? ' has-error' : '' )}}">--}}
                                        {{--<label for="inputEmail3">Thumbnail Image (w:845 x h:560) <span style="color: orangered">*required</span></label>--}}
                                        {{--<div class="input-group">--}}
                                            {{--<span class="input-group-btn">--}}
                                                {{--<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">--}}
                                                    {{--<i class="fa fa-picture-o"></i> Choose--}}
                                                {{--</a>--}}
                                            {{--</span>--}}
                                            {{--{!! Form::text('filepath',old('filepath' , $post->original_image),array('class' => 'form-control','id'=>'thumbnail')) !!}--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('artist') ? ' has-error' : '' )}}">
                                            <label for="inputEmail3">Artist <span style="color: orangered">*required</span></label>
                                            {!! Form::text('artist',old('artist',$post->url),array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Category</label>
                                        <select class="form-control" name="category">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ ($category->id == old('category',$post->category_id)) ? 'selected=selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group{{($errors->has('content') ? ' has-error' : '' )}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Description <span style="color: orangered">*required</span></label>
                                        <div {{($errors->has('content') ? 'class=ckeditor-error' : '' )}}>
                                        <textarea id="my-editor" name="content"
                                                  class="form-control">{!! old('content', old('content',$post->description)) !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<label style="margin-right: 5px">Embed type:</label>--}}
                                        {{--<input type="radio" value="none" name="type" {{ old('type',$post->embedType) == 'none' ? 'checked' : 'checked' }} id="type_none"> <label for="type_none" style="margin-right: 10px;">None</label>--}}
                                        {{--<input type="radio" value="reward" name="type" {{ old('type',$post->embedType) == 'reward' ? 'checked' : '' }} id="type_reward"> <label for="type_reward" style="margin-right: 10px;">Reward</label>--}}
                                        {{--<input type="radio" value="vote" name="type" {{ old('type',$post->embedType) == 'vote' ? 'checked' : '' }} id="type_vote"> <label for="type_vote" style="margin-right: 10px;">Vote</label>--}}
                                        {{--<input type="radio" value="contest" name="type" {{ old('type',$post->embedType) == 'contest' ? 'checked' : '' }} id="type_contest"> <label for="type_contest">Contest</label>--}}
                                        {{--<br><label><span style="color: orangered">*Dropdown required. if you embed</span></label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<label >Embed list (ID:Name)</label>--}}
                                        {{--<div class="form-group{{($errors->has('embed_id') ? ' has-error' : '' )}}">--}}
                                        {{--<select class="form-control select2"--}}
                                                {{--style="width: 100%;{{ old('type',$post->embedType) == 'reward' ? '' : 'display: none'}}"--}}
                                                {{--id="reward"--}}
                                                {{--name="embed_id"--}}
                                                {{--{{ old('type', $post->embedType) == 'reward' ? '' : 'disabled=disabled' }} >--}}
                                            {{--<option value="none" selected="selected">None</option>--}}
                                            {{--@foreach($rewards as $reward)--}}
                                                {{--<option value="{{$reward->id}}" {{ old('embed_id',$post->prize_id) == $reward->id && old('type', $post->embedType) == 'reward' ? 'selected=selected' : '' }}>{{ $reward->id.' : '.$reward->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<select class="form-control select2"--}}
                                                {{--style="width: 100%;{{ old('type',$post->embedType) == 'vote' ? '' : 'display: none'}}"--}}
                                                {{--id="vote"--}}
                                                {{--name="embed_id"--}}
                                                {{--{{ old('type', $post->embedType) == 'vote' ? '' : 'disabled=disabled' }} >--}}
                                            {{--<option value="none" selected="selected">None</option>--}}
                                            {{--@foreach($votes as $vote)--}}
                                                {{--<option value="{{$vote->id}}" {{ old('embed_id',$post->vote_id) == $vote->id && old('type',$post->embedType) == 'vote' ? 'selected=selected' : '' }}>{{ $vote->id.' : '.$vote->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<select class="form-control select2"--}}
                                                {{--style="width: 100%;{{ old('type',$post->embedType) == 'contest' ? '' : 'display: none'}}"--}}
                                                {{--id="contest"--}}
                                                {{--name="embed_id"--}}
                                                {{--{{ old('type', $post->embedType) == 'contest' ? '' : 'disabled=disabled' }} >--}}
                                            {{--<option value="none" selected="selected">None</option>--}}
                                            {{--@foreach($contests as $contest)--}}
                                                {{--<option value="{{$contest->id}}" {{ old('embed_id',$post->contest_id) == $contest->id && old('type',$post->embedType) == 'contest' ? 'selected=selected' : '' }}>{{ $contest->id.' : '.$contest->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Tags (use Tab button or " , ")</label><br>
                                        <input id="tags" name="tags" class="form-control" type="text" value="{{ old('tags',implode(',',$post->tags()->pluck('name')->all())) }}" data-role="tagsinput"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input id="status" name="status" type="hidden" value="1">
                            <button type="submit" class="btn btn-success" id="btn-publish">Publish</button>
                            @if(empty($post->published_at))
                                <button type="button" class="btn btn-warning" id="btn-draft" style="margin-left: 5px;">Draft</button>
                            @endif
                            <a href="{{ url('cfadmin/music') }}">
                                <button type="button" class="btn btn-default pull-right">Cancel</button>
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- page script -->
    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            // filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            //filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removeButtons: 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Flash,Anchor,Language,BidiLtr,BidiRtl,ShowBlocks',
            contentsCss : [ '{{ asset('css/bootstrap.css') }}', '{{ asset('style.css') }}' ],
            height:300,
        };

        $(document).ready(function() {
            $('#btn-draft').click(function () {
                $('#status').val(2);
                $(this).prop("disabled", true);
                $('#btn-publish').prop("disabled", true);
                $('#form-editmusic').submit();
            });

            $('#btn-publish').click(function () {
                $(this).prop("disabled", true);
                $('#btn-draft').prop("disabled", true);
                $('#form-editmusic').submit();
            });

            // If you want inserted images in a CKEditor to be responsive
// you can use the following code. It creates a htmlfilter for the
// image tag that replaces inline "width" and "style" definitions with
// their corresponding attributes and add's (in this example) the
// Bootstrap "img-responsive" class.
            CKEDITOR.on('instanceReady', function (ev) {
                ev.editor.dataProcessor.htmlFilter.addRules( {
                    elements : {
                        img: function( el ) {
                            // Add bootstrap "img-responsive" class to each inserted image
                            el.addClass('img-responsive');

                            // Remove inline "height" and "width" styles and
                            // replace them with their attribute counterparts.
                            // This ensures that the 'img-responsive' class works
                            var style = el.attributes.style;

                            if (style) {
                                // Get the width from the style.
                                var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
                                    width = match && match[1];

                                // Get the height from the style.
                                match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                                var height = match && match[1];

                                // Replace the width
                                if (width) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, '');
                                    // el.attributes.width = width;
                                }

                                // Replace the height
                                if (height) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, 'max-height:'+height+'px;');
                                    // el.attributes.height = height;
                                }
                            }

                            // Remove the style tag if it is empty
                            if (!el.attributes.style)
                                delete el.attributes.style;
                        }
                    }
                });
            });
            CKEDITOR.replace('my-editor', options);
            $('#lfm').filemanager('image');

            $('.bootstrap-tagsinput input').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#tags").tagsinput({
                cancelConfirmKeysOnEmpty: true,
                confirmKeys: [13],
            });

            $("#tags").parent().find('input').on('keydown',function (e) {

                if (e.which == 9)
                {
                    $("#tags").tagsinput('add',$(this).val());
                    $(this).val("");
                    e.preventDefault();
                }

            });

            $('input[name=type]').change(function ()
            {
                if($('input[name=type]:checked').val() == 'reward')
                {
                    $('#reward').css('display','block');
                    $('#reward').attr('disabled',false);
                    $('#vote').css('display','none');
                    $('#contest').css('display','none');
                    $('#contest').attr('disabled',true);
                    $('#vote').attr('disabled',true);
                    $('#contest').attr('disabled',true);
                    $('#vote').val('none').change();
                    $('#contest').val('none').change();
                }
                else if($('input[name=type]:checked').val() == 'vote')
                {
                    $('#vote').css('display','block');
                    $('#vote').attr('disabled',false);
                    $('#reward').css('display','none');
                    $('#contest').css('display','none');
                    $('#reward').attr('disabled',true);
                    $('#contest').attr('disabled',true);
                    $('#reward').val('none').change();
                    $('#contest').val('none').change();

                }
                else if(($('input[name=type]:checked').val() == 'contest'))
                {
                    $('#contest').css('display','block');
                    $('#contest').attr('disabled',false);
                    $('#vote').css('display','none');
                    $('#reward').css('display','none');
                    $('#vote').attr('disabled',true);
                    $('#reward').attr('disabled',true);
                    $('#vote').val('none').change();
                    $('#reward').val('none').change();
                }
                else
                {
                    $('#reward').css('display','none');
                    $('#vote').css('display','none');
                    $('#contest').css('display','none');
                    $('#contest').attr('disabled',true);
                    $('#vote').attr('disabled',true);
                    $('#contest').attr('disabled',true);
                    $('#reward').attr('disabled',true);
                    $('#vote').val('none').change();
                    $('#contest').val('none').change();
                    $('#reward').val('none').change();
                }
            });
        });

    </script>
@endsection
