<div class="col-md-3 clearfix">
    <style>
        .sidebar-active{
            z-index:2;color: #292929;background-color: #f2f2f2;border: 1px solid rgba(0, 0, 0, 0.125);
        }
    </style>
    <div class="list-group">
        {{--<a href="#" class="list-group-item list-group-item-action clearfix">Profile <i class="icon-user float-right"></i></a>--}}
        {{--<a href="#" class="list-group-item list-group-item-action clearfix">Servers <i class="icon-laptop2 float-right"></i></a>--}}
        <a href="{{ url('member') }}" class="list-group-item list-group-item-action clearfix {{ Request::is('member') ? 'sidebar-active' : '' }}" >Messages <i class="icon-envelope2 float-right"></i></a>
        <a href="{{ url('member/reward') }}" class="list-group-item list-group-item-action clearfix {{ Request::is('member/reward') ? 'sidebar-active' : '' }}">Registered Reward <i class="icon-credit-cards float-right"></i></a>
        <a href="{{ url('redeem') }}" class="list-group-item list-group-item-action clearfix {{ Request::is('redeem') ? 'sidebar-active' : '' }}">Redeem <i class="icon-gift float-right"></i></a>
        <a href="{{ url('member/profile') }}" class="list-group-item list-group-item-action clearfix {{ Request::is('member/profile') ? 'sidebar-active' : '' }}">Settings <i class="icon-cog float-right"></i></a>
        {{--<a href="{{ url('member/profile') }}" class="list-group-item list-group-item-action clearfix {{ Request::is('member/follow') ? 'sidebar-active' : '' }}">Following <i class="icon-bell float-right"></i></a>--}}
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="list-group-item list-group-item-action clearfix">Logout <i class="icon-line2-logout float-right"></i></a>
    </div>

    {{--<div class="fancy-title topmargin title-border">--}}
        {{--<h4>About Me</h4>--}}
    {{--</div>--}}

    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum laboriosam, dignissimos veniam obcaecati. Quasi eaque, odio assumenda porro explicabo laborum!</p>--}}

    {{--<div class="fancy-title topmargin title-border">--}}
        {{--<h4>Social Profiles</h4>--}}
    {{--</div>--}}

    {{--<a href="#" class="social-icon si-facebook si-small si-rounded si-light" title="Facebook">--}}
        {{--<i class="icon-facebook"></i>--}}
        {{--<i class="icon-facebook"></i>--}}
    {{--</a>--}}

    {{--<a href="#" class="social-icon si-gplus si-small si-rounded si-light" title="Google+">--}}
        {{--<i class="icon-gplus"></i>--}}
        {{--<i class="icon-gplus"></i>--}}
    {{--</a>--}}

    {{--<a href="#" class="social-icon si-dribbble si-small si-rounded si-light" title="Dribbble">--}}
        {{--<i class="icon-dribbble"></i>--}}
        {{--<i class="icon-dribbble"></i>--}}
    {{--</a>--}}

    {{--<a href="#" class="social-icon si-flickr si-small si-rounded si-light" title="Flickr">--}}
        {{--<i class="icon-flickr"></i>--}}
        {{--<i class="icon-flickr"></i>--}}
    {{--</a>--}}

    {{--<a href="#" class="social-icon si-linkedin si-small si-rounded si-light" title="LinkedIn">--}}
        {{--<i class="icon-linkedin"></i>--}}
        {{--<i class="icon-linkedin"></i>--}}
    {{--</a>--}}

    {{--<a href="#" class="social-icon si-twitter si-small si-rounded si-light" title="Twitter">--}}
        {{--<i class="icon-twitter"></i>--}}
        {{--<i class="icon-twitter"></i>--}}
    {{--</a>--}}
</div>