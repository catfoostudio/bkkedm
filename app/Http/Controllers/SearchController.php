<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function search(Request $request){
        $input = $request->input();
        $view = 'search.index';
        $allnewsmusic = Post::where('name','LIKE','%'.$input['key'].'%')->where('status', 1)->orderBy('updated_at','desc')->paginate(6);
        return view($view, [
            'allnewsmusic' => $allnewsmusic,
            'key' => $input['key'],
        ]);

    }

    public function searchajaxnews(Request $request)
    {
        $input = $request->input();

        $ajaxnews = Post::where('name','LIKE','%'.$input['key'].'%')->where('status', 1)->orderBy('updated_at','desc')->paginate(6);

        if ($request->ajax()) {

            $view = view('music.loadallnews', compact('ajaxnews'))->render();
            return response()->json(['html'=>$view]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
