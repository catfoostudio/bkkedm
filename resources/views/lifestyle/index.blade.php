@extends('layouts.main')
@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title" style="background: url('images/parallax/home/5.jpg');background-size: cover;">
        <div class="container clearfix" align="center">
            <h1 style="font-size: 40px;">Lifestyle</h1>
        </div>
    </section><!-- #page-title end -->
    @if(count($posts) >= 1)
        <!-- Content
    ============================================= -->
        <section id="content">

            <div class="content-wrap" style="padding-top:20px;padding-bottom: 0px;">

                <div class="container clearfix">

                    <!-- Posts
                    ============================================= -->
                    <div id="posts" class="post-grid grid-container grid-2 clearfix" data-layout="fitRows">
                        <div class="row">
                            @if(count($posts) >= 1)
                                <div class="col-md-6">
                                    <div class="entry-image" style="margin-bottom: 10px;">
                                        <a href="{{ url('lifestyle/'.$posts[0]->id.'/'.str_replace(' ','-',$posts[0]->name)) }}">
                                            <img class="image_fade"
                                                 src="{{ asset('images/posts/'.$posts[0]->thumbnail_image) }}"
                                                 alt="Standard Post with Image"></a>
                                    </div>
                                    <div class="entry-title" style="max-height: 60px;overflow: hidden;">
                                        <h2><a href="{{ url('lifestyle/'.$posts[0]->id.'/'.str_replace(' ','-',$posts[0]->name)) }}">{{ $posts[0]->name }}</a></h2>
                                    </div>
                                    <ul class="entry-meta clearfix" style="margin: 0px 0px 10px 0px;">
                                        <li>
                                            <i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($posts[0]->updated_at)->format('dS F Y') }}
                                        </li>
                                    </ul>
                                </div>
                                @if(count($posts) >= 2)
                                    <div class="col-md-6">
                                        <div class="entry-image" style="margin-bottom: 10px;">
                                            <a href="{{ url('lifestyle/'.$posts[1]->id.'/'.str_replace(' ','-',$posts[1]->name)) }}">
                                                <img class="image_fade"
                                                     src="{{ asset('images/posts/'.$posts[1]->thumbnail_image) }}"
                                                     alt="Standard Post with Image"></a>
                                        </div>
                                        <div class="entry-title" style="max-height: 60px;overflow: hidden;">
                                            <h2><a href="{{ url('lifestyle/'.$posts[1]->id.'/'.str_replace(' ','-',$posts[1]->name)) }}">{{ $posts[1]->name }}</a>
                                            </h2>
                                        </div>
                                        <ul class="entry-meta clearfix" style="margin: 0px 0px 10px 0px;">
                                            <li>
                                                <i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($posts[1]->updated_at)->format('dS F Y') }}
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endif
                        </div>

                    </div><!-- #posts end -->

                    <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
                        <div class="row" style="padding-bottom: 0px;">
                            @if(count($posts) >= 3)
                                <div class="col-md-4 col-xs-1">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-image" style="margin-bottom: 10px;">
                                                <a href="{{ url('lifestyle/'.$posts[2]->id.'/'.str_replace(' ','-',$posts[2]->name)) }}">
                                                    <img class="image_fade"
                                                         src="{{ asset('images/posts/'.$posts[2]->thumbnail_image) }}"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-title">
                                                <h2 style="height: auto">
                                                    <a href="{{ url('lifestyle/'.$posts[2]->id.'/'.str_replace(' ','-',$posts[2]->name)) }}">{{ $posts[2]->name }}</a>
                                                </h2>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="line" style="margin-top: 0px;margin-bottom: 10px;"></div>
                                </div>
                            @endif
                            @if(count($posts) >= 4)
                                <div class="col-md-4 col-xs-1">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-image" style="margin-bottom: 10px;">
                                                <a href="{{ url('lifestyle/'.$posts[3]->id.'/'.str_replace(' ','-',$posts[3]->name)) }}">
                                                    <img class="image_fade"
                                                         src="{{ asset('images/posts/'.$posts[3]->thumbnail_image) }}"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-title" >
                                                <h2 style="height: auto">
                                                    <a href="{{ url('lifestyle/'.$posts[3]->id.'/'.str_replace(' ','-',$posts[3]->name)) }}">{{ $posts[3]->name }}</a>
                                                </h2>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="line" style="margin-top: 0px;margin-bottom: 10px;"></div>
                                </div>
                            @endif
                            @if(count($posts) >= 5)
                                <div class="col-md-4 col-xs-1">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-image" style="margin-bottom: 10px;">
                                                <a href="{{ url('lifestyle/'.$posts[4]->id.'/'.str_replace(' ','-',$posts[4]->name)) }}">
                                                    <img class="image_fade"
                                                         src="{{ asset('images/posts/'.$posts[4]->thumbnail_image) }}"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-6">
                                            <div class="entry-title" >
                                                <h2 style="height: auto">
                                                    <a href="{{ url('lifestyle/'.$posts[4]->id.'/'.str_replace(' ','-',$posts[4]->name)) }}">{{ $posts[4]->name }}</a>
                                                </h2>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="line" style="margin-top: 0px;margin-bottom: 10px;"></div>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </section><!-- #content end -->
    @endif
    @if(!empty(count($allnewsmusic)))
        <section style="position: relative;
    background-color: #F5F5F5;padding-top:20px">
            <div class="container clearfix">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" id="nav-allnews-tab" data-toggle="tab" href="#nav-allnews" role="tab"
                           aria-controls="nav-allnews" aria-selected="false">All Lifestyle</a>
                    </li>
                    @foreach(\App\Models\Category::where('type','lifestyle')->where('status',1)->get() as $category)
                        <li class="nav-item">
                            <a class="nav-link" id="nav-{{ $category->id }}-tab" data-toggle="tab"
                               href="#nav-{{ $category->id }}" role="tab" aria-controls="nav-{{ $category->id }}"
                               aria-selected="true">{{ $category->name }}</a>
                        </li>
                    @endforeach
                    {{--@if(!empty(count($newsmusic)))--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="nav-news-tab" data-toggle="tab" href="#nav-news" role="tab" aria-controls="nav-news" aria-selected="true">News</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                    {{--@if(!empty(count($featured)))--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="nav-featured-tab" data-toggle="tab" href="#nav-featured" role="tab" aria-controls="nav-featured" aria-selected="false">Featured</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                    {{--@if(!empty(count($reviews)))--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="nav-reviews-tab" data-toggle="tab" href="#nav-reviews" role="tab" aria-controls="nav-reviews" aria-selected="false">Reviews</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                    {{--@if(!empty(count($recommended)))--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" id="nav-recommended-tab" data-toggle="tab" href="#nav-recommended" role="tab" aria-controls="nav-recommended" aria-selected="false">Recommended Listening</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                </ul>
            </div>
        </section>
        <section style="position: relative;overflow: hidden;background-color: #FFF;padding-top: 20px;">
            <div class="container clearfix">

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-allnews" role="tabpanel"
                         aria-labelledby="nav-allnews-tab">
                        <div id="posts_cat_0" class="row">
                            @foreach($allnewsmusic as $news)
                                <div class="col-lg-4 col-md-6 col-xs-6 col-sm-6 col-6">
                                <div class="entry clearfix" >
                                    <div class="entry-image" style="margin-bottom:10px;">
                                        <a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}"><img class="image_fade"
                                                                                                                          src="{{ asset('images/posts/'.$news->thumbnail_image) }}"></a>
                                    </div>
                                    <div class="entry-title" style="overflow: hidden;margin-bottom: 10px;">
                                        <h2 style="height: auto;font-size: 18px;"><a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}">{{ $news->name }}</a></h2>
                                    </div>
                                </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row mb-3">
                            <div class="col-12" align="center">
                                <button class="button button-3d button-rounded button-black"
                                        onclick="loadMoreData(0);">Load more
                                </button>
                            </div>
                        </div>
                    </div>
                    @foreach(\App\Models\Category::where('type','lifestyle')->where('status',1)->get() as $category)
                        <div class="tab-pane fade" id="nav-{{ $category->id }}" role="tabpanel"
                             aria-labelledby="nav-{{ $category->id }}-tab">
                            <div id="posts_cat_{{$category->id}}" class="row" >
                                @foreach(\App\Models\Post::where('type','lifestyle')->where('category_id',$category->id)->where('status',1)->orderBy('updated_at','desc')->paginate(6) as $news)
                                    <div class="col-lg-4 col-md-6 col-xs-6 col-sm-6 col-6">
                                    <div class="entry clearfix">
                                        <div class="entry-image" style="margin-bottom:10px;">
                                            <a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}"><img class="image_fade"
                                                                                                                              src="{{ asset('images/posts/'.$news->thumbnail_image) }}"></a>
                                        </div>
                                        <div class="entry-title" style="margin-bottom: 10px;">
                                            <h2 style="height: auto;font-size: 18px;"><a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}">{{ $news->name }}</a></h2>
                                        </div>
                                    </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row mb-3">
                                <div class="col-12" align="center">
                                    <button class="button button-3d button-rounded button-black"
                                            onclick="loadMoreData({{$category->id}});">Load more
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>


                <!-- .pager end -->
            </div>
        </section>
    @endif
    @if(!empty(count($musichilights)))
        <div class="section" style="margin: 0px 0;padding: 20px 0;">
            <div class="container clearfix">
                <h3 style="margin-bottom: 0px;">Lifestyle Hilights</h3>
                <div id="oc-images2" class="owl-carousel image-carousel carousel-widget" data-margin="20"
                     data-pagi="false"
                     data-rewind="true" data-items-xs="2" data-items-sm="2" data-items-md="3" data-items-lg="4"
                     data-items-xl="5">
                    @foreach($musichilights as $musichilight)
                        <div class="oc-item">
                            <div class="ipost clearfix">
                                <div class="entry-image">
                                    <a href="{{ url('lifestyle/'.$musichilight->id.'/'.str_replace(' ','-',$musichilight->name)) }}"><img class="image_fade"
                                                                                                                                      src="{{ asset('images/posts/'.$musichilight->thumbnail_image) }}"></a>
                                </div>
                                <div class="entry-title">
                                    <h4><a href="{{ url('lifestyle/'.$musichilight->id.'/'.str_replace(' ','-',$musichilight->name)) }}">{{ $musichilight->name }}</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script>
        // $(document).ready(function () {
        // $('#myTab a[href="#profile"]').tab('show') // Select tab by name
        // $('#myTab li:first-child a').tab('show') // Select first tab
        // $('#myTab li:last-child a').tab('show') // Select last tab
        // $('#myTab li:nth-child(3) a').tab('show') // Select third tab

        var pageNumber_cat_0 = 2;

                @foreach(\App\Models\Category::where('type','lifestyle')->where('status',1)->get() as $category)
        var pageNumber_cat_{{$category->id}} = 2;
        @endforeach
        function getPage(cat) {
            if(cat == 0){
                return pageNumber_cat_0;
            }

            @foreach(\App\Models\Category::where('type','lifestyle')->where('status',1)->get() as $category)
            if(cat == '{{$category->id}}') {
                return pageNumber_cat_{{$category->id}};
            }
            @endforeach
        }

        function setPage(cat) {
            if(cat == 0){
                pageNumber_cat_0 += 1;
            }

            @foreach(\App\Models\Category::where('type','lifestyle')->where('status',1)->get() as $category)
            if(cat == '{{$category->id}}') {
                pageNumber_cat_{{$category->id}} += 1;
            }
            @endforeach
        }


        function loadMoreData(cat) {
            var html = "<div class=\"css3-spinner\" style='background-color: #ffffff85'><div class=\"css3-spinner-bounce1\"></div><div class=\"css3-spinner-bounce2\"></div><div class=\"css3-spinner-bounce3\"></div></div>";
            $(html).hide().appendTo("body").fadeIn(500);
            $.ajax({
                type: 'GET',
                url: "ajaxlifestyle?cat="+cat+"&page=" + getPage(cat),
                success: function (data) {
                    setPage(cat);
                    if (data.length == 0) {
                        // :( no more articles
                    } else {
                        $('#posts_cat_'+cat).append(data.html);
                    }
                    $('.css3-spinner').fadeOut(500);
                }, error: function (data) {
                    $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            })
        }

        var $loading = $('.css3-spinner').hide();
        //Attach the event handler to any element
        $(document)
            .ajaxStart(function () {
                //ajax request went so show the loading image
                $loading.show();
            })
            .ajaxStop(function () {
                //got response so hide the loading image
                $loading.hide();
            });
        // });
    </script>
@endsection