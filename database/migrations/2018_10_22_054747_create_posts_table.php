<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->string('name');
            $table->text('content');
            $table->string('thumbnail_image');
            $table->string('original_image');
            $table->string('type');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('topstory_flag')->default(0);
            $table->tinyInteger('topstory_order')->default(0);
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->text('remark')->nullable();
            $table->integer('views')->unsigned()->nullable();
            $table->integer('vote_id')->unsigned()->nullable();
            $table->integer('prize_id')->unsigned()->nullable();
            $table->integer('contest_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
