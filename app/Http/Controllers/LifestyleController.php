<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Choice;
use App\Models\Contest;
use App\Models\Contestant;
use App\Models\Hunter;
use App\Models\Post;
use App\Models\Prize;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class LifestyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->isAdminRequest()){
            $view = 'lifestyle.cfadmin.index';
            $posts = Post::where('type','lifestyle')->orderBy('created_at','desc')->get();
            return view($view, [
                'posts' => $posts,
            ]);
        }
        else{

            $view = 'lifestyle.index';
            $posts = Post::where('type','lifestyle')->where('status',1)->orderBy('published_at','desc')->take(5)->get();

            $allnewsmusic = Post::where('type','lifestyle')->where('status',1)->orderBy('published_at','desc')->paginate(6);
            $musichilights = Post::where('type','lifestyle')->where('status',1)->orderBy('published_at','desc')->take(5)->get();

            return view($view, [
                'posts' => $posts,
                'allnewsmusic' => $allnewsmusic,
                'musichilights' => $musichilights,
            ]);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where('type','lifestyle')->where('status',1)->get();
        return view('lifestyle.cfadmin.create', [
            'categories' => $categories,
            'topstoryfull' => (count(Post::where('type','lifestyle')->where('status',1)->where('topstory_flag',1)->get()) >= 6) ? true : false,
            'rewards' => Prize::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
            'votes' => Vote::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
            'contests' => Contest::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
//            'rewards' => Prize::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//            'votes' => Vote::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//            'contests' => Contest::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->input();
        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
        ];

        if ($input['type'] == 'reward') {
            $validate['embed_id'] = 'required|not_in:none';
        } elseif ($input['type'] == 'vote') {
            $validate['embed_id'] = 'required|not_in:none';
        } elseif ($input['type'] == 'contest') {
            $validate['embed_id'] = 'required|not_in:none';
        } else {

        }

        $this->validate($request, $validate);

        $lifestylenews = Post::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'category_id' => $input['category'],
            'thumbnail_image' => $input['filepath'],
            'original_image' => $input['filepath'],
            'status' => $input['status'],
            'type' => 'lifestyle',
            'topstory_flag' => (empty($input['topstory_flag'])) ? 0 : 1,
            'topstory_order' => (empty($input['topstory_flag'])) ? 0 : Post::lastOrderTopstory() + 1,
            'user_id' => Auth::user()->id,
        ]);

        if(!empty($input['tags'])){
            $lifestylenews->tags = explode(',', $input['tags']);
        }

        if ($input['type'] == 'reward') {
            $lifestylenews->prize_id = $input['embed_id'];
        } elseif ($input['type'] == 'vote') {
            $lifestylenews->vote_id = $input['embed_id'];
        } elseif ($input['type'] == 'contest') {
            $lifestylenews->contest_id = $input['embed_id'];
        }

        $path = str_replace(' ', '%20', $input['filepath']);
        $time = time();
        $img = Image::make(asset($path));
        $img->fit(845 , 560);
        $filename = $time.'_post_'.$lifestylenews->id.'.jpg';
        $filepath = public_path('images/posts').'/'.$filename;
        $img->save($filepath);
        $lifestylenews->thumbnail_image = $filename;
        if($lifestylenews->status == 1){
            $lifestylenews->published_at = Carbon::now();
        }
        $lifestylenews->save();


        return redirect('cfadmin/lifestyle')->with('alert-success', 'Lifestyle news #'.$lifestylenews->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if($this->isAdminRequest()){
            $view = 'lifestyle.cfadmin.detail';
            $categories = Category::where('type','lifestyle')->where('status',1)->get();
            return view($view, [
                'post' => $post,
                'categories' => $categories,
                'topstoryfull' => (count(Post::where('type','lifestyle')->where('status',1)->where('topstory_flag',1)->get()) >= 6) ? true : false,
                'rewards' => Prize::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
                'votes' => Vote::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
                'contests' => Contest::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
//                'rewards' => Prize::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//                'votes' => Vote::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//                'contests' => Contest::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
            ]);
        }
        else{

            if($post->status == 0){
                return abort(404);
            }

            if($post->type == 'lifestyle') {

                $post->views = $post->views + 1;
                $post->timestamps = false;
                $post->save();

                $view = 'lifestyle.detail';
                $mostPopularPosts = Post::where('status',1)->where('type','lifestyle')->where('id','!=' , $id)->orderBy('updated_at','desc')->take(5)->get();
                $relatednews = Post::withAnyTags($post->tags)->where('id','!=' , $id)->where('type','lifestyle')->where('status',1)->take(4)->get();

                if(!empty($post->vote_id)) {
                    //embed section
                    $vote = $post->vote;

                    $isPastVote = (Carbon::parse($vote->end)->endOfDay()->lt(Carbon::now()));
                    $isBeforeVote = (Carbon::parse($vote->start)->startOfDay()->gt(Carbon::now()));

                    if (Auth::check()) {

                        $userChooses = Choice::whereHas('users', function ($query) {
                            $query->where('users.id', Auth::user()->id);
                        })->where('vote_id', $vote->id)->get();

                        $isVoted = !empty(count($userChooses));
                    } else {
                        $isVoted = false;
                        $userChooses = null;
                    }

                    return view($view, [
                        'post' => $post,
                        'mostPopularPosts' => $mostPopularPosts,
                        'relatednews' => $relatednews,
                        //embed
                        'vote' => $vote,
                        'voted' => $isVoted,
                        'isPastVote' => $isPastVote,
                        'isBeforeVote' => $isBeforeVote,
                        'userChooses' => $userChooses,
                    ]);
                }
                elseif(!empty($post->prize_id)) {
                    if(Auth::check()){
                        $hunter = Hunter::where('user_id',Auth::user()->id)->where('prize_id',$post->prize_id)->first();
                    }
                    else{
                        $hunter = null;
                    }

                    $isPastPrize = (Carbon::parse($post->prize->end)->endOfDay()->lt(Carbon::now()));
                    $isBeforePrize = (Carbon::parse($post->prize->start)->startOfDay()->gt(Carbon::now()));
                    return view($view, [
                        'post' => $post,
                        'mostPopularPosts' => $mostPopularPosts,
                        'relatednews' => $relatednews,
                        'prize' => $post->prize,
                        'hunter' => $hunter,
                        'isPastPrize' => $isPastPrize,
                        'isBeforePrize' => $isBeforePrize,
                    ]);
                }
                elseif(!empty($post->contest_id)) {
                    $contest = $post->contest;
                    if (empty($contest))
                        return abort(404);



                    return view($view, [
                        'post' => $post,
                        'mostPopularPosts' => $mostPopularPosts,
                        'relatednews' => $relatednews,
                        'contest' => $contest,
                        'contested' => (!empty(count(Contestant::where('contest_id',$contest->id)->get()))) ,
                    ]);

                }
                else{
                    return view($view, [
                        'post' => $post,
                        'mostPopularPosts' => $mostPopularPosts,
                        'relatednews' => $relatednews,
                    ]);
                }

            }
            else{
                return abort(404);
            }
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->input();
        //
//        dd($input);
        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
        ];

        if ($input['type'] == 'reward') {
            $validate['embed_id'] = 'required|not_in:none';
        } elseif ($input['type'] == 'vote') {
            $validate['embed_id'] = 'required|not_in:none';
        } elseif ($input['type'] == 'contest') {
            $validate['embed_id'] = 'required|not_in:none';
        } else {

        }

        $this->validate($request, $validate);

        $post = Post::find($id);
        $post->name = $input['name'];
        $post->content = $input['content'];
        $post->category_id = $input['category'];
        $post->user_id = Auth::user()->id;
        $post->status = $input['status'];

        if ($input['type'] == 'reward') {
            $post->prize_id = $input['embed_id'];
            $post->vote_id = null;
            $post->contest_id = null;
        } elseif ($input['type'] == 'vote') {
            $post->vote_id = $input['embed_id'];
            $post->prize_id = null;
            $post->contest_id = null;
        } elseif ($input['type'] == 'contest') {
            $post->contest_id = $input['embed_id'];
            $post->prize_id = null;
            $post->vote_id = null;
        } else {
            $post->prize_id = null;
            $post->vote_id = null;
            $post->contest_id = null;
        }

        if(!empty($input['tags'])){
            $post->tags = explode(',', $input['tags']);
        }
        else{
            $post->syncTags([]);
        }

        if($post->status == 1){
            $post->published_at = Carbon::now();
        }

        if($input['filepath'] != $post->original_image) {
            $path = str_replace(' ', '%20', $input['filepath']);
            $time = time();
            $img = Image::make(asset($path));
            $img->fit(845, 560);
            $filename = $time . '_post_' . $post->id . '.jpg';
            $filepath = public_path('images/posts') . '/' . $filename;
            $img->save($filepath);
            $post->thumbnail_image = $filename;
            $post->original_image = $input['filepath'];
        }

//        $post->save();

        $old_flag = $post->topstory_flag;
        $new_flag = (empty($input['topstory_flag'])) ? 0 : 1;
        $post->topstory_flag = $new_flag;
        $post->user_id = Auth::user()->id;
        $post->save();

        //reorder featured
        if($old_flag == 1 && $new_flag == 0){
//            $post->featured_order = 0;
            $post->save();
            $topstories = Post::where('topstory_flag' , 1)->orderBy('topstory_order')->get();
            for ($i = 0; $i < count($topstories); $i++) {
                $topstories[$i]->topstory_order = $i+1;
                $topstories[$i]->save();
            }
        }
        elseif($old_flag == 0 && $new_flag == 1){
            $post->topstory_order = Post::lastOrderTopstory() + 1;
            $post->save();
        }

        return redirect('cfadmin/lifestyle')->with('alert-success', 'Lifestyle news #'.$post->id.' has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->status = 0;
        $post->save();
        return redirect('cfadmin/lifestyle')->with('alert-danger', 'Lifestyle news #'.$post->id.' has been inactivated.');
    }

    public function active($id){
        $post = Post::find($id);

        if(empty($post->published_at)){
            $post->published_at = Carbon::now();
        }

        $post->status = 1;
        $post->save();
        return redirect('cfadmin/lifestyle')->with('alert-success', 'Lifestyle news #'.$post->id.' has been activated.');
    }


    public function topstories(Request $request)
    {
        $topstories = Post::where('type', 'lifestyle')->where('status',1)->where('topstory_flag' , 1)->orderBy('topstory_order')->get();

        $musicnews = Post::where('type', 'lifestyle')->where('topstory_flag', 0)->where('status', 1)->orderBy('created_at', 'desc')->get();

        return view('lifestyle.cfadmin.topstories', [
            'topstories' => $topstories,
            'musicnews' => $musicnews,
            'topstoryfull' => (count(Post::where('type','lifestyle')->where('status',1)->where('topstory_flag',1)->get()) >= 6) ? true : false,
        ]);
    }

    public function getlifestylenottopstory(Request $request)
    {

        $ajaxnews = Post::where('type', 'lifestyle')->where('topstory_flag', 0)->where('status', 1)->orderBy('created_at', 'desc')->get();

        if ($request->ajax()) {
            $view = view('music.cfadmin.loadmusicnottopstory', compact('ajaxnews'))->render();
            return response()->json(['html' => $view]);
        }
    }

    public function addtopstory(Request $request)
    {
        $input = $request->input();

        $topstoryfull = (count(Post::where('type', 'lifestyle')->where('status', 1)->where('topstory_flag', 1)->get()) >= 6);

        $post = Post::find($input['postid']);
        if($post->topstory_flag == 1 || $topstoryfull){
            return 1;
        }
        else{
            $post->topstory_flag = 1;
            $post->topstory_order = Post::lastOrderTopstory() + 1;
            $post->save();
            $topstoryfull = (count(Post::where('type', 'lifestyle')->where('status', 1)->where('topstory_flag', 1)->get()) >= 6);

            if ($topstoryfull) {
                return 2;
            } else {
                return 0;
            }
        }
    }

    public function removetopstory(Request $request)
    {
        $input = $request->input();
        $post = Post::find($input['id']);
        $post->topstory_flag = 0;
        $post->topstory_order = 0;
        $post->save();

        $topstories = Post::where('topstory_flag' , 1)->orderBy('topstory_order')->get();
        for ($i = 0; $i < count($topstories); $i++) {
            $topstories[$i]->topstory_order = $i+1;
            $topstories[$i]->save();
        }
    }

    public function reordertopstory(Request $request)
    {
        $input = $request->input();

        $ids = explode(",", $input['ids']);
        for ($i = 0; $i < count($ids); $i++) {
            $post = Post::find($ids[$i]);
            $post->topstory_order = $i+1;
            $post->save();
        }
    }

    public function ajaxnews(Request $request)
    {
        $input = $request->input();
        if($input['cat'] == 0){
            $ajaxnews = Post::where('type','lifestyle')->where('status', 1)->orderBy('updated_at','desc')->paginate(6);

        }
        else{
            $ajaxnews = Post::where('type','lifestyle')->where('category_id', $input['cat'])->where('status', 1)->orderBy('updated_at','desc')->paginate(6);
        }

//        dd($ajaxnews);

        if ($request->ajax()) {

            $view = view('lifestyle.loadnews', compact('ajaxnews'))->render();
            return response()->json(['html'=>$view]);
        }
    }
}
