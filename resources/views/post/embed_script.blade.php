@if(Auth::check())
    $('.choice-item').click(function (event) {
    event.preventDefault();
    $('input[name="choice"]').val($(this).data('choice'));

    $('#vote-form').submit();
    });
@endif