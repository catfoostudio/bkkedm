@extends('layouts.main')


@section('title')
    {{ $post->name.' | BKK EDM' }}
@endsection
@section('fb-og')
    <meta property="og:url"           content="{{ url('music/'.$post->id) }}" />
    <meta property="og:title"         content="{{$post->name.' | '}}" />
    {{--<meta property="og:description"   content="Your description" />--}}
    <meta property="og:image"         content="{{ asset('images/posts/'.$post->thumbnail_image) }}" />
@endsection
@section('content')
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '2289111224681592',
                cookie     : true,
                xfbml      : true,
                version    : 'v3.2'
            });

            FB.AppEvents.logPageView();

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap" style="padding: 30px 0;">

            <div class="container clearfix">

                <!-- Post Content
                ============================================= -->
                <div class="postcontent nobottommargin clearfix">

                    <div class="single-post nobottommargin">

                        <!-- Single Post
                        ============================================= -->
                        <div class="entry clearfix">

                            <!-- Entry Title
                            ============================================= -->
                            <div class="entry-title">
                                <h2>{{ $post->name }}</h2>
                            </div><!-- .entry-title end -->

                            <!-- Entry Meta
                            ============================================= -->
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> {{ \Carbon\Carbon::parse($post->updated_at)->format('dS F Y') }}</li>
                                <li><a href="#"><i class="icon-user"></i> {{ $post->user->name }}</a></li>
                            </ul><!-- .entry-meta end -->

                            <!-- Entry Image
                            ============================================= -->
                            <div class="entry-image">
                                <img src="{{ asset('images/posts/'.$post->thumbnail_image) }}">
                            </div><!-- .entry-image end -->
                            <!-- Post Single - Share
                                                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Share this Post:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook js-share-facebook-link" id="shareBtn">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a target="_blank" href="https://twitter.com/intent/tweet?text={{ urlencode(\Illuminate\Support\Facades\Request::url()) }}" class="social-icon si-borderless si-twitter js-share-twitter-link">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                </div>
                            </div><!-- Post Single - Share End -->
                            <div class="line" style="margin:20px 0;"></div>
                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">

                            {!! $post->content  !!}
                            <!-- Tag Cloud
                                ============================================= -->
                                @if(!empty(count($post->tags)))
                                <div class="tagcloud clearfix bottommargin">
                                    @foreach($post->tags as $tag)
                                        <a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>
                                    @endforeach
                                </div><!-- .tagcloud end -->
                                @endif
                                <div class="clear"></div>


                            </div>
                            @include('post.embed')

                        </div><!-- .entry end -->
                    <!-- Post Author Info
                        ============================================= -->
                        <div class="card">
                            <div class="card-header"><strong>Posted by <a href="#">{{ $post->user->name }}</a></strong>
                            </div>
                            <div class="card-body">
                                <div class="author-image">
                                    <img src="{{ empty($post->user->avatar) ? asset('images/author/1.jpg') : asset('images/profile/'.$post->user->avatar)}}"
                                         alt="" class="rounded-circle">
                                </div>
                                {{ $post->user->quote }}
                            </div>
                        </div><!-- Post Single - Author End -->


                        {{--@include('layouts.comments')--}}
                        @if(!empty(count($relatednews)))
                        <div class="line"></div>

                        <h4>Related Posts:</h4>

                        <div class="related-posts clearfix">

                            @for($i=0;$i<count($relatednews);$i+=2)
                            <div class="col_half nobottommargin @if($i==2) col_last @endif" style="margin-bottom:0px !important;">
                                @if(!empty($relatednews[$i]))
                                <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;max-height: 120px;overflow: hidden;">
                                    <div class="entry-image">
                                        <a href="{{ url($relatednews[$i]->type.'/'.$relatednews[$i]->id.'/'.str_replace(' ','-',urlencode($relatednews[$i]->name))) }}"><img style="object-fit: cover;height: 110px;" src="{{ asset('images/posts/'.$relatednews[$i]->thumbnail_image)}}"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title" style="max-height: 70px;overflow: hidden;">
                                            <h4 style="font-size: 16px;"><a href="{{ url($relatednews[$i]->type.'/'.$relatednews[$i]->id.'/'.str_replace(' ','-',urlencode($relatednews[$i]->name))) }}">{{ $relatednews[$i]->name }}</a></h4>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($relatednews[$i]->updated_at)->format('dS F Y')}}</li>
                                        </ul>

                                    </div>
                                </div>
                                @endif
                                @if(!empty($relatednews[$i+1]))
                                        <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;max-height: 120px;overflow: hidden;">
                                    <div class="entry-image">
                                        <a href="{{ url($relatednews[$i+1]->type.'/'.$relatednews[$i+1]->id.'/'.str_replace(' ','-',urlencode($relatednews[$i+1]->name))) }}"><img style="object-fit: cover;height: 110px;" src="{{ asset('images/posts/'.$relatednews[$i+1]->thumbnail_image)}}"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title" style="max-height: 70px;overflow: hidden;">
                                            <h4 style="font-size: 16px;"><a href="{{ url($relatednews[$i+1]->type.'/'.$relatednews[$i+1]->id.'/'.str_replace(' ','-',urlencode($relatednews[$i+1]->name))) }}">{{ $relatednews[$i+1]->name }}</a></h4>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($relatednews[$i+1]->updated_at)->format('dS F Y')}}</li>
                                        </ul>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @endfor
                        </div>
                        @endif
                    </div>

                </div><!-- .postcontent end -->

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar nobottommargin col_last clearfix">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">

                            <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                                <h4>Most Popular</h4>
                                <div class="tab-container">

                                    <div class="tab-content clearfix" id="tabs-1">
                                        <div id="popular-post-list-sidebar">
                                            @foreach($mostPopularPosts as $item)
                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="{{ url($item->type.'/'.$item->id.'/'.str_replace(' ','-',urlencode($item->name))) }}" class="nobg">
                                                            <img class="rounded-circle" src="{{ asset('images/posts/'.$item->thumbnail_image)}}" style="object-fit: cover;"></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="{{ url($item->type.'/'.$item->id.'/'.str_replace(' ','-',urlencode($item->name))) }}">{{ $item->name }}</a>
                                                            </h4>
                                                        </div>
                                                        <ul class="entry-meta clearfix">
                                                            <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($item->updated_at)->format('dS F Y')}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div><!-- .sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')
    {!! NoCaptcha::renderJs() !!}
<script>
    $(document).ready(function() {

        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                href: '{{ url('music/'.$post->id) }}'
            }, function(response){});

        }

        $('.js-share-twitter-link').click(function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            window.open(href, "Twitter", "height=285,width=550,resizable=1");
        });

        @include('post.embed_script')

        // $('.js-share-facebook-link').click(function(e) {
        //     e.preventDefault();
        //     var href = $(this).attr('href');
        //     window.open(href, "Facebook", "height=269,width=550,resizable=1");
        // });
    });
</script>
@endsection
