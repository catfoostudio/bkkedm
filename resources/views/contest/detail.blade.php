@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-event">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-6 col-12" style="padding-bottom: 20px;">
                            <div class="entry-image nobottommargin">
                                <img src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" >
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 col-12">
                            <div class="card events-meta mb-3">

                                <div class="card-body" style="background-color: #333;color: white">
                                    <ul class="iconlist nobottommargin">
                                        <h3 style="color: white">{{ $contest->name }}</h3>
                                        <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($contest->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($contest->end)->format('d M Y') }}</li>
                                        <li><i class="icon-gift"></i>{{ $contest->reward }}</li>
                                        {{--<li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($lotto->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($lotto->end)->format('d M Y') }}</li>--}}
                                    </ul>
                                </div>

                            </div>
                            @if(Auth::check())
                            {!! Form::open(['method' => 'POST' , 'action' => ['ContestController@register', $contest->id],'id' => 'form-contest', 'enctype' => 'multipart/form-data']) !!}
                            @endif
                            <div class="fancy-title title-border" style="margin-bottom: 0px;">
                                <h3>Register</h3>
                            </div>
                            <div>
                                @if(!Auth::check())<div style="position: absolute;opacity:1;color: black; top: 330px;left:35%;font-weight: bold;font-size: 20px;z-index: 1;">LOGIN to Vote</div>
                                @elseif($contested)<div style="position: absolute;opacity:1;color: black; top: 330px;left:35%;font-weight: bold;font-size: 25px;z-index: 1;"></div>@endif
                            <div style="{{ (!Auth::check() || $contested) ? 'pointer-events:none;opacity:0.3;' : '' }}">
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-lg-6">
                                    {{ $contest->link_name }}: {!! Form::text('link' , '', ['class'=>'form-control'.($errors->has('link') ? ' error' : '')]) !!}
                                </div>
                                <div class="col-lg-6">
                                    {{ $contest->file_name }}: <span style="font-size: 14px;">(รูปขนาดไม่เกิน 4MB)</span> {!! Form::file('file', ['class'=>'form-control'.($errors->has('file') ? ' error' : '')]) !!}
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-lg-12">
                                    {{ $contest->content_name }}: {!! Form::textarea('content' , '', ['class'=>'form-control '.(($errors->has('content')) ? 'error' : ''), 'rows'=>5]) !!}
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-lg-12">
                                    {!! NoCaptcha::displaySubmit('form-contest', ($contested) ? 'Registered' : 'Register', ['class' => 'btn btn-dark btn-block btn-lg']) !!}
                                </div>
                            </div>
                            </div>
                            </div>
                            @if(Auth::check())
                            {!! Form::close() !!}
                            @endif
{{--                            @if(Auth::check())--}}
                            {{--@if(empty($hunter))--}}
                                {{--{!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@register' , $contest],'id'=>'my-form-id']) !!}--}}
                                {{--{!! NoCaptcha::displaySubmit('my-form-id', 'Register', ['class' => 'btn btn-danger btn-block btn-lg']) !!}--}}
                                {{--{!! Form::close() !!}--}}
                            {{--@else--}}
                                {{--<button class="btn btn-danger btn-block btn-lg disabled">Registered</button>--}}
                            {{--@endif--}}
                            {{--@else--}}
                                {{--<a href="{{ route('signin') }}"><button class="btn btn-danger btn-block btn-lg">Sign In to register</button></a>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                    <div class="clear"></div>
                    {{--<div class="fancy-title title-border">--}}
                        {{--<h3>Contestant</h3>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">--}}
                            {{--<img src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" style="width:300px;height: 300px;object-fit: cover">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">--}}
                            {{--<img src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" style="width:300px;height: 300px;object-fit: cover">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">--}}
                            {{--<img src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" style="width:300px;height: 300px;object-fit: cover">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">--}}
                            {{--<img src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" style="width:300px;height: 300px;object-fit: cover">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="col_three_fourth">
                        <div style="padding: 30px 0;">
                            <div class="fancy-title title-border">
                                <h3>Description</h3>
                            </div>
                        {!! $contest->content !!}
                            {{--{!! $contest->content !!}--}}
                        <!-- Tag Cloud
                                ============================================= -->
                            @if(!empty(count($contest->tags)))
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-lg-12">
                                        <div class="tagcloud clearfix bottommargin">
                                            @foreach($contest->tags as $tag)
                                                <a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>
                                            @endforeach
                                        </div><!-- .tagcloud end -->
                                    </div>
                                </div>

                            @endif
                            <div class="clear"></div>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {!! NoCaptcha::renderJs() !!}
@endsection