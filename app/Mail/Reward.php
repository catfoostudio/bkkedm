<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reward extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Congratulations! You\'ve won a prize on '.$this->input['prizename'])
            ->view('prize.mail')
            ->with([
                'prizename' => $this->input['prizename'],
                'code' => $this->input['code'],
                'username' => $this->input['username'],
            ]);
    }
}
