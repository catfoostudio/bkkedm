<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , $type)
    {
        $input = $request->input();

        $categories = Category::where('type',$type)->get();

        return view('category.cfadmin.index', [
            'categories' => $categories,
            'type' => $type,
        ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        //
        return view('category.cfadmin.create', [
            'type' => $type,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'type' => 'required',
        ];

        $this->validate($request, $validate);

        $category = Category::create([
            'name' => $input['name'],
            'status' => 1,
            'description' => '',
            'thumbnail_image' => '',
            'type' => $input['type'],
            'user_id' => Auth::user()->id,
        ]);

        return redirect('cfadmin/categorytype/'.$input['type'])->with('alert-success', 'Category #'.$category->id.' has been created!');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $category = Category::find($id);
        return view('category.cfadmin.detail', [
            'category' => $category,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
        ];

        $this->validate($request, $validate);

        $category = Category::find($id);
        $category->name = $input['name'];
        $category->user_id = Auth::user()->id;
        $category->save();

        return redirect('cfadmin/categorytype/'.$category->type)->with('alert-success', 'Category #'.$category->id.' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);
        $category->status = 0;
        $category->save();
        return redirect('cfadmin/categorytype/'.$category->type)->with('alert-danger', 'Category #'.$category->id.' has been inactivated!');
    }

    public function active($id)
    {
        //
        $category = Category::find($id);
        $category->status = 1;
        $category->save();
        return redirect('cfadmin/categorytype/'.$category->type)->with('alert-success', 'Category #'.$category->id.' has been activated!');
    }
}
