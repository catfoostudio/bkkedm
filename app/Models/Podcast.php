<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Podcast extends Model
{
    //
    use \Spatie\Tags\HasTags;
    //
    protected $table = 'podcasts';

    protected $guarded = [];

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Published';
        }
        elseif($this->status == 2){
            return 'Draft';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Published</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Draft</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }
}
