@extends('layouts.main')
@section('content')
    <section id="page-title">
        <div class="container clearfix">
            <h1>{{ $tag->name }}</h1>
            @if(Auth::check() && !Auth::user()->tags()->find($tag->id))
                <button class="button button-3d button-mini button-rounded button-black" style="margin-top: 10px;" id="btn-follow">Following</button>
                <button class="button button-3d button-mini button-rounded button-black" style="margin-top: 10px;display: none" id="btn-unfollow">Unfollow</button>
            @elseif(Auth::check() && Auth::user()->tags()->find($tag->id))
                <button class="button button-3d button-mini button-rounded button-black" style="margin-top: 10px;display: none" id="btn-follow">Following</button>
                <button class="button button-3d button-mini button-rounded button-black" style="margin-top: 10px;" id="btn-unfollow">Unfollow</button>
            @endif
        </div>
    </section>

    <section id="content">

        <div class="content-wrap">
            <div class="container clearfix">
            @if(!empty(count($posts)))
            <h4>POSTS</h4>
            <div id="posts_cat_0" class="row">
                @foreach($posts as $news)
                    <div class="col-lg-4 col-md-6 col-xs-6 col-sm-6 col-12">
                        <div class="entry clearfix" >
                            <div class="entry-image" style="margin-bottom:10px;">
                                <a href="{{ url('music/'.$news->id.'/'.str_replace(' ','-',urlencode($news->name))) }}"><img class="image_fade"
                                                                                                                             src="{{ asset('images/posts/'.$news->thumbnail_image) }}"></a>
                            </div>
                            <div class="entry-title" style="margin-bottom: 10px;">
                                <h2 style="height: auto;font-size: 18px;"><a href="{{ url('music/'.$news->id.'/'.str_replace(' ','-',urlencode($news->name))) }}">{{ $news->name }}</a></h2>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row" id="btn-loadmoredata">
                <div class="col-12" align="center">
                    <button class="button button-3d button-rounded button-black"
                            onclick="loadMoreData(0);">Load more
                    </button>
                </div>
            </div>
            @endif
                @if(!empty(count($events)))
                <h4>EVENTS</h4>
                <div id="posts" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($events as $event)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('event/'.$event->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/events/posters/'.$event->poster_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('event/'.$event->id) }}">{{ $event->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS M Y') }}</li>
                                <li><i class="icon-map-marker"></i>{{ $event->location }}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreEventData('present')">Load more</button>
                    </div>
                </div>
                @endif
            </div>

        </div>

    </section><!-- #content end -->
@endsection


@section('script')
    <script>

        var pageNumber_cat_0 = 2;


        function getPage(cat) {
            if(cat == 0){
                return pageNumber_cat_0;
            }

        }

        function setPage(cat) {
            if(cat == 0){
                pageNumber_cat_0 += 1;
            }

        }


        function loadMoreData(cat) {
            var html = "<div class=\"css3-spinner\" style='background-color: #ffffff85'><div class=\"css3-spinner-bounce1\"></div><div class=\"css3-spinner-bounce2\"></div><div class=\"css3-spinner-bounce3\"></div></div>";
            $(html).hide().appendTo("body").fadeIn(500);
            $.ajax({
                type: 'GET',
                url: "/ajaxtag?tag={{$tag->id}}&page=" + getPage(cat),
                success: function (data) {
                    setPage(cat);
                    if (data.length == 0) {
                        // :( no more articles
                    } else {
                        $('#posts_cat_'+cat).append(data.html);

                        if(!data.hasMorePages){
                            $('#btn-loadmoredata').hide();
                        }
                    }
                    $('.css3-spinner').fadeOut(500);
                }, error: function (data) {
                    console.log('fail to load more event.');
                    $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            })
        }

        function loadMoreEventData(type){
            if(type == 'present') {
                $.ajax({
                    type: 'GET',
                    url: "event?page=" + pageNumber,
                    success: function (data) {
                        pageNumber += 1;
                        if (data.length == 0) {
                            // :( no more articles
                        } else {
                            $('#posts').append(data.html);
                        }
                    }, error: function (data) {
                        console.log('fail to load more event.');
                    },
                })
            }
            else{
                $.ajax({
                    type: 'GET',
                    url: "loadpastevent?page=" + pastPageNumber,
                    success: function (data) {
                        pastPageNumber += 1;
                        if (data.length == 0) {
                            // :( no more articles
                        } else {
                            $('#pastposts').append(data.html);
                        }
                    }, error: function (data) {
                        console.log('fail to load more event.');
                    },
                })
            }
        }

        @if(Auth::check())
        
        function follow() {
            var html = "<div class=\"css3-spinner\" style='background-color: #ffffff85'><div class=\"css3-spinner-bounce1\"></div><div class=\"css3-spinner-bounce2\"></div><div class=\"css3-spinner-bounce3\"></div></div>";
            $(html).hide().appendTo("body").fadeIn(500);
            $.ajax({
                type: 'GET',
                url: "/follow?id={{$tag->id}}",
                success: function (data) {

                    // if (data.length == 0) {
                    //     // :( no more articles
                    // } else {
                    //     $('#posts_cat_'+cat).append(data.html);
                    // }
                    // $('.css3-spinner').fadeOut(500);
                    if(data.success){
                        $('#btn-follow').hide();
                        $('#btn-unfollow').show();
                    }
                    $('.css3-spinner').fadeOut(500);
                }, error: function (data) {
                    // console.log('fail to load more event.');
                    $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            })
        }

        function unfollow() {
            $.ajax({
                type: 'GET',
                url: "/unfollow?id={{$tag->id}}",
                success: function (data) {

                    if(data.success){
                        $('#btn-unfollow').hide();
                        $('#btn-follow').show();
                    }

                    $('.css3-spinner').fadeOut(500);
                }, error: function (data) {
                    // console.log('fail to load more event.');
                    // $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            })
        }

        $('#btn-follow').click(function () {
           follow();
        });

        $('#btn-unfollow').click(function () {
            unfollow();
        });

        @endif

        var $loading = $('.css3-spinner').hide();
        //Attach the event handler to any element
        $(document)
            .ajaxStart(function () {
                //ajax request went so show the loading image
                $loading.show();
            })
            .ajaxStop(function () {
                //got response so hide the loading image
                $loading.hide();
            });
        // });
    </script>
@endsection