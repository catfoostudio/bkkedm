@extends('layouts.main')
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="row clearfix">

                    <div class="col-md-9">

                        {{--<img src="images/icons/avatar.jpg" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}

                        <div class="heading-block noborder">
                            <h3>{{ $member->name }}</h3>
                            <span>Lottos</span>
                            {{--<span>Messages <a href="#" class="button button-3d button-rounded button-black"><i class="icon-mail"></i>Compose</a></span>--}}
                        </div>
                        <div class="clear"></div>
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <div class="tabs tabs-alt clearfix" id="tabs-profile">
                                    <ul class="tab-nav clearfix">
                                        <li><a href="#tab-feeds"><i class="icon-rss2"></i> All</a></li>
                                        {{--<li><a href="#tab-posts"><i class="icon-pencil2"></i> Unread</a></li>--}}
                                        {{--<li><a href="#tab-replies"><i class="icon-reply"></i> Read</a></li>--}}
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="tab-feeds">
                                            {{--<p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium harum ea quo! Nulla fugiat earum, sed corporis amet iste non, id facilis dolorum, suscipit, deleniti ea. Nobis, temporibus magnam doloribus. Reprehenderit necessitatibus esse dolor tempora ea unde, itaque odit. Quos.</p>--}}
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Lotto end</th>
                                                    <th>Title</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($gamblers as $gambler)
                                                    <tr>
                                                        <td style="width:30%;">
                                                            <code>{{ \Carbon\Carbon::parse($gambler->end)->format('M d, Y') }}</code>
                                                        </td>
                                                        <td><a href="{{ url('contest/'.$gambler->id) }}">{{ $gambler->name }}</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    <div class="col-md-3 clearfix">

                        <div class="list-group">
                            {{--<a href="#" class="list-group-item list-group-item-action clearfix">Profile <i class="icon-user float-right"></i></a>--}}
                            {{--<a href="#" class="list-group-item list-group-item-action clearfix">Servers <i class="icon-laptop2 float-right"></i></a>--}}
                            <a href="{{ url('member') }}" class="list-group-item list-group-item-action clearfix">Messages <i class="icon-envelope2 float-right"></i></a>
                            <a href="{{ url('member/lotto') }}" class="list-group-item list-group-item-action clearfix">Lotto <i class="icon-credit-cards float-right"></i></a>
                            <a href="{{ url('member/prize') }}" class="list-group-item list-group-item-action clearfix">Prize <i class="icon-credit-cards float-right"></i></a>
                            <a href="{{ url('member/contest') }}" class="list-group-item list-group-item-action clearfix" style="color: #292929;background-color: #f2f2f2;border: 1px solid rgba(0, 0, 0, 0.125);">Contest <i class="icon-credit-cards float-right"></i></a>
                            {{--<a href="#" class="list-group-item list-group-item-action clearfix">Settings <i class="icon-cog float-right"></i></a>--}}
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="list-group-item list-group-item-action clearfix">Logout <i class="icon-line2-logout float-right"></i></a>
                        </div>

                        {{--<div class="fancy-title topmargin title-border">--}}
                        {{--<h4>About Me</h4>--}}
                        {{--</div>--}}

                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum laboriosam, dignissimos veniam obcaecati. Quasi eaque, odio assumenda porro explicabo laborum!</p>--}}

                        {{--<div class="fancy-title topmargin title-border">--}}
                        {{--<h4>Social Profiles</h4>--}}
                        {{--</div>--}}

                        {{--<a href="#" class="social-icon si-facebook si-small si-rounded si-light" title="Facebook">--}}
                        {{--<i class="icon-facebook"></i>--}}
                        {{--<i class="icon-facebook"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="social-icon si-gplus si-small si-rounded si-light" title="Google+">--}}
                        {{--<i class="icon-gplus"></i>--}}
                        {{--<i class="icon-gplus"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="social-icon si-dribbble si-small si-rounded si-light" title="Dribbble">--}}
                        {{--<i class="icon-dribbble"></i>--}}
                        {{--<i class="icon-dribbble"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="social-icon si-flickr si-small si-rounded si-light" title="Flickr">--}}
                        {{--<i class="icon-flickr"></i>--}}
                        {{--<i class="icon-flickr"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="social-icon si-linkedin si-small si-rounded si-light" title="LinkedIn">--}}
                        {{--<i class="icon-linkedin"></i>--}}
                        {{--<i class="icon-linkedin"></i>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="social-icon si-twitter si-small si-rounded si-light" title="Twitter">--}}
                        {{--<i class="icon-twitter"></i>--}}
                        {{--<i class="icon-twitter"></i>--}}
                        {{--</a>--}}
                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')

@endsection
