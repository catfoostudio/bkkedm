<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    use \Spatie\Tags\HasTags;

    protected $table = 'prizes';

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps()->withPivot('link','status');
    }

    public function activatecodes()
    {
        return $this->hasMany('App\Models\Activatecode');
    }

    public function club()
    {
        return $this->belongsTo('App\Models\Club');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Published';
        }
        elseif($this->status == 2){
            return 'Draft';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Published</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Draft</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public function getTypeAttribute()
    {
        $type = 'none';

        if(!empty($this->club_id)){
            $type = 'club';
        }
        elseif (!empty($this->event_id)){
            $type = 'event';
        }
        else{
            $type = 'own';
        }

        return $type;
    }
}
