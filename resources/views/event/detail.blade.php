@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-event">
                    <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">
                        <div class="entry-image nobottommargin">
                            <img src="{{ asset('images/events/posters/'.$event->poster_image) }}" alt="Event Single">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                        <div class="card events-meta mb-3">

                            <div class="card-body" style="background-color: #333;color: white">
                                <ul class="iconlist nobottommargin">
                                    <h3 style="color: white">{{ $event->name }}</h3>
                                    <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('d M Y') }}</li>
                                    {{--<li><i class="icon-time"></i> 20:00 - 02:00</li>--}}
                                    <li><i class="icon-map-marker2"></i> {{ $event->location }}</li>
                                    @if($event->price != 0.00)<li><i class="icon-money"></i> <strong>{{ $event->price }}</strong></li>@endif
                                </ul>
                            </div>
                        </div>
                        @if(!empty($event->ticket))<a target="_blank" href="{{ $event->ticketurl }}" class="btn btn-danger btn-block btn-lg">Buy Tickets</a>@endif
                    </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_three_fourth">
                       <div style="padding: 30px 0;">
                           <div class="fancy-title title-border">
                               <h3>Description</h3>
                           </div>
                           {!! $event->content !!}

                       <!-- Tag Cloud
                                ============================================= -->
                           @if(!empty(count($event->tags)))
                               <div class="tagcloud clearfix bottommargin">
                                   @foreach($event->tags as $tag)
                                       <a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>
                                   @endforeach
                               </div><!-- .tagcloud end -->
                           @endif
                           <div class="clear"></div>
                       </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

@endsection