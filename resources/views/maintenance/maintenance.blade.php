<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>@yield('title')BKK EDM - BANGKOK THAILAND ELECTRONIC DANCE MUSIC AND NEWS!!</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="logo" style="height: 80px;" >
						<a href="{{ url('index') }}" class="standard-logo" data-dark-logo="{{ asset('images/logo/logo.png') }}"><img src="{{ asset('images/logo/logo.png') }}" alt="Canvas Logo"></a>
						<a href="{{ url('index') }}" class="retina-logo" data-dark-logo="{{ asset('images/logo/logo.png') }}"><img style="height: 60px;margin-top: 10px;" src="{{ asset('images/logo/logo.png') }}" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

				</div>

			</div>

		</header><!-- #header end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="heading-block center nobottomborder">
						<h1>Site is Under Maintenance</h1>
						<span>Please check back in sometime.</span>
					</div>

					<div class="col_one_third topmargin">

					</div>

					<div class="col_one_third topmargin">
						<div class="feature-box fbox-center fbox-light fbox-plain">
							<div class="fbox-icon">
								<a href="#"><i class="icon-warning-sign"></i></a>
							</div>
							<h3>Why is the Site Down?</h3>
							<p>The site is under maintenance probably because we are working to improve this website drastically.</p>
						</div>
					</div>

					<div class="col_one_third topmargin col_last">

					</div>

				</div>

			</div>

		</section><!-- #content end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{asset('js/jquery.js') }}"></script>
	<script src="{{asset('js/plugins.js') }}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('js/functions.js') }}"></script>

</body>
</html>