@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-md-9">
                        {{--                        <img src="{{ asset('images/icons/avatar.jpg') }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}
                        <div class="heading-block noborder" style="margin-bottom: 0px;">
                            <h3>Redeem</h3>
                            {{--                            <span style="font-size: 15px;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y') }}</span>--}}
                        </div>
                        {{--<div class="clear"></div>--}}

                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="lead">Confirmation Info.</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody><tr>
                                                <th style="width:50%">Name:</th>
                                                <td>{{ $activatecode->prize->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Start - End date:</th>
                                                <td>{{ \Carbon\Carbon::parse($activatecode->prize->start)->format('d/m/Y') }} - {{ \Carbon\Carbon::parse($activatecode->prize->end)->format('d/m/Y') }}</td>
                                            </tr>
                                            <tr>
                                                <th>Club:</th>
                                                <td>{{ (!empty($activatecode->prize->club_id)) ? $activatecode->prize->club->name : '-'}}</td>
                                            </tr>
                                            <tr>
                                                <th>Event:</th>
                                                <td>{{ (!empty($activatecode->prize->event_id)) ? $activatecode->prize->event->name : '-'}}</td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    {!! Form::open(['method' => 'POST' , 'route' => ['confirm_member', $activatecode->id] , 'style' => 'display:inline', 'id' => 'redeem-form']) !!}
                                    <div class="form-group">
                                        <label>Redeem Code:</label>
                                        <input type="text" name="redeem_code" class="form-control" data-inputmask="'mask': ['aa-****']" data-mask="" value="{{ $activatecode->code }}" disabled="disabled">
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-4">
                                        <button class="btn btn-dark btn-block btn-lg">Confirm</button>
                                    </div>

                                    <div class="col-lg-4">
                                    <a href="{{ url('redeem') }}"><button type="button" class="btn btn-light btn-block btn-lg">Cancel</button></a>
                                    </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    @include('member.sidebar')

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        $(function () {
            $('.mnth').datepicker({
                autoclose: true,
                // viewMode: "years",
                // minViewMode: "years",
                format: "dd/mm/yyyy",
                yearRange: "-100:+0",
            });

            // $('[data-mask]').inputmask();

        });
    </script>
    {!! NoCaptcha::renderJs() !!}
@endsection
