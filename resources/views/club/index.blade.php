@extends('layouts.main')

@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title" style="background: url(images/parallax/home/5.jpg);background-size: cover;">
        <div class="container clearfix" align="center">
            <h1 style="font-size: 40px;">Club</h1>
        </div>
    </section><!-- #page-title end -->

    <section id="content">

        <div class="content-wrap" style="padding: 50px 0;">
            <div class="container clearfix">
            <div id="posts" class="post-grid grid-container grid-4 clearfix" data-layout="fitRows">

                @foreach($clubs as $club)
                <div class="entry ">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="{{ url('club/'.$club->id)  }}"><img src="{{ asset('images/clubs/'.$club->logo_image) }}"></a>
                        </div>
                        <div class="fbox-desc">
                            <h3><a href="{{ url('club/'.$club->id)  }}">{{ $club->name }}</a><span class="subtitle" style="font-weight: 500">{{ $club->location }}</span></h3>
                        </div>
                    </div>

                </div>
                @endforeach
            </div>
            </div>
        </div>
    </section>
@endsection