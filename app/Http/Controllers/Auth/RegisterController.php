<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Helper;
use App\Mail\ActivateUser;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'firstname' => 'required|string|max:255',
            'firstname' => 'required|string|unique:users,firstname,NULL,id,lastname,' . $data['lastname'],
            'lastname' => 'required|string|max:255',
            'email_register' => 'required|string|email|max:255|unique:users,email',
            'password_register' => 'required|string|min:8|required_with:password_register|same:password_confirmation',
            'password_confirmation' => 'min:8',
            'mobile' => 'required|digits:10|unique:users,mobile',
            'birthday' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['firstname'].' '.$data['lastname'],
            'email' => $data['email_register'],
            'password' => Hash::make($data['password_register']),
            'activate_code' => time().'_'.Helper::generateRandomString(30),
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'birthday' => Carbon::createFromFormat('d/m/Y',$data['birthday'])->format('Y-m-d'),
            'status' => 0
        ]);
    }

    protected function register(Request $request)
    {
//        dd('register');

        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        $user->assignRole('member');
        Mail::to($user->email)->send(new ActivateUser($user->activate_code)); //$request->input()['email']

//        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: view('auth.verify');
    }

    protected function activate(Request $request,$code)
    {
        $user = User::where('activate_code' , $code)->first();
        $user->status = 1;
        $user->save();

        Auth::login($user);

        return redirect('/');
    }

}
