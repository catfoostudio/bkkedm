<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Vote extends Model
{
    //
    use \Spatie\Tags\HasTags;

    protected $table = 'votes';

    protected $guarded = [];

    public function choices()
    {
        return $this->hasMany('App\Models\Choice');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Published';
        }
        elseif($this->status == 2){
            return 'Draft';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Published</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Draft</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    /**
     * Check login user is already vote or not.
     *
     *
     * @return boolean
     */
    public function isVoted()
    {
        if(Auth::check()) {
            $userChooses = Choice::whereHas('users', function ($query) {
                $query->where('users.id', Auth::user()->id);
            })->where('vote_id', $this->id)->get();

            return !empty(count($userChooses));
        }
        else{
            return false;
        }
    }
}
