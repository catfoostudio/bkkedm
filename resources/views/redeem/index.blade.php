@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-md-9">
                        {{--                        <img src="{{ asset('images/icons/avatar.jpg') }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}
                        <div class="heading-block noborder" style="margin-bottom: 0px;">
                            <h3>Redeem</h3>
                            {{--                            <span style="font-size: 15px;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y') }}</span>--}}
                        </div>
                        {{--<div class="clear"></div>--}}
                        <div class="row clearfix">
                            <div class="col-lg-6">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))
                                    <div class="style-msg successmsg">
                                        <div class="sb-msg"><i class="icon-thumbs-up"></i>{{ Session::get('alert-' . $msg) }}</div>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    </div>
                                    @endif
                                @endforeach
                                <!-- Comments
						============================================= -->
                                <div id="comments" class="clearfix" style="margin-top: 5px;padding-top: 30px;">
                                    <!-- Comment Form
                                    ============================================= -->
                                    <div id="respond" class="clearfix">

                                        {!! Form::open(array('method'=>'POST','route' => 'redeem_member','id' => 'redeem-form', 'name'=> 'register-form', 'class'=>'nobottommargin' )); !!}
                                        @csrf
                                        <div class="col_full">
                                            <label for="redeem_code">Redeem code:</label>
                                            <input type="hidden" name="user" value="{{Auth::user()->id}}">
                                            <input type="text" id="redeem_code" name="redeem_code" value="{{ old('redeem_code') }}" class="form-control {{$errors->has('redeem_code') ? 'error' : '' }}" />
                                            {{--<input type="text" id="redeem_code" name="redeem_code" value="{{ old('redeem_code') }}" class="form-control {{$errors->has('redeem_code') ? 'error' : '' }}" data-inputmask="'mask': ['aa-****']"  data-mask="" />--}}
                                            @if($errors->has('redeem_code')) <strong style="color:red;">รหัสไม่ถูกต้อง หรือรหัสได้ถูกใช้งานแล้ว</strong> @endif
                                        </div>
                                        <div class="col_full nobottommargin">
                                            {!! NoCaptcha::displaySubmit('redeem-form', 'Redeem', ['class' => 'button button-3d button-black nomargin', 'style' => 'margin-top:50px;']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div><!-- #respond end -->

                                </div><!-- #comments end -->
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    @include('member.sidebar')

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        $(function () {
            $('.mnth').datepicker({
                autoclose: true,
                // viewMode: "years",
                // minViewMode: "years",
                format: "dd/mm/yyyy",
                yearRange: "-100:+0",
            });

            $('[data-mask]').inputmask();

        });
    </script>
    {!! NoCaptcha::renderJs() !!}
@endsection
