<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('content');
            $table->string('thumbnail_image');
            $table->string('original_image');
            $table->string('reward');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('remark')->nullable();
            $table->integer('quota')->unsigned()->nullable();
            $table->integer('views')->unsigned()->nullable();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('required_file')->default(1);
            $table->tinyInteger('required_link')->default(1);
            $table->timestamps();
            $table->dateTime('published_at')->nullable();
            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs');
            $table->integer('event_id')->unsigned()->nullable();
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizes');
    }
}
