<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Podcast;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if($this->isAdminRequest()){
            $view = 'podcast.cfadmin.index';
            $postcasts = Podcast::orderBy('updated_at')->get();

            return view($view, [
                'postcasts' => $postcasts,
            ]);
        }
        else{

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where('type','podcast')->where('status',1)->get();
        return view('podcast.cfadmin.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();
        $validate = [
            'name' => 'required',
            'content' => 'required',
            'embedded_code' => 'required',
        ];

        $this->validate($request, $validate);

        $podcast = Podcast::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'category_id' => $input['category'],
            'embedded_code' => $input['embedded_code'],
            'type' => 'mixcloud',
            'user_id' => Auth::user()->id,
            'status' => $input['status'],
        ]);

        if(!empty($input['tags'])){
            $podcast->tags = explode(',', $input['tags']);
        }

        if($podcast->status == 1){
            $podcast->published_at = Carbon::now();
            $podcast->save();
        }

        return redirect('cfadmin/podcast')->with('alert-success', 'Podcast #'.$podcast->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $podcast = Podcast::find($id);

        if($this->isAdminRequest()){
            $view = 'podcast.cfadmin.detail';
            $categories = Category::where('type','podcast')->where('status',1)->get();
            return view($view, [
                'podcast' => $podcast,
                'categories' => $categories,
            ]);
        }
        else{

            if($podcast->type == 'lifestyle') {
                $view = 'lifestyle.detail';
                $mostPopularPosts = Post::orderBy('updated_at')->get();
                $relatednews = Post::withAnyTags($podcast->tags)->get();

                return view($view, [
                    'podcast' => $podcast,
                    'mostPopularPosts' => $mostPopularPosts,
                    'relatednews' => $relatednews,
                ]);
            }
            else{
                return abort(404);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $validate = [
            'name' => 'required',
            'content' => 'required',
            'embedded_code' => 'required',
        ];

        $this->validate($request, $validate);

        $podcast = Podcast::find($id);
        $podcast->name = $input['name'];
        $podcast->content = $input['content'];
        $podcast->category_id = $input['category'];
        $podcast->embedded_code = $input['embedded_code'];
        $podcast->type = 'mixcloud';

        if(!empty($input['tags'])){
            $podcast->tags = explode(',', $input['tags']);
        }
        else{
            $podcast->syncTags([]);
        }

        $podcast->user_id = Auth::user()->id;
        $podcast->status = $input['status'];
        if($podcast->status == 1){
            $podcast->published_at = Carbon::now();
        }
        $podcast->save();

        return redirect('cfadmin/podcast')->with('alert-success', 'Podcast #'.$podcast->id.' has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $podcast = Podcast::find($id);
        $podcast->status = 0;
        $podcast->save();
        return redirect('cfadmin/podcast')->with('alert-danger', 'Podcast #'.$podcast->id.' has been inactivated.');
    }

    public function active($id){
        $podcast = Podcast::find($id);
        $podcast->status = 1;
        if(empty($podcast->published_at))
        {
            $podcast->published_at = Carbon::now();
        }
        $podcast->save();
        return redirect('cfadmin/podcast')->with('alert-success', 'Podcast #'.$podcast->id.' has been activated.');
    }

}
