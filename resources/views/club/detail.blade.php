@extends('layouts.main')

@section('content')

    {{--<!-- Page Title--}}
		{{--============================================= -->--}}
    {{--<section id="page-title">--}}

        {{--<div class="container clearfix">--}}
            {{--<h1>Club Name</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li class="breadcrumb-item"><a href="#">Home</a></li>--}}
                {{--<li class="breadcrumb-item"><a href="#">Clubs</a></li>--}}
                {{--<li class="breadcrumb-item active" aria-current="page">Club Name</li>--}}
            {{--</ol>--}}
        {{--</div>--}}

    {{--</section><!-- #page-title end -->--}}

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="single-event">

                    <div class="row">
                        <div class="col-lg-9">
                            <div class="entry-image">
                                <a href="#"><img src="{{ asset('images/clubs/'.$club->thumbnail_image)}}"></a>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card events-meta mb-3">
                                <div class="card-header"><h5 class="mb-0">Club Info:</h5></div>
                                <div class="card-body">
                                    <ul class="iconlist nobottommargin">
                                        <img src="{{ asset('images/clubs/'.$club->logo_image)}}" style="margin-bottom: 10px;">
                                        {{--<li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS F Y') }}</li>--}}
                                        <li><i class="icon-location"></i> {{ $club->location }}</li>
                                        <li><i class="icon-facebook"></i><a href="{{ $club->facebook_id }}">{{ $club->name }}</a> </li>
                                        <li><i class="icon-phone"></i> <strong>{{ $club->tel }}</strong></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clear" style="margin-bottom: 30px;"></div>


                    <div class="nobottommargin">
                        <h2>{{ $club->name }}</h2>
                        <div>
                            {!! $club->content !!}
                        </div>
                        <h4>Events Timeline</h4>

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Events</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($club->events as $event)
                                <tr>
                                    <td><span class="badge badge-danger">{{ \Carbon\Carbon::parse($event->eventdate)->format('d M Y') }}</span></td>
                                    <td><a href="{{ url('event/'.$event->id.'/'.str_replace(' ','-',$event->name)) }}">{{ $event->name }}</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                    {{--<div class="col_half nobottommargin col_last">--}}

                        {{--<h4>Gallery</h4>--}}

                        {{--<!-- Events Single Gallery Thumbs--}}
                        {{--============================================= -->--}}
                        {{--<div class="masonry-thumbs grid-4" data-lightbox="gallery">--}}
                            {{--<a href="{{ asset('images/events/1.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/1.jpg') }}" alt="Gallery Thumb 1"></a>--}}
                            {{--<a href="{{ asset('images/events/2.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/2.jpg') }}" alt="Gallery Thumb 2"></a>--}}
                            {{--<a href="{{ asset('images/events/3.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/3.jpg') }}" alt="Gallery Thumb 3"></a>--}}
                            {{--<a href="{{ asset('images/events/4.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/4.jpg') }}" alt="Gallery Thumb 4"></a>--}}
                            {{--<a href="{{ asset('images/events/5.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/5.jpg') }}" alt="Gallery Thumb 5"></a>--}}
                            {{--<a href="{{ asset('images/events/6.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/6.jpg') }}" alt="Gallery Thumb 6"></a>--}}
                            {{--<a href="{{ asset('images/events/7.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/7.jpg') }}" alt="Gallery Thumb 7"></a>--}}
                            {{--<a href="{{ asset('images/events/8.jpg') }}" data-lightbox="gallery-item"><img class="image_fade" src="{{ asset('images/events/thumbs/8.jpg') }}" alt="Gallery Thumb 8"></a>--}}
                        {{--</div><!-- Event Single Gallery Thumbs End -->--}}

                    {{--</div>--}}

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection