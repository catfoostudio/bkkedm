<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Helper;
use App\Mail\FacebookRegister;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;
use Exception;

class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {

        try {
            $user = Socialite::driver('facebook')->stateless()->user();
        } catch (Exception $e) {
            return redirect('/');
        }

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect('/');


    }

    private function findOrCreateUser($fbUser)
    {
        if ($authUser = User::where('facebook_id', $fbUser->id)->first()) {
            return $authUser;
        } elseif ($authUser = User::where('email', $fbUser->email)->first()) {
            $authUser->facebook_id = $fbUser->id;
//            $authUser->facebook_url = $fbUser->user['link'];
            $authUser->save();
            return $authUser;
        }
        else{
            $pass = Helper::generateRandomString(6);
            $user_regist = User::create([
                'email' => $fbUser->email,
                'facebook_id' => $fbUser->id,
                'name' => $fbUser->name,
//                'facebook_url' => $fbUser->user['link'],
                'password' => Hash::make($pass),
                'status' => 1,
            ]);

            $user_regist->assignRole('member');
//            Mail::to($authUser->email)->send(new FacebookRegister($pass));
            return $user_regist;
        }

    }
}
