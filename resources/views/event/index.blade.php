@extends('layouts.main')
@section('content')
    <section id="slider" class="slider-element boxed-slider">
        <div class="container clearfix">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1" data-animate-in="zoomIn" data-speed="450" data-animate-out="fadeOut">
                @foreach($featuredevents as $featuredevent)
                    <a href="{{ url('event/'.$featuredevent->id) }}"><img src="{{ (empty($featuredevent->cover_image)) ? asset('images/eventcover_dummy.png') : asset('images/events/cover_images/'.$featuredevent->cover_image) }}" alt="Slider"></a>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding-bottom: 0px;padding-top: 0px;">
            <div class="container clearfix">
                <h3>DISCOVER EVENTS</h3>
                <div id="posts" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($events as $event)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('event/'.$event->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/events/posters/'.$event->poster_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('event/'.$event->id) }}">{{ $event->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS M Y') }}</li>
                                <li><i class="icon-map-marker"></i>{{ $event->location }}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData('present')">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
            </div>
        </div>
    </section><!-- #content end -->

        <div class="section dark" style="padding-bottom: 0px;padding-top: 40px;margin: 0px 0;">
            <div class="container clearfix">

                <h3>PAST EVENTS</h3>
                <div id="pastposts" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($pastevents as $event)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('event/'.$event->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/events/posters/'.$event->poster_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('event/'.$event->id) }}">{{ $event->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS M Y') }}</li>
                                <li><i class="icon-map-marker"></i>{{ $event->location }}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData('past')">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
                </div>

        </div>
@endsection
@section('script')
    <script>
        var pageNumber = 2;
        var pastPageNumber = 2;

        function loadMoreData(type){
            if(type == 'present') {
                $.ajax({
                    type: 'GET',
                    url: "event?page=" + pageNumber,
                    success: function (data) {
                        pageNumber += 1;
                        if (data.length == 0) {
                            // :( no more articles
                        } else {
                            $('#posts').append(data.html);
                        }
                    }, error: function (data) {
                        console.log('fail to load more event.');
                    },
                })
            }
            else{
                $.ajax({
                    type: 'GET',
                    url: "loadpastevent?page=" + pastPageNumber,
                    success: function (data) {
                        pastPageNumber += 1;
                        if (data.length == 0) {
                            // :( no more articles
                        } else {
                            $('#pastposts').append(data.html);
                        }
                    }, error: function (data) {
                        console.log('fail to load more event.');
                    },
                })
            }
        }
    </script>
@endsection
