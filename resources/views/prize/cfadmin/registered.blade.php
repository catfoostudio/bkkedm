@extends('layouts.cfadmin')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: auto">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Registered Reward #{{ $prize->id }}: {{ $prize->name }}

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/prize') }}">Prize</a></li>
                <li class="active">Registered Prize #{{ $prize->id }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            {{--<div class="row">--}}
            {{--<!-- left column -->--}}
            {{--<div class="col-xs-12">--}}
            {{--<div class="box">--}}
            {{--<div class="box-header">--}}
            {{--<h3 class="box-title">Reward Detail:</h3>--}}
            {{--</div>--}}
            {{--<!-- /.box-header -->--}}
            {{--<div class="box-body">--}}
            {{--<strong>Title:</strong> {{ $prize->name }}<br>--}}
            {{--<strong>Reward:</strong> {{ $prize->reward }}<br>--}}
            {{--<strong>Detail:</strong> {!! $prize->content !!}<br>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                <!-- left column -->
                <div class="col-xs-12">
                    <div class="box box-default">
                        {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Action</h3>--}}
                        {{--</div>--}}
                        <div class="box-body">

                            Number of draw:
                            <div class="row">
                                <div class="col-lg-2">
                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@luckydraw', $prize->id] , 'style' => 'display:inline', 'id' => 'form-gencode']) !!}
                                    <div class="input-group">

                                        <input type="number" id="number" min="0" name="gen_code_amount"
                                               class="form-control">

                                        <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-flat" id="btn-gen-code">Lucky draw!</button>

                            </span>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-lg-10">
                                    <a href="{{ url('cfadmin/prizecode/'.$prize->id) }}" target="_blank">
                                        <button type="button" class="btn btn-default" data-toggle="modal"
                                                data-target="#modal-default">
                                            Redeem codes
                                        </button>
                                    </a>
                                    <a href="{{ url('reward/'.$prize->id) }}" target="_blank">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                                data-target="#modal-info">Reward Info.
                                        </button>
                                    </a>
                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@resetreward' , $prize->id] , 'style=display:inline']) !!}
                                    <button type="button" class="btn btn-danger btn-resetreward" data-toggle="modal"
                                            data-target="#resetreward">Reset Reward
                                    </button>
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-warning btn-sendmsg" data-toggle="modal"
                                            data-target="#sendmsg">Send Message
                                    </button>
                                    <a href="{{ url('cfadmin/registeredprizeexport/'.$prize->id) }}">
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                data-target="#modal-info">Export Excel
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))
                                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                    @endif
                                @endforeach
                            </div> <!-- end .flash-message -->
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Registered list</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>User Name</th>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Registered date</th>
                                </tr>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>User Name</th>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Registered date</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>User Name</th>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Registered date</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($hunters as $hunter)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                {{--                                            <a href="{{ url('cfadmin/lotto/'.$lotto->id) }}"><button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button></a>--}}
                                                @if($hunter->status == 1)
                                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@decline' , $hunter] , 'style' => 'display:inline', 'id' => 'hunter_'.$hunter->id]) !!}
                                                    {!! Form::hidden('hunter_id' , $hunter->id) !!}
                                                    <button style="padding: 2px 10px;" type="button"
                                                            class="btn btn-danger btn-flat btn-delete-post"
                                                            data-hunterid="{{ $hunter->id }}"><i
                                                                class="fa fa-trash"></i></button>
                                                    {!! Form::close() !!}
                                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@reward' , $hunter] , 'style' => 'display:inline', 'id' => 'hunter_'.$hunter->id]) !!}
                                                    {!! Form::hidden('hunter_id' , $hunter->id) !!}
                                                    <button style="padding: 2px 10px;" type="button"
                                                            class="btn btn-success btn-flat btn-reward-post"
                                                            data-hunterid="{{ $hunter->id }}"><i class="fa fa-gift"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                @elseif($hunter->status == 2)
                                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@decline' , $hunter] , 'style' => 'display:inline', 'id' => 'hunter_'.$hunter->id]) !!}
                                                    {!! Form::hidden('hunter_id' , $hunter->id) !!}
                                                    <button style="padding: 2px 10px;" type="button"
                                                            class="btn btn-danger btn-flat btn-delete-post"
                                                            data-hunterid="{{ $hunter->id }}"><i
                                                                class="fa fa-trash"></i></button>
                                                    {!! Form::close() !!}
                                                @else
                                                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@approve' , $hunter] , 'style' => 'display:inline', 'id' => 'hunter_'.$hunter->id]) !!}
                                                    {!! Form::hidden('hunter_id' , $hunter->id) !!}
                                                    <button type="button"
                                                            class="btn btn-warning btn-flat btn-active-post"
                                                            data-hunterid="{{ $hunter->id }}"><i
                                                                class="fa fa-refresh"></i></button>
                                                    {!! Form::close() !!}
                                                @endif
                                                {{--                                            <a href="{{ url('cfadmin/lottocode/'.$lotto->id) }}"><button type="button" class="btn btn-info btn-flat"><i class="fa fa-edit"></i></button></a>--}}
                                            </div>
                                        </td>
                                        <td>{{ $hunter->id }}</td>
                                        <td>{!! $hunter->statusBadge !!}</td>
                                        <td style="width: 100px;">{{ $hunter->user->name }}</td>
                                        <td style="width: 30%;"><a href="{{url('registered/'.$hunter->file)}}"
                                                                   data-toggle="lightbox">{!! $hunter->file !!}</a></td>
                                        <td style="width: 100px;">{{ $hunter->link }}</td>
                                        <td style="width: 100px;">{{ $hunter->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->

                    </div>
                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <div class="gen-code-modal">
        <div class="modal modal-warning" id="confirm-gen-code-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-gen-code">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <div class="delete-modal">
        <div class="modal modal-danger" id="confirm-delete-post-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-delete">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->

    <div class="active-modal">
        <div class="modal modal-warning" id="confirm-active-post-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Active Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-active">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <div class="reward-modal">
        <div class="modal modal-success" id="confirm-reward-post-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Active Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-reward">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <div class="sendmsg-modal">
        <div class="modal modal-warning" id="confirm-sendmsg-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Active Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-sendmsg">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <!-- /.delete-modal -->

    <div class="resetreward-modal">
        <div class="modal modal-danger" id="confirm-resetreward-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Active Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-resetreward">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
@endsection

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('plugins/lightbox/ekko-lightbox.js') }}"></script>

    <script src="{{ asset('plugins/preloaders/jquery.preloaders.min.js') }}"></script>
    <script>
        $(function () {

            $('#btn-gen-code').on('click', function () {
                $('#confirm-gen-code-modal').modal('show');
                $('#confirm-gen-code-modal').find('.modal-title').text('Warning!');
                $('#confirm-gen-code-modal').find('.modal-body').text('Are you sure you want to Lucky draw?');

                $('#confirm-gen-code').click(function () {
                    $('#form-gencode').submit();
                });

            });

            $('.btn-delete-post').on('click', function () {
                $('#confirm-delete-post-modal').modal('show');
                $('#confirm-delete-post-modal').find('.modal-title').text('Warning!');
                $('#confirm-delete-post-modal').find('.modal-body').text('Are you sure you want to Decline this record?');

                var form = $(this).parent();
                $hotel_id = $(this).data('postid');
                $('#confirm-delete').on('click', function () {
                    form.submit();
                });
            });

            $('.btn-active-post').on('click', function () {
                $('#confirm-active-post-modal').modal('show');
                $('#confirm-active-post-modal').find('.modal-title').text('Warning!');
                $('#confirm-active-post-modal').find('.modal-body').text('Are you sure you want to reset status this record?');

                var form = $(this).parent();
                $hotel_id = $(this).data('postid');
                $('#confirm-active').on('click', function () {
                    form.submit();
                });
            });

            $('.btn-reward-post').on('click', function () {
                $('#confirm-reward-post-modal').modal('show');
                $('#confirm-reward-post-modal').find('.modal-title').text('Warning!');
                $('#confirm-reward-post-modal').find('.modal-body').text('Are you sure you want to reward this record?');

                var form = $(this).parent();
                $('#confirm-reward').on('click', function () {
                    form.submit();
                });
            });


            $('.btn-sendmsg').on('click', function () {
                $('#confirm-sendmsg-modal').modal('show');
                $('#confirm-sendmsg-modal').find('.modal-title').text('Warning!');
                $('#confirm-sendmsg-modal').find('.modal-body').text('Are you sure to Send Message to all Winner?');

                var form = $(this).parent();
                $('#confirm-sendmsg').on('click', function () {


                    $.preloader.start({
                        modal: true,
                    });
                    $('#confirm-sendmsg-modal').modal('hide');
                    $.ajax({
                        type: "GET",
                        url: "{{ url('cfadmin/sendToWon/'.$prize->id) }}",
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        success: function (result) {
                            if(result == 'send'){
                                $.preloader.stop();
                            }
                        }
                    });

                });
            });

            $('.btn-resetreward').on('click', function () {
                $('#confirm-resetreward-modal').modal('show');
                $('#confirm-resetreward-modal').find('.modal-title').text('Warning!');
                $('#confirm-resetreward-modal').find('.modal-body').text('Are you sure to want reset ALL reward status?');

                var form = $(this).parent();
                $('#confirm-resetreward').on('click', function () {
                    form.submit();
                });
            });

            // Setup - add a text input to each header cell
            $('#example1 thead tr:eq(1) th').each(function () {
                var title = $('#example1 thead tr:eq(0) th').eq($(this).index()).text();
                if (title == 'ID')
                    $(this).html('<input type="text" placeholder="' + title + '" style="width: 50px;" />');
                else
                    $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            var table = $('#example1').DataTable({
                orderCellsTop: true,
                "sScrollX": "100%",
                "sScrollXInner": "100%",
                "bScrollCollapse": true,
                "pageLength": 50,
                "columnDefs": [
                    {"width": "5%", "targets": 0},
                    {
                        "width": "5%",
                        "targets": [1],
                        "visible": false
                    },
                    {"width": "5%", targets: 2},
                ],
                "order": [[5, "desc"]],
            });

            // Apply the search
            table.columns().every(function (index) {
                $('#example1_wrapper thead tr:eq(1) th:eq(' + index + ') input').on('keyup change', function () {

                    table.column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
                });
            });

            $('#example1 tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });


            {{--$('#example1 tbody').on('dblclick', 'tr', function () {--}}

            {{--var data = table.row( this ).data();--}}
            {{--window.location = '{{ url('cfadmin/lifestyle/') }}/'+data[1];--}}
            {{--//                alert( 'You clicked on '+data[0]+'\'s row' );--}}
            {{--} );--}}

            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });

            // Select your input element.
            var number = document.getElementById('number');

// Listen for input event on numInput.
            number.onkeydown = function (e) {
                if (!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {
                    return false;
                }
            }

        });


    </script>

@endsection
