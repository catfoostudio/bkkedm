<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>cfAdmin2 - BKK EDM.com</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/skin-blue.min.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
{{--    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">--}}
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" type="text/css"/>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/lightbox/ekko-lightbox.css') }}">
    <style>
        .bootstrap-tagsinput {
            width: 100% !important;
            display: block;
            margin: auto 0;
        }
        .ckeditor-error{
            border: orangered solid 1px;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('cfadmin/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>cfA</b>2</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>cf</b>Admin</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ (!empty(Auth::user()->avatar)) ? asset('images/profile/'.Auth::user()->avatar) : asset('images/avicii.jpeg') }}"
                                 class="user-image"
                                 alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ (!empty(Auth::user()->avatar)) ? asset('images/profile/'.Auth::user()->avatar) : asset('images/avicii.jpeg') }}"
                                     class="img-circle"
                                     alt="User Image">

                                <p>
                                    {{ Auth::user()->name }}
                                    <small>Member
                                        since {{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('M. Y') }}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                        {{--<li class="user-body">--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-xs-4 text-center">--}}
                        {{--<a href="#">Followers</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-4 text-center">--}}
                        {{--<a href="#">Sales</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-4 text-center">--}}
                        {{--<a href="#">Friends</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /.row -->--}}
                        {{--</li>--}}
                        <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('cfadmin/profile') }}" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <form id="frm-logout" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                        <div>Logout</div>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    {{--<li>--}}
                        {{--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ (!empty(Auth::user()->avatar)) ? asset('images/profile/'.Auth::user()->avatar) : asset('images/avicii.jpeg') }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- search form (Optional) -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
        {{--<div class="input-group">--}}
        {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
        {{--<span class="input-group-btn">--}}
        {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
        {{--</button>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header" id="clock" style="color: white;font-size: 16px;text-align: center"></li>
                <!-- Optionally, you can add icons to the links -->
                <li {{ Request::is('cfadmin/dashboard*') || Request::is('cfadmin') ? 'class=active' : '' }}><a href="#"><i
                                class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                {{--<li {{ Request::is('cfadmin/music*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/music') }}"><i--}}
                {{--class="fa fa-music"></i> <span>Music</span></a></li>--}}
                @if(Auth::user()->hasRole(['super-admin','admin','moderator','writer']))
                    <li class="treeview{{ Request::is('cfadmin/music*') || Request::is('cfadmin/topstories*') || Request::is('cfadmin/categorytype/music') ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-music"></i> <span>Music</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ Request::is('cfadmin/music*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/music') }}">Music</a></li>
                            <li {{ Request::is('cfadmin/topstories*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/topstories') }}">Top Music</a></li>
                            <li {{ Request::is('cfadmin/categorytype/music') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/categorytype/music') }}">Categories</a></li>
                        </ul>
                    </li>
                    <li class="treeview{{ Request::is('cfadmin/lifestyle*') || Request::is('cfadmin/lifetopstories') || Request::is('cfadmin/categorytype/lifestyle') ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-coffee"></i> <span>Lifestyle</span>
                            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ Request::is('cfadmin/lifestyle*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/lifestyle') }}">Lifestyle</a></li>
                            <li {{ Request::is('cfadmin/lifetopstories') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/lifetopstories') }}">Top Lifestyle</a></li>
                            <li {{ Request::is('cfadmin/categorytype/lifestyle') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/categorytype/lifestyle') }}">Categories</a></li>
                        </ul>
                    </li>
                    <li class="treeview{{ Request::is('cfadmin/podcast*') || Request::is('cfadmin/categorytype/podcast') ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-music"></i> <span>Podcasts</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ Request::is('cfadmin/podcast*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/podcast') }}">Podcasts</a></li>
                            <li {{ Request::is('cfadmin/categorytype/podcast') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/categorytype/podcast') }}">Categories</a></li>
                        </ul>
                    </li>
                    <li class="treeview{{ Request::is('cfadmin/video*') || Request::is('cfadmin/categorytype/video') || Request::is('cfadmin/videoslide*') ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-video-camera"></i> <span>Videos</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ Request::is('cfadmin/video*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/video') }}">Videos</a></li>
                            <li {{ Request::is('cfadmin/categorytype/video') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/categorytype/video') }}">Categories</a></li>
                            {{--<li {{ Request::is('cfadmin/videoslide*') ? 'class=active' : '' }}><a--}}
                                        {{--href="{{ url('cfadmin/videoslide') }}">Slide</a></li>--}}
                        </ul>
                    </li>
                @endif
                @if(Auth::user()->hasRole(['super-admin','admin','moderator','event']))
                    <li class="treeview{{ Request::is('cfadmin/event*') || Request::is('cfadmin/featuredevent') ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-calendar"></i> <span>Event</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ Request::is('cfadmin/event*') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/event') }}">Event list</a></li>
                            @if(Auth::user()->hasRole(['super-admin','admin','moderator']))
                            <li {{ Request::is('cfadmin/featuredevent') ? 'class=active' : '' }}><a
                                        href="{{ url('cfadmin/featuredevent') }}">Featured</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['super-admin','admin','moderator','club']))
                    <li {{ Request::is('cfadmin/club*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/club') }}"><i
                                    class="fa fa-moon-o"></i> <span>Club</span></a></li>
                @endif
                @if(Auth::user()->hasRole(['super-admin','admin','moderator']))
                    <li {{ Request::is('cfadmin/aboutus*') ? 'class=active' : '' }}><a
                                href="{{ url('cfadmin/aboutus') }}"><i
                                    class="fa fa-users"></i> <span>About us</span></a></li>
                    {{--<li {{ Request::is('cfadmin/contact*') ? 'class=active' : '' }}><a--}}
                                {{--href="{{ url('cfadmin/contact') }}"><i--}}
                                    {{--class="fa fa-address-card"></i> <span>Contact</span></a></li>--}}
                    <li {{ Request::is('cfadmin/job*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/job') }}"><i
                                    class="fa fa-briefcase"></i> <span>Job</span></a></li>
                    {{--                <li {{ Request::is('cfadmin/lotto*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/lotto') }}"><i--}}
                    {{--class="fa fa-gift"></i> <span>Lotto</span></a></li>--}}
                    <li {{ Request::is('cfadmin/prize*') ? 'class=active' : '' }}><a
                                href="{{ url('cfadmin/prize') }}"><i
                                    class="fa fa-usd"></i> <span>Reward</span></a></li>
                    <li {{ Request::is('cfadmin/vote*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/vote') }}"><i
                                    class="fa fa-bar-chart"></i> <span>Vote</span></a></li>
                    <li {{ Request::is('cfadmin/contest*') ? 'class=active' : '' }}><a
                                href="{{ url('cfadmin/contest') }}"><i
                                    class="fa fa-music"></i> <span>Contest</span></a></li>
                    <li {{ Request::is('cfadmin/member*') ? 'class=active' : '' }}><a
                                href="{{ url('cfadmin/member') }}"><i
                                    class="fa fa-user-circle"></i> <span>Member</span></a></li>
                @endif
                @if(Auth::user()->hasRole(['super-admin','admin','moderator','club','event']))
                    <li {{ Request::is('cfadmin/redeem*') ? 'class=active' : '' }}><a
                                href="{{ url('cfadmin/redeem') }}"><i
                                    class="fa fa-barcode"></i> <span>Redeem</span></a></li>
                @endif
                @if(Auth::user()->hasRole(['super-admin','admin']))
                    <li {{ Request::is('cfadmin/user*') ? 'class=active' : '' }}><a href="{{ url('cfadmin/user') }}"><i
                                    class="fa fa-users"></i> <span>User</span></a></li>
                @endif

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

@yield('content')

<!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
{{--<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>--}}
{{--<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>--}}
<!-- jQuery Knob Chart -->
{{--<script src="{{ asset('adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>--}}
<!-- Bootstrap WYSIHTML5 -->
{{--<script src="{{ asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}} "></script>--}}
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

{{--<script src="{{ asset('adminlte/dist/js/pages/dashboard.js')}}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js')}}"></script>
@yield('script')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script>
    // var timeA = moment().add(10, 'seconds'); // 10 seconds from now

    var tmr = setInterval(function(){
        var now = moment().format('MMMM Do YYYY , h:mm:ss a');

        // var then = timeA.unix();
        $('#clock').html(now);
        // if (now == then) {
        //     clearInterval(tmr);
        //     console.log('Whatever you want to do');
        // }
    }, 1000);
</script>
</body>
</html>