@foreach($events as $event)
    <option value="{{ $event->id }}"
            data-image="{{ asset('images/events/posters/'.$event->poster_image) }}"
            data-name="{{$event->name}}">{{ $event->id }}:
        - {{ $event->name }}</option>
@endforeach