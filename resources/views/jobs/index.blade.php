@extends('layouts.main')

@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('images/about/parallax.jpg'); padding: 120px 0;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">

        <div class="container clearfix">
            <h1>Job Openings</h1>
            <span>Join our Fabulous Team of Intelligent Individuals</span>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="col_three_fifth nobottommargin">

                    @foreach($jobs as $job)
                    <div class="fancy-title title-bottom-border">
                        <h3>{{ $job->name }}</h3>
                    </div>

                    <p>{{ $job->description }}</p>

                    <div class="accordion accordion-bg clearfix">

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Requirements</div>
                        <div class="acc_content clearfix">
                            {!! $job->requirement !!}
                        </div>
                    </div>

                    <div class="divider divider-short"><i class="icon-star3"></i></div>
                    @endforeach
                </div>

                <div class="col_two_fifth nobottommargin col_last">

                    <div id="job-apply" class="heading-block highlight-me">
                        <h2>Apply Now</h2>

                        <span>Send your resume to <a href="mailto:bkkedm@gmail.com">bkkedm@gmail.com</a> <br>And we'll get back to you within 48 hours.</span>
                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection