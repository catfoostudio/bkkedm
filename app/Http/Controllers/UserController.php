<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Club;
use App\Models\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:admin|super-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if($this->isAdminRequest()){
                $view = 'user.cfadmin.index';
                $users = User::role(['admin','writer','moderator','club','event'])->get();
                return view($view, [
                    'users' => $users,
                ]);
        }
        else{

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if($this->isAdminRequest()){
            $view = 'user.cfadmin.create';
//            $users = User::role(['admin','super-admin'])->get();
            return view($view, [
                'clubs' => Club::all(),
                'events' => Event::all(),
            ]);
        }
        else{

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $validate = [
            'firstname' => 'required|string|unique:users,firstname,NULL,id,lastname,' . $input['lastname'],
            'lastname' => 'required|string|max:255',
            'email_register' => 'required|string|email|max:255|unique:users,email',
            'password_register' => 'required|string|min:8|required_with:password_register|same:password_confirmation',
            'password_confirmation' => 'min:8',
            'mobile' => 'required|digits:10|unique:users,mobile',
            'birthday' => 'required|before:' . date('Y-m-d'),
        ];

        // Static rules that don't change
        $v = Validator::make($request->all(), $validate);

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        else{

        }

        $user = User::create([
            'name' => $input['firstname'].' '.$input['lastname'],
            'email' => $input['email_register'],
            'password' => Hash::make($input['password_register']),
            'activate_code' => time().'_'.Helper::generateRandomString(30),
            'mobile' => $input['mobile'],
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'birthday' => Carbon::createFromFormat('d/m/Y',$input['birthday'])->format('Y-m-d'),
            'status' => 1,
            'quote' => (!empty($input['quote'])) ? $input['quote'] : ''
        ]);

        if($input['role'] == 'club'){
            $user->club_id = $input['club'];
        }
//        elseif($input['role'] == 'event'){
//            $user->event_id = $input['event'];
//        }

        $user->assignRole($input['role']);
        $user->save();

        return redirect('cfadmin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($this->isAdminRequest()){
            $view = 'user.cfadmin.detail';
            $user = User::find($id);

            return view($view, [
                'clubs' => Club::all(),
                'events' => Event::all(),
                'user' => $user,
            ]);
        }
        else{

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'firstname' => 'required|string|unique:users,firstname,'.$id.',id,lastname,' . $input['lastname'],
            'lastname' => 'required|string|max:255',
            'mobile' => 'required|digits:10|unique:users,mobile,'.$id,
            'birthday' => 'required|before:' . date('Y-m-d'),
        ];

        // Static rules that don't change
        $v = Validator::make($request->all(), $validate);

        $v->sometimes('password_register', 'required|string|min:8|required_with:password_register|same:password_confirmation', function ($input) {
            return !empty($input->password_register);
        });

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        else{

        }

        $user = User::find($id);
        $user->name = $input['firstname'].' '.$input['lastname'];
        $user->firstname = $input['firstname'];
        $user->lastname = $input['lastname'];
        $user->mobile = $input['mobile'];
        $user->birthday = Carbon::createFromFormat('d/m/Y',$input['birthday'])->format('Y-m-d');
        $user->quote = (!empty($input['quote'])) ? $input['quote'] : '';

        if($input['role'] == 'club'){
            $user->club_id = !empty($input['club']) ? $input['club'] : null;
        }
        elseif($input['role'] == 'event'){
            $user->event_id = !empty($input['event']) ? $input['event'] : null;
        }

        $user->syncRoles([$input['role']]);
        $user->save();

        return redirect('cfadmin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
