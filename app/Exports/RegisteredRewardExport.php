<?php

namespace App\Exports;

use App\Models\Prize;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RegisteredRewardExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */



    public function __construct(int $id)
    {
        $this->prizeId = $id;
    }

    public function query()
    {
        return Prize::find($this->prizeId)->users();
    }

    public function map($user): array
    {
        $status = '';
        if($user->pivot->status == 0){
            $status = 'Decline';
        }
        elseif($user->pivot->status == 1){
            $status = 'Registered';
        }
        elseif($user->pivot->status == 2){
            $status = 'Won';
        }

        return [
            $status,
            $user->name,
            $user->email,
            $user->mobile,
            $user->pivot->link,
            $user->pivot->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'Status',
            'Name',
            'Email',
            'Mobile',
            'Link',
            'Registered Date'
        ];
    }
}
