{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('login') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<div class="form-check">--}}
                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                    {{--<label class="form-check-label" for="remember">--}}
                                        {{--{{ __('Remember Me') }}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-8 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Login') }}--}}
                                {{--</button>--}}
                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--{{ __('Forgot Your Password?') }}--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Login - Layout 4 | Canvas</title>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap nopadding">

            <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: #444;"></div>

            <div class="section nobg full-screen nopadding nomargin">
                <div class="container-fluid vertical-middle divcenter clearfix">

                    <div class="center">
                        <a href="index.html"><img style="height: 70px;" src="{{ asset('images/logo/logo.png') }}" ></a>
                    </div>

                    <div class="card divcenter noradius noborder" style="max-width: 400px;">
                        <div class="card-body" style="padding: 40px;">
                            {{--<form id="login-form" name="login-form" class="nobottommargin" action="#" method="post">--}}
                            {!! Form::open(array('method'=>'post','action' => 'Auth\LoginController@login','id' => 'login-form', 'name'=> 'login-form', 'class'=>'nobottommargin' )); !!}
                                    @csrf
                                <h3>Login to your Account</h3>

                                <div class="col_full">
                                    <label for="login-form-username">Username:</label>
                                    {{--<input type="text" id="login-form-username" name="login-form-username" value="" class="form-control not-dark" />--}}
                                    <input id="email" type="email" class="not-dark form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                </div>

                                <div class="col_full">
                                    <label for="login-form-password">Password:</label>
                                    {{--<input type="password" id="login-form-password" name="login-form-password" value="" class="form-control not-dark" />--}}
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                </div>

                                <div class="col_full nobottommargin">
                                    {{--<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>--}}
                                    <button type="submit" class="button button-3d button-black nomargin">
                                    {{ __('Login') }}
                                    </button>
                                    {{--<a href="#" class="fright">Forgot Password?</a>--}}
                                    <a class="fright" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            </form>

                            {{--<div class="line line-sm"></div>--}}

                            {{--<div class="center">--}}
                                {{--<h4 style="margin-bottom: 15px;">or Login with:</h4>--}}
                                {{--<a href="#" class="button button-rounded si-facebook si-colored">Facebook</a>--}}
                                {{--<span class="d-none d-md-block">or</span>--}}
                                {{--<a href="#" class="button button-rounded si-twitter si-colored">Twitter</a>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    <div class="center dark"><small>Copyrights &copy; All Rights Reserved by Catfoostudio.</small></div>

                </div>
            </div>

        </div>

    </section><!-- #content end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="{{ asset('js/jquery.js')}}"></script>
<script src="{{ asset('js/plugins.js')}}"></script>

<!-- Footer Scripts
============================================= -->
<script src="{{ asset('js/functions.js')}}"></script>

</body>
</html>
