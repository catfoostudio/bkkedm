<footer id="footer" class="dark" >

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix" style="padding: 10px 0;">

            <div class="col_half" style="margin-bottom: 10px !important;">

                <div class="widget clearfix">

                    <img src="{{ asset('images/logo/logo.png')}}" style="height: 50px;margin-bottom: 5px;margin-left: -30px;" class="footer-logo">

                    <p>Bringing you the best of electronic and dance music, straight outta Bangkok.</p>

                    {{--<div class="clearfix" style="padding: 10px 0; background: url('images/world-map.png') no-repeat center center;">--}}
                        {{--<div class="col_half">--}}
                            {{--<address class="nobottommargin">--}}
                                {{--<abbr title="Headquarters" style="display: inline-block;margin-bottom: 7px;"><strong>Headquarters:</strong></abbr><br>--}}
                                {{--795 Folsom Ave, Suite 600<br>--}}
                                {{--San Francisco, CA 94107<br>--}}
                            {{--</address>--}}
                        {{--</div>--}}
                        {{--<div class="col_half col_last">--}}
                            {{--<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>--}}
                            {{--<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>--}}
                            {{--<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <a target="_blank" href="https://www.facebook.com/BKKEDM/" class="social-icon si-small si-rounded si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    {{--<a target="_blank" href="https://twitter.com/bkkedm/media" class="social-icon si-small si-rounded si-twitter">--}}
                        {{--<i class="icon-twitter"></i>--}}
                        {{--<i class="icon-twitter"></i>--}}
                    {{--</a>--}}

                    <a target="_blank" href="https://www.youtube.com/channel/UCNnA2Mnyn-Fk0aNqP-rF2iw" class="social-icon si-small si-rounded si-gplus">
                        <i class="icon-youtube"></i>
                        <i class="icon-youtube"></i>
                    </a>

                    <a target="_blank" href="https://www.instagram.com/bkkedm/" class="social-icon si-small si-rounded si-pinterest">
                        <i class="icon-instagram"></i>
                        <i class="icon-instagram"></i>
                    </a>
                </div>

            </div>

            <div class="col_one_fourth" style="margin-bottom: 10px !important;">

                <div class="widget widget_links clearfix">
                    <ul>
                        <li><a href="{{ url('aboutus') }}">About Us</a></li>
                        {{--<li><a href="http://wordpress.org/support/forum/requests-and-feedback">Our Team</a></li>--}}
                        <li><a href="{{ url('contactus') }}">Contact Us</a></li>
                        <li><a href="{{ url('jobs') }}">Jobs</a></li>
                        {{--<li><a href="{{ url('advertisting') }}">Our Services</a></li>--}}
                    </ul>

                </div>

            </div>

            <div class="col_one_fourth col_last" style="margin-bottom: 10px !important;">

                <div class="widget widget_links clearfix">
                    <ul>
{{--                        <li><a href="{{ url('advertisting') }}">Our Services</a></li>--}}
                        {{--<li><a href="{{ url('terms') }}">Terms & Conditions</a></li>--}}
                        {{--<li><a href="{{ url('policy') }}">Privacy Policy</a></li>--}}
                    </ul>

                </div>

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights" style="padding: 10px 0;background-color: #000">

        <div class="container clearfix">

            <div class="col_full nobottommargin center">
                {{--<div class="copyrights-menu copyright-links clearfix">--}}
                    {{--<a href="#">Home</a>/<a href="#">About</a>/<a href="#">Features</a>/<a href="#">Portfolio</a>/<a href="#">FAQs</a>/<a href="#">Contact</a>--}}
                {{--</div>--}}
                Copyrights © 2019 All Rights Reserved by BKK EDM.
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->