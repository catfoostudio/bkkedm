<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;

class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if($this->isAdminRequest()){
            $view = 'aboutus.cfadmin.detail';
            $config = Config::where('name' , 'aboutus_1')->first();
            $config2 = Config::where('name' , 'aboutus_2')->first();
            $config3 = Config::where('name' , 'aboutus_3')->first();

        }
        else{
            $view = 'aboutus.index';
            $config = Config::where('name' , 'aboutus_1')->first();
            $config2 = Config::where('name' , 'aboutus_2')->first();
            $config3 = Config::where('name' , 'aboutus_3')->first();
        }

        if(empty($config)){
            return abort('404');
        }

        return view($view, [
            'config' => $config,
            'config2' => $config2,
            'config3' => $config3,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $config = Config::where('name', 'aboutus_1')->first();
        $config2 = Config::where('name', 'aboutus_2')->first();
        $config3 = Config::where('name', 'aboutus_3')->first();

        $input = $request->input();

        $config->string_value = $input['about1_name'];
        $config->text_value = $input['about1_desc'];
        $config->save();

        $config2->string_value = $input['about2_name'];
        $config2->text_value = $input['about2_desc'];
        $config2->save();

        $config3->string_value = $input['about3_name'];
        $config3->text_value = $input['about3_desc'];
        $config3->save();

        $request->session()->flash('alert-success', 'Aboutus was successful updated!');

        return redirect('cfadmin/aboutus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
