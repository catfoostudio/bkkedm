@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-event">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">
                            <div class="entry-image nobottommargin">
                                <img src="{{ asset('images/votes/'.$vote->thumbnail_image) }}">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-12">
                            <div class="card events-meta mb-3">

                                <div class="card-body" style="background-color: #333;color: white">
                                    <ul class="iconlist nobottommargin">
                                        <h3 style="color: white">{{ $vote->name }}</h3>
                                        <li>
                                            <i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($vote->start)->format('d M Y') }}
                                            - {{ \Carbon\Carbon::parse($vote->end)->format('d M Y') }}</li>
                                        {{--<li><i class="icon-gift"></i>Reward</li>--}}
                                        {{--<li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($lotto->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($lotto->end)->format('d M Y') }}</li>--}}
                                    </ul>
                                </div>
                            </div>
                            <div class="fancy-title title-border">
                                <h3>Description</h3>
                            </div>
                            {!! $vote->content !!}
                            {{--@if(Auth::check())--}}
                            {{--@if(empty($hunter))--}}
                            {{--{!! Form::open(['method' => 'POST' , 'action' => ['VoteController@choose' , $vote],'id'=>'my-form-id']) !!}--}}
                            {{--{!! NoCaptcha::displaySubmit('my-form-id', 'Register', ['class' => 'btn btn-danger btn-block btn-lg']) !!}--}}
                            {{--{!! Form::close() !!}--}}
                            {{--@else--}}
                            {{--<button class="btn btn-danger btn-block btn-lg disabled">Registered</button>--}}
                            {{--@endif--}}
                            {{--@else--}}
                            {{--<a href="{{ route('signin') }}"><button class="btn btn-danger btn-block btn-lg">Sign In to register</button></a>--}}
                            {{--@endif--}}
                        </div>

                    </div>
                    <div class="fancy-title title-border" style="margin-bottom: 10px;">
                        <h3>Vote</h3><br>
                        @if($errors->has('textlist'))<h4><strong style="color:red;">กรุณาตรวจสอบอีเมลอีกครั้ง
                                อีเมลไม่ถูกต้อง หรือถูกใช้งานแล้ว</strong></h4>
                        @elseif($errors->has('checkbox'))<strong
                                style="color:red;">กรุณาเลือกอย่างน้อยหนึ่งตัวเลือก</strong>
                        @elseif($errors->has('thumbnail'))<strong style="color:red;">กรุณาตรวจสอบอีเมลอีกครั้ง
                            อีเมลไม่ถูกต้อง หรือถูกใช้งานแล้ว</strong>
                        @elseif($errors->has('inputbox.*'))<strong
                                style="color:red;">กรุณากรอกตัวเลือกให้ครบทุกช่อง และห้ามซ้ำ</strong>
                        @endif
                        {{--@if(Auth::check())--}}


                        {{--@endif--}}
                    </div>

                {!! Form::open(['method' => 'POST' , 'action' => ['VoteController@choose', $vote],'id'=>'vote-form']) !!}
                {!! Form::hidden('vote', $vote->id) !!}
                <?php $checked = true;?>
                <!-- VOTE TEXTLIST -->
                    @if($vote->type == 1)
                        @if($isBeforeVote)
                            <h4>Please waiting until start date</h4>
                        @elseif($isPastVote)
                            <h4>Voting has closed</h4>
                        @elseif(Auth::check() && !$voted)
                        <div class="row">
                            @foreach($vote->choices as $choice)
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                                    <div class="col-12">
                                        <div class="form-check" style="margin-bottom: 10px">
                                            <input class="form-check-input" type="radio" value="{{$choice->id}}"
                                                   name="textlist"
                                                   id="choice_{{$choice->id}}" {{ ($checked) ? 'checked=checked' : '' }}>
                                            <label class="form-check-label" for="choice_{{$choice->id}}">
                                                {{$choice->name}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php $checked = false;?>
                            @endforeach
                        </div>
                        <div class="clear"></div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-3">
                                <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                            </div>
                        </div>
                        @elseif(Auth::check() && $voted)
                            <div class="row">
                            @foreach($vote->choices as $choice)
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                                    <div class="col-12">
                                        <div class="form-check" style="margin-bottom: 10px">
                                            <input class="form-check-input" type="radio" value="{{$choice->id}}"
                                                   name="textlist"
                                                   id="choice_{{$choice->id}}" {{ ($userChooses->first()->id == $choice->id) ? 'checked=checked' : '' }}
                                            disabled="disabled" >
                                            <label class="form-check-label" for="choice_{{$choice->id}}">
                                                {{$choice->name}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <? $checked = false;?>
                            @endforeach
                            </div>
                            <div class="clear"></div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-3">
                                    <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                                </div>
                            </div>
                        @endif
                        <!-- VOTE CHECKBOX -->
                    @elseif($vote->type == 2)
                        @if($isBeforeVote)
                            <h4>Please waiting until start date</h4>
                        @elseif($isPastVote)
                            <h4>Voting has closed</h4>
                        @elseif(Auth::check() && !$voted)
                        <div class="row">
                            @foreach($vote->choices as $choice)

                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 10px;">

                                    <div class="col-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{$choice->id}}"
                                                   name="checkbox[]" id="choice_{{$choice->id}}">
                                            <label class="form-check-label" for="choice_{{$choice->id}}">
                                                {{$choice->name}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="clear"></div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                            </div>
                        </div>
                        @elseif(Auth::check() && $voted)
                            <div class="row">
                                @foreach($vote->choices as $choice)

                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 10px;">

                                        <div class="col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="{{$choice->id}}"
                                                       name="checkbox[]" id="choice_{{$choice->id}}" disabled="disabled" {{ (in_array($choice->id,$userChooses->pluck('id')->toArray())) ? 'checked=checked' : '' }}>
                                                <label class="form-check-label" for="choice_{{$choice->id}}">
                                                    {{$choice->name}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="clear"></div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                                </div>
                            </div>
                        @endif
                        <!-- VOTE THUMBNAIL WITH TEXT -->
                    @elseif($vote->type == 3)
                        @if($isBeforeVote)
                            <h4>Please waiting until start date</h4>
                        @elseif($isPastVote)
                            <h4>Voting has closed</h4>
                        @elseif(Auth::check() && !$voted)
                        <div class="row">
                            {!! Form::hidden('choice',0) !!}
                            @foreach($vote->choices as $choice)
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 30px;">
                                    <div class="col-12" style="text-align: center">
                                        <img src="{{ asset($choice->content)}}"
                                             style="width: 100%;height: 300px;object-fit: cover;{{ (!Auth::check() || $voted || $isPastVote) ? 'pointer-events:none;opacity:0.3;' : '' }}"/><br>
                                        <button data-choice="{{$choice->id}}" class="btn btn-dark choice-item"
                                                style="margin-top:5px;width: 100%;{{ (!Auth::check() || $voted || $isPastVote) ? 'pointer-events:none;opacity:0.3;' : '' }}">{{$choice->name}}</button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @elseif(Auth::check() && $voted)
                            <div class="row">
                                {!! Form::hidden('choice',0) !!}
                                @foreach($vote->choices as $choice)
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 30px;">
                                        <div class="col-12" style="text-align: center">
                                            <img src="{{ asset($choice->content)}}"
                                                 style="width: 100%;height: 300px;object-fit: cover; {{ ($userChooses->first()->id != $choice->id) ? 'pointer-events:none;opacity:0.3;' : '' }}"/><br>
                                            <button data-choice="{{$choice->id}}" class="btn btn-dark choice-item"
                                                    style="margin-top:5px;width: 100%;{{ ($userChooses->first()->id != $choice->id) ? 'pointer-events:none;opacity:0.3;' : '' }}" disabled="disabled">{{$choice->name}}</button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <!-- VOTE INPUTBOX -->
                    @elseif($vote->type == 4)
                        @if($isBeforeVote)
                            <h4>Please waiting until start date</h4>
                        @elseif($isPastVote)
                            <h4>Voting has closed</h4>
                        @elseif(Auth::check() && !$voted)
                            @for($i=0;$i<$vote->input_amount;$i++)
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-4">
                                        <input type="text" name="inputbox[]"
                                               class="form-control {{$errors->has('inputbox.'.$i) ? 'error' : '' }}"
                                               value="{{ old('inputbox.'.$i) }}" >
                                    </div>
                                </div>
                                <div class="clear"></div>
                            @endfor
                            <div class="row">
                                <div class="col-3">
                                    <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                                </div>
                            </div>
                        @elseif(Auth::check() && $voted)
                            @for($i=0;$i<count($userChooses);$i++)
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-4">
                                        <input type="text" name="inputbox[]"
                                               class="form-control {{$errors->has('inputbox.'.$i) ? 'error' : '' }}"
                                               value="{{ old('inputbox.'.$i,$userChooses[$i]->name) }}" disabled="disabled" >
                                    </div>
                                </div>
                                <div class="clear"></div>
                            @endfor
                            <div class="row">
                                <div class="col-3">
                                    <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                                </div>
                            </div>
                        @endif
                    @endif
                    {!! Form::close() !!}
                    {{--<div class="col_three_fourth">--}}
                    {{--<div style="padding: 30px 0;">--}}
                    {{--<div class="fancy-title title-border">--}}
                    {{--<h3>Description</h3>--}}
                    {{--</div>--}}
                    {{--{!! $vote->content !!}--}}
                    {{--<!-- Tag Cloud--}}
                    {{--============================================= -->--}}
                    {{--@if(!empty(count($vote->tags)))--}}
                    {{--<div class="tagcloud clearfix bottommargin">--}}
                    {{--@foreach($vote->tags as $tag)--}}
                    {{--<a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>--}}
                    {{--@endforeach--}}
                    {{--</div><!-- .tagcloud end -->--}}
                    {{--@endif--}}
                    {{--<div class="clear"></div>--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clear"></div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            @if(Auth::check())
            $('.choice-item').click(function (event) {
                event.preventDefault();
                $('input[name="choice"]').val($(this).data('choice'));

                $('#vote-form').submit();
            });
            @endif
        });
    </script>
@endsection