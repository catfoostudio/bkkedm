@extends('layouts.main')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/et-line.css') }}" type="text/css" />
    <!-- Page Title
    ============================================= -->
    {{--<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 150px 0; background-image: url('{{ asset('images/about/cover_aboutus.jpg') }}'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -500px;">--}}

        {{--<div class="container clearfix" >--}}
            {{--<h1>About Us</h1>--}}
            {{--<span>Everything you need to know about us</span>--}}
        {{--</div>--}}

    {{--</section>--}}
    <section id="page-title">
        <div class="container clearfix">
            <h1>About Us</h1>
            <span>Learn About us</span>
        </div>
    </section><!-- #page-title end -->
    <!-- #page-title end -->

    <!-- Content
    ============================================= -->
    {{--<section id="content">--}}

        {{--<div class="content-wrap">--}}

            {{--<div class="container clearfix">--}}

                {{--<div class="col_one_third">--}}

                    {{--<div class="heading-block fancy-title nobottomborder title-bottom-border">--}}
                        {{--<h4>{{ $config->string_value }}</h4>--}}
                    {{--</div>--}}
                    {{--{!! $config->text_value  !!}--}}
                {{--</div>--}}

                {{--<div class="col_one_third">--}}

                    {{--<div class="heading-block fancy-title nobottomborder title-bottom-border">--}}
                        {{--<h4>{{ $config2->string_value }}</h4>--}}
                    {{--</div>--}}
                    {{--{!! $config2->text_value  !!}--}}
                {{--</div>--}}

                {{--<div class="col_one_third col_last">--}}

                    {{--<div class="heading-block fancy-title nobottomborder title-bottom-border">--}}
                        {{--<h4>{{ $config3->string_value }}</h4>--}}
                    {{--</div>--}}
                    {{--{!! $config3->text_value  !!}--}}
                {{--</div>--}}

            {{--</div>--}}

            {{--<div class="container clearfix">--}}

                {{--<div class="clear"></div>--}}

                {{--<div class="heading-block center">--}}
                    {{--<h4>Our Clients</h4>--}}
                {{--</div>--}}

                {{--<ul class="clients-grid grid-6 nobottommargin clearfix">--}}
                    {{--<li><a href="http://logofury.com/logo/enzo.html"><img src="images/clients/1.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com"><img src="images/clients/2.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofaves.com/2014/03/grabbt/"><img src="images/clients/3.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofaves.com/2014/01/ladera-granola/"><img src="images/clients/4.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofaves.com/2014/02/hershel-farms/"><img src="images/clients/5.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/logo/food-fight-radio.html"><img src="images/clients/6.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com"><img src="images/clients/7.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/logo/up-travel.html"><img src="images/clients/8.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/logo/caffi-bardi.html"><img src="images/clients/9.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/logo/bignix-design.html"><img src="images/clients/10.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/"><img src="images/clients/11.png" alt="Clients"></a></li>--}}
                    {{--<li><a href="http://logofury.com/"><img src="images/clients/12.png" alt="Clients"></a></li>--}}
                {{--</ul>--}}

            {{--</div>--}}

            {{--<div class="section footer-stick">--}}

                {{--<h4 class="uppercase center">What <span>Clients</span> Say?</h4>--}}

                {{--<div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">--}}
                    {{--<div class="flexslider">--}}
                        {{--<div class="slider-wrap">--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-image">--}}
                                    {{--<a href="#"><img src="images/testimonials/3.jpg" alt="Customer Testimonails"></a>--}}
                                {{--</div>--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--Steve Jobs--}}
                                        {{--<span>Apple Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-image">--}}
                                    {{--<a href="#"><img src="images/testimonials/2.jpg" alt="Customer Testimonails"></a>--}}
                                {{--</div>--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--Collis Ta'eed--}}
                                        {{--<span>Envato Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-image">--}}
                                    {{--<a href="#"><img src="images/testimonials/1.jpg" alt="Customer Testimonails"></a>--}}
                                {{--</div>--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--John Doe--}}
                                        {{--<span>XYZ Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

        {{--</div>--}}

    {{--</section><!-- #content end -->--}}
    <div class="section nopadding notopmargin nobg clearfix" style="margin-bottom: 0px;">
        <div class="row common-height clearfix">
            <div class="col-lg-6 center col-padding" style="background: url({{ asset('images/1.jpg') }}) center center no-repeat; background-size: cover;">
                <div>&nbsp;</div>
            </div>

            <div class="col-lg-6 col-padding">
                <div>
                    <div style="position: relative;">
                        <div class="heading-block noborder" data-heading="A">
                            <h3 class="nott ls0">About Us</h3>
                        </div>
                    </div>

                    <!-- About Us Featured Boxes
                    ============================================= -->
                    <div class="row clearfix">
                        <div class="col-lg-10 col-md-8 bottommargin">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    {{--<a href="#"><i class="icon-desktop"></i></a>--}}
                                </div>
                                <h3>{{ $config->string_value }}</h3>
                                <p>{{ $config->text_value }}</p>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-8 bottommargin">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    {{--<a href="#"><i class="icon-et-gears"></i></a>--}}
                                </div>
                                <h3>{{ $config2->string_value }}</h3>
                                <p>{{ $config2->text_value }}</p>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-8">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    {{--<a href="#"><i class="icon-et-document"></i></a>--}}
                                </div>
                                <h3>{{ $config3->string_value }}</h3>
                                <p>{{ $config3->text_value }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection