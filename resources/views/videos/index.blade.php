@extends('layouts.main')

@section('content')
    <style>
        #full-width-slider {
            width: 100%;
            color: #000;
        }
        .coloredBlock {
            padding: 12px;
            background: rgba(255,0,0,0.6);
            color: #FFF;
            width: 200px;
            left: 20%;
            top: 5%;
        }
        .infoBlock {
            position: absolute;
            top: 30px;
            right: 30px;
            left: auto;
            max-width: 25%;
            padding-bottom: 0;
            background: #FFF;
            background: rgba(255, 255, 255, 0.8);
            overflow: hidden;
            padding: 20px;
        }
        .infoBlockLeftBlack {
            color: #FFF;
            background: #000;
            background: rgba(0,0,0,0.75);
            left: 30px;
            right: auto;
        }
        .infoBlock h4 {
            font-size: 20px;
            line-height: 1.2;
            margin: 0;
            padding-bottom: 3px;
        }
        .infoBlock p {
            font-size: 14px;
            margin: 4px 0 0;
        }
        .infoBlock a {
            color: #FFF;
            text-decoration: underline;
        }
        .photosBy {
            position: absolute;
            line-height: 24px;
            font-size: 12px;
            background: #FFF;
            color: #000;
            padding: 0px 10px;
            position: absolute;
            left: 12px;
            bottom: 12px;
            top: auto;
            border-radius: 2px;
            z-index: 25;
        }
        .photosBy a {
            color: #000;
        }
        .fullWidth {
            max-width: 1400px;
            margin: 0 auto 24px;
        }

        @media screen and (min-width:960px) and (min-height:660px) {
            .heroSlider .rsOverflow,
            .royalSlider.heroSlider {
                height: 620px !important;
            }
        }

        @media screen and (min-width:960px) and (min-height:1000px) {
            .heroSlider .rsOverflow,
            .royalSlider.heroSlider {
                height: 760px !important;
            }
        }
        @media screen and (min-width: 0px) and (max-width: 800px) {
            .royalSlider.heroSlider,
            .royalSlider.heroSlider .rsOverflow {
                height: 400px !important;
            }
            .infoBlock {
                padding: 10px;
                height: auto;
                max-height: 100%;
                min-width: 40%;
                left: 5px;
                top: 5px;
                right: auto;
                font-size: 12px;
            }
            .infoBlock h3 {
                font-size: 14px;
                line-height: 17px;
            }
        }
    </style>
    <div id="full-width-slider" class="royalSlider heroSlider rsMinW">
        <?php $i = 0;?>
        @foreach($topvideos as $video)
            <?php $i++;?>
            @if($i%2 == 1)
            <div class="rsContent">
                <a class="rsImg" data-rsvideo="https://www.youtube.com/watch?v={{ $video->youtube_id }}" href="https://img.youtube.com/vi/{{ $video->youtube_id }}/maxresdefault.jpg"></a>
                <div class="infoBlock  rsAbsoluteEl" style="color:#000;" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
                    <h4>{{ $video->name }}</h4>
                    <p>{{ $video->url }}</p>
                </div>

            </div>
            @else
            <div class="rsContent">
                <a class="rsImg"  data-rsvideo="https://www.youtube.com/watch?v={{ $video->youtube_id }}" href="https://img.youtube.com/vi/{{ $video->youtube_id }}/maxresdefault.jpg"></a>
                <div class="infoBlock infoBlockLeftBlack rsABlock" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
                    <h4 style="color: white">{{ $video->name }}</h4>
                    <p>{{ $video->url }}</p>
                </div>
            </div>
            @endif
        @endforeach
    </div>

    @foreach($categories as $category)
    <div  style="margin: 0px 0;margin-top: 30px;">

        <div class="container clearfix">

            <h3 style="margin-bottom: 0px">{{ $category->name }}</h3>
            {{--<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>--}}
            <div id="oc-images2" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-pagi="false" data-rewind="true" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4" data-items-xl="4">
                @foreach(\App\Models\Video::where('category_id',$category->id)->get() as $video)
                <div class="oc-item">
                        <div class="ipost clearfix">
                            <div class="entry-image">
                                <a href="{{ url('video/'.$video->id) }}"><img class="image_fade" src="https://img.youtube.com/vi/{{$video->youtube_id}}/hqdefault.jpg" alt="Image"></a>
                            </div>
                            <div class="entry-title">
                                <h4><a href="{{ url('video/'.$video->id) }}">{{$video->name}}</a></h4>
                                <ul class="entry-meta">
                                    <li>{{ \Carbon\Carbon::parse($video->published_at)->format('F Y')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="divider divider-center"><a href="#" data-scrollto="#header"><i class="icon-chevron-up"></i></a></div>

        </div>

    </div>
    @endforeach
@endsection

@section('script')
    <!-- Main slider JS script file -->
    <!-- Create it with slider online build tool for better performance. -->
    <script src="{{ asset('royalslider/jquery.royalslider.custom.min.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            $('#full-width-slider').royalSlider({
                arrowsNav: true,
                loop: false,
                keyboardNavEnabled: true,
                controlsInside: false,
                imageScaleMode: 'fill',
                arrowsNavAutoHide: false,
                autoScaleSlider: true,
                autoScaleSliderWidth: 960,
                autoScaleSliderHeight: 350,
                controlNavigation: 'bullets',
                thumbsFitInViewport: false,
                navigateByClick: true,
                startSlideId: 0,
                autoPlay: false,
                transitionType:'move',
                globalCaption: false,
                deeplinking: {
                    enabled: true,
                    change: false
                },
                /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
                imgWidth: 1400,
                imgHeight: 680
            });
        });

    </script>

@endsection