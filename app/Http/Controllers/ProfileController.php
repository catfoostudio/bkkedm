<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->isAdminRequest()){
            $view = 'user.cfadmin.profile';
//            $posts = Post::where('type','music')->where('status',1)->orderBy('updated_at')->get();

            return view($view, [
                'user' => Auth::user(),
            ]);
        }
        else{

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $user = Auth::user();

        $input = $request->input();

        $user->name = $input['name'];
        $user->quote = $input['quote'];

        $path = str_replace(' ', '%20', $input['filepath']);
        $time = time();
        $img = Image::make(asset($path));
        $img->fit(120 , 120);
        $filename = $time.'_profile_'.$user->id.'.jpg';
        $filepath = public_path('images/profile').'/'.$filename;
        $img->save($filepath);
        $user->avatar = $filename;
        $user->original_image = $input['filepath'];
        $user->save();

        return redirect('cfadmin/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
