@extends('.layouts.cfadmin')
@section('content')
    <style>
        /* image gallery */
        .gallery {
            width: 100%;
            float: left;
            margin-top: 20px;
        }

        .gallery ul {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        .gallery ul li {
            padding: 7px;
            border: 2px solid #ccc;
            float: left;
            margin: 10px 7px;
            background: none;
            width: auto;
            height: auto;
        }

        .gallery img {
            width: 250px;
        }

        /* notice box */
        .notice, .notice a {
            color: #fff !important;
        }

        .notice {
            z-index: 8888;
            padding: 10px;
            margin-top: 20px;
        }

        .notice a {
            font-weight: bold;
        }

        .notice_error {
            background: #E46360;
        }

        .notice_success {
            background: #657E3F;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{--{{ $event->name }}--}}
                Featured Events
                {{--<small>Manage featured events</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/event') }}">Event</a></li>
                <li class="active">Featured Events</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>ขออภัย!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Featured Events Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group" id="gallery_section" data-index="{{ count($events) }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="reorder_link btn btn-default">Reorder events
                                        </button>
                                        <button type="button" class="reload" style="display: none"><img
                                                    src="{{ asset('images/refresh-animated.gif') }}"/></button>
                                        <button type="button" id="saveReorder" class="btn btn-default"
                                                style="display: none">Save reordering
                                        </button>
                                        <button type="button" class="btn btn-default" data-toggle="modal"
                                                id="btn-event-modal">
                                            Add event
                                        </button>
                                        <div id="reorderHelper" class="light_box" style="display:none;">1. Drag photos
                                            to reorder.<br>2. Click 'Save Reordering' when finished.
                                        </div>
                                        <div class="gallery">
                                            <ul class="reorder_ul reorder-photos-list">
                                                <?php
                                                // Include and create instance of db class
                                                //require_once 'DB.class.php';
                                                //$db = new DB();

                                                // Fetch all images from database

                                                //$images = \App\Models\Image::all();
                                                ?>
                                                @if(!empty($featuredevents))
                                                    @foreach($featuredevents as $i)
                                                        <li id="image_li_{{ $i->id }}" class="ui-sortable-handle">
                                                            <img class="btn-delete-featured" data-id="{{ $i->id }}"
                                                                 src="{{ asset('images/red-cross.png') }}"
                                                                 style="position: absolute;width: 20px;height: 20px;cursor: pointer;margin-left: 180px;margin-top: 155px;"/>
                                                            <a href="javascript:void(0);" style="float:none;"
                                                               class="image_link">
                                                                <img src="{{ asset('images/events/posters/'.$i->poster_image) }}" alt=""
                                                                     style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">
                                                            </a>
                                                            <div style="margin-top: 5px;width: 180px;height: 40px;overflow: hidden;">{{ $i->id }}
                                                                : {{ $i->name }}</div>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        {{--<div class="box-footer">--}}
                        {{--<button type="submit" class="btn btn-info">Update</button>--}}
                        {{--<a href="{{ url('cfadmin/event') }}">--}}
                        {{--<button type="button" class="btn btn-default pull-right">Cancel</button>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <div class="modal fade" id="modal-event">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Event</h4>
                    <div id="error" style="color: red;display: none;">This event already in featured.</div>
                </div>
                <div class="modal-body" style="padding: 0;">

                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Select event</label><br>
                                    <select name="event" id="event" style="margin-left: 10px;width: 100%;"
                                            class="form-control select2">
                                        @foreach($events as $event)
                                            <option value="{{ $event->id }}"
                                                    data-image="{{ asset('images/events/posters/'.$event->poster_image) }}"
                                                    data-name="{{$event->name}}">{{ $event->id }}:
                                                - {{ $event->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add_event">Add</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- page script -->
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>

        function getnonetopstory(){
            //AJAX GET MUSIC NEWS LIST
            $.ajax({
                type: 'GET',
                url: "{{ url('cfadmin/geteventnotfeatured') }}",
                success: function (data) {

                    $('#event').html(data.html);
                }, error: function (data) {
                    console.log('fail to load more event.');
                    $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            });
        }

        $(document).ready(function () {

            //Initialize Select2 Elements
            $('.select2').select2();

            $('.reorder_link').on('click', function () {
                var btn_reorder = $(this);
                $("ul.reorder-photos-list").sortable({tolerance: 'pointer'});
//                $('.reorder_link').html('save reordering');
//                $('.reorder_link').attr("id","saveReorder");
                $("#reorderHelper").html("1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.").removeClass('notice notice_error notice_success').addClass('light_box');
                $('#reorderHelper').slideDown('slow');

                $('.reorder_link').css('display', 'none');
                $('.reload').css('display', 'none');
                $('#saveReorder').css('display', 'inline');

                $('.image_link').attr("href", "javascript:void(0);");
                $('.image_link').css("cursor", "move");

            });

            $("#saveReorder").click(function (e) {
                if (!$("#saveReorder i").length) {
                    $('.reorder_link').css('display', 'none');
                    $('.reload').css('display', 'inline');
                    $('#saveReorder').css('display', 'none');
                    {{--$(this).html('').prepend('<img src="{{ asset('images/refresh-animated.gif') }}"/>');--}}
                    $("ul.reorder-photos-list").sortable('destroy');
                    $("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');

                    var h = [];
                    $("ul.reorder-photos-list li").each(function () {
                        h.push($(this).attr('id').substr(9));
                    });

                    $.ajax({
                        type: "POST",
                        url: "{{ url('cfadmin/reorderfeatured') }}",
                        data: {ids: " " + h + ""},
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        success: function () {
                            $("#reorderHelper").html("Finish! Reordered Photos").removeClass('light_box').addClass('notice notice_success');
                            $('.reorder_link').css('display', 'inline');
                            $('.reload').css('display', 'none');
                            $('#saveReorder').css('display', 'none');
//                            btn_reorder.text('').prepend('Reorder photos');
//                            $('.reorder_link').removeAttr( "id" );
                        }
                    });
                    return false;
                }
                e.preventDefault();
            });

            var lfm = function (options, cb) {

                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = cb;
            }


            $('.btn-delete-featured').click(function () {

                var featuredObject = $(this).parent();

                $.ajax({
                    type: "POST",
                    url: "{{ url('cfadmin/removefeatured') }}",
                    data: {id: $(this).data('id')},
                    headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    success: function (result) {
                        featuredObject.remove();
                        getnonetopstory();
                    }
                });

            });

            $('#btn-event-modal').click(function (e) {
                $('#modal-event').modal('show');
            });

            $('#add_event').click(function (e) {

                var eventid = $('#event').val();
                var image = $('#event').find(':selected').data('image');
                var eventname = $('#event').find(':selected').data('name');
                var btn_add_photo = $(this);

                $.ajax({
                    type: "POST",
                    url: "{{ url('cfadmin/addfeatured') }}",
                    data: {eventid: eventid},
                    headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    success: function (result) {
                        if (result == 0) {
                            $('.reorder-photos-list').append('<li id="image_li_' + eventid + '" class="ui-sortable-handle">\n' +
                                '                    <img class="btn-delete-featured" data-id="' + eventid + '" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer;margin-left: 180px;margin-top: 155px;" />\n' +
                                '                    <a href="javascript:void(0);" style="float:none;" class="image_link">\n' +
                                '                        <img src="' + image + '" alt="" style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">\n' +
                                '                    </a>' +
                                '                       <div style="margin-top: 5px;width: 180px;height:40px;overflow: hidden;">' + eventid + ': ' + eventname + '</div>' +
                                '                </li>');
                            btn_add_photo.html('').prepend('Add');

                            getnonetopstory();

                            $('.btn-delete-featured').click(function () {

                                var featuredObject = $(this).parent();

                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('cfadmin/removefeatured') }}",
                                    data: {id: $(this).data('id')},
                                    headers:
                                        {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                    success: function (result) {
                                        featuredObject.remove();
                                        getnonetopstory();
                                    }
                                });

                            });

                            $("#reorderHelper").html("Finish! Photo Added").removeClass('notice notice_success light_box').addClass('notice notice_success');
                        }
                        else {
                            $('#error').show().delay(3000).fadeOut();
                        }
                    }
                });


                {{--var btn_add_photo = $(this);--}}
                {{--lfm({type: 'image', prefix: ''}, function(url, path) {--}}
                {{--//alert(url + ' ' + path);--}}
                {{--btn_add_photo.html('').prepend('<img src="{{ asset('images/refresh-animated.gif') }}"/>');--}}
                {{--$("#reorderHelper").html( "Processing Photo - This could take a moment. Please don't navigate away from this page." ).removeClass('notice notice_success light_box').addClass('notice notice_error');--}}
                {{--$('#reorderHelper').slideDown('slow');--}}
                {{--$.ajax({--}}
                {{--type: "POST",--}}
                {{--url: "{{ url('cfadmin/addproductimage') }}",--}}
                {{--data: {path: path,id: '{{ $product->id }}'},--}}
                {{--headers:--}}
                {{--{--}}
                {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                {{--},--}}
                {{--success: function(result){--}}
                {{--$('.reorder-photos-list').append('<li id="image_li_'+result+'" class="ui-sortable-handle">\n' +--}}
                {{--'                    <img class="btn-delete-image" data-id="'+result+'" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer" />\n' +--}}
                {{--'                    <a href="javascript:void(0);" style="float:none;" class="image_link">\n' +--}}
                {{--'                        <img src="'+url+'" alt="" style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">\n' +--}}
                {{--'                    </a>\n' +--}}
                {{--'                </li>');--}}
                {{--btn_add_photo.html('').prepend('Add photo');--}}
                {{--$("#reorderHelper").html( "Finish! Photo Added" ).removeClass('notice notice_success light_box').addClass('notice notice_success');--}}
                {{--}--}}
                {{--});--}}


                {{--});--}}
            });

        });

    </script>
@endsection
