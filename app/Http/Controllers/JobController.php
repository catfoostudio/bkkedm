<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;


class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if($this->isAdminRequest()){
            $view = 'jobs.cfadmin.index';
            $jobs = Job::orderBy('created_at', 'desc')->get();
        }
        else{
            $view = 'jobs.index';
            $jobs = Job::where('status',1)->orderBy('created_at', 'desc')->get();
        }


        return view($view, [
            'jobs' => $jobs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if($this->isAdminRequest()){
            $view = 'jobs.cfadmin.create';

        }
        else{


        }


        return view($view, [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'description' => 'required',
            'requirement' => 'required',
        ];

        $this->validate($request, $validate);

        $job = Job::create([
            'name' => $input['name'],
            'description' => $input['description'],
            'requirement' => $input['requirement'],
        ]);

        $request->session()->flash('alert-success', 'Job #'.$job->id.' was successful Added!');

        return redirect('cfadmin/job');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $job = Job::find($id);

        if($this->isAdminRequest()){

            $view = 'jobs.cfadmin.detail';

            return view($view, [
                'job' => $job,

            ]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'description' => 'required',
            'requirement' => 'required',
        ];

        $this->validate($request, $validate);

        $job = Job::find($id);
        $job->name = $input['name'];
        $job->description = $input['description'];
        $job->requirement = $input['requirement'];
        $job->save();


        $request->session()->flash('alert-success', 'Job #'.$job->id.' was successful updated!');

        return redirect('cfadmin/job');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $job = Job::find($id);
        $job->status = 0;
        $job->save();
        return redirect('cfadmin/job')->with('alert-danger', 'Job #'.$job->id.' was successful inactivated!');
    }

    public function active($id){
        $job = Job::find($id);
        $job->status = 1;
        $job->save();
        return redirect('cfadmin/job')->with('alert-success', 'Job #'.$job->id.' was successful activated!');
    }
}
