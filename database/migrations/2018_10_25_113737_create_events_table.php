<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('content')->nullable();
            $table->string('cover_original_image');
            $table->string('poster_original_image');
            $table->string('cover_image');
            $table->string('poster_image');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('featured_flag')->default(0);
            $table->tinyInteger('featured_order')->default(0);
            $table->text('quote')->nullable();
            $table->string('location')->nullable();
            $table->string('type')->nullable();
            $table->decimal('price',13,2);
            $table->string('facebook_id')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('line_id')->nullable();
            $table->string('tel')->nullable();
            $table->string('mobile')->nullable();
            $table->text('remark')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs');
            $table->date('eventdate')->nullable();
            $table->string('reservationtime')->nullable();
            $table->timestamps();
            $table->string('ticket')->nullable();
            $table->date('eventenddate')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->integer('views')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
