@extends('layouts.main')
@section('content')
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding-bottom: 0px;padding-top: 30px;">
            <div class="container clearfix">
                <h3>VOTES</h3>
                <div id="votes" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($votes as $vote)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('vote/'.$vote->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/votes/'.$vote->thumbnail_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('vote/'.$vote->id) }}">{{ $vote->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($vote->start)->format('dS M Y') }}</li>
                                <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($vote->end)->format('dS M Y') }}</li>
                                {{--<li><i class="icon-gift"></i>Ticket</li>--}}
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData('present')">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
            </div>
        </div>
    </section><!-- #content end -->
    @if(count($pastvotes) > 0)
    <div class="section dark" style="padding-bottom: 0px;padding-top: 40px;margin: 0px 0;">
        <div class="container clearfix">

            <h3>PAST VOTES</h3>
            <div id="pastvotes" class="post-grid clearfix" data-layout="fitRows">
                @foreach($pastvotes as $pastvote)
                    <div class="entry clearfix">
                        <div class="entry-image" style="margin-bottom: 10px;">
                            <a href="{{ url('vote/'.$pastvote->id) }}">
                                <img class="image_fade" style="object-fit: cover" src="{{ asset('images/votes/'.$pastvote->thumbnail_image) }}" alt="Image">
                            </a>
                        </div>
                        <div class="entry-title" style="height: 51px;overflow: hidden;">
                            <h2><a href="{{ url('vote/'.$pastvote->id) }}">{{ $pastvote->name }}</a></h2>
                        </div>
                        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($pastvote->start)->format('dS M Y') }}</li>
                            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($pastvote->end)->format('dS M Y') }}</li>
                            {{--<li><i class="icon-gift"></i>Ticket</li>--}}
                        </ul>
                    </div>
                @endforeach
            </div>
            <!-- Pagination
                      ============================================= -->
            <div class="row mb-3">
                <div class="col-12" align="center">
                    <button class="button button-3d button-rounded button-black" onclick="loadMoreData('past')">Load more</button>
                </div>
            </div>
            <!-- .pager end -->
        </div>

    </div>
    @endif
@endsection
@section('script')
    <script>

        var pageNumber = 2;
        var pastPageNumber = 2;

        function loadMoreData(type){
            if(type == 'present'){
                if(pageNumber <= {{ $votes->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "vote?page=" +pageNumber,
                        success : function(data){
                            pageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#votes').append(data.html);
                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }
            else
            {
                if(pastPageNumber <= {{ $pastvotes->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "loadpastvote?page=" +pastPageNumber,
                        success : function(data){
                            pastPageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#pastvotes').append(data.html);
                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }

        }
    </script>
@endsection
