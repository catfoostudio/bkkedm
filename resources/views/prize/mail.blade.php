Dear. {{ $username }}
<br><br>
Since you registered our campaign "{{ $prizename }}"
<br>Congratulations! You've won a prize.
<br>This is your redeem code for reward : {{ $code }}
<br>
<br>You can login then redeem your reward on this link {{ url('redeem') }}
<br>or If your reward have to redeem at the event. Please don't forget to bring reward code with you.
<br><br>
If you didn't register our campaign, have problem about redeem or any questions.
<br>Please contact back to us bkkedm@gmail.com
<br>We will response as soon as possible.
<br><br>Thank you.