<!-- Content
  ============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-top: 0px;">

        <div class="single-event">

            <div class="container topmargin clearfix">
                <img class="entry-image" src="{{ asset('images/events/waterzonic/096.png')}}">
                <div class="col_full">
                    <div class="col_half nobottommargin">
                        <p><i class="icon-calendar3"></i> <strong>Date:</strong> 15th March, 2015 20:00 - 02:00<br>
                            <i class="icon-map-marker2"></i> <strong>Location:</strong> Ibiza, Spain</p>
                    </div>
                    <div class="col_one_fourth nobottommargin col_last">
                        <a href="#" class="btn btn-danger btn-block btn-lg">Buy Tickets</a>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="nobottommargin clearfix">


                    <div class="col_full" align="center">
                        <img src="{{ asset('images/events/waterzonic/WTZ18_Eventpop_poster_kv.png')}}"
                             style="width:100%;">
                        <img src="{{ asset('images/events/waterzonic/DLDK_DYRO.jpg')}}" style="width:48%;">
                        <img src="{{ asset('images/events/waterzonic/DLDK_Dimitri.jpg')}}" style="width:48%;">
                        <img src="{{ asset('images/events/waterzonic/WTZ_VIP_PRiveleges_1200x1200_V02.png')}}"><br><br>
                        <p style="font-size: 20px;"><strong>Terms and Conditions</strong><br>
                            - Only 20+ can entry the event.<br>
                            - Ticket cannot be replaced if lost, stolen or destroyed.<br>
                            - Non-refundable / Non-returnable<br>
                            - Name on E-Ticket must match name on your ID Card or Passport (Foreigner)<br>
                            If the name does not match must authorized signatory.</p>

                    </div>

                    <div class="clear"></div>
                    {{--<div class="col_full" style="margin-top: 30px;">--}}
                        {{--<div class="col_half nobottommargin">--}}

                            {{--<h4>Preview</h4>--}}

                            {{--<iframe src="//player.vimeo.com/video/30626474" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>--}}

                            {{--</div>--}}

                        {{--<div class="col_half nobottommargin col_last">--}}

                            {{--<h4>Events Timeline</h4>--}}

                            {{--<div class="table-responsive">--}}
                                {{--<table class="table table-striped">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>Timings</th>--}}
                                        {{--<th>Location</th>--}}
                                        {{--<th>Events</th>--}}
                                        {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--<tr>--}}
                                        {{--<td><span class="badge badge-danger">10:00 - 12:00</span></td>--}}
                                        {{--<td>Main Auditorium</td>--}}
                                        {{--<td>WWDC Developer Conference</td>--}}
                                        {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<td><span class="badge badge-danger">12:00 - 12:45</span></td>--}}
                                        {{--<td>Cafeteria</td>--}}
                                        {{--<td>Lunch</td>--}}
                                        {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<td><span class="badge badge-danger">15:00 - 18:00</span></td>--}}
                                        {{--<td>Room - 25, 2nd Floor</td>--}}
                                        {{--<td>Hardware Testing &amp; Evaluation</td>--}}
                                        {{--</tr>--}}
                                    {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->