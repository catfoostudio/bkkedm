<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hunter extends Model
{
    //
    protected $table = 'prize_user';

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function prize()
    {
        return $this->hasOne('App\Models\Prize', 'id', 'prize_id');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Registered';
        }
        elseif($this->status == 2){
            return 'Won';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Decline';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-warning">Registered</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-success">Won</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Decline</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    static public function getBadgeByStatus($status)
    {
        if($status == 1){
            return '<span class="badge badge-warning">Registered</span>';
        }
        elseif($status == 2){
            return '<span class="badge badge-success">Won</span>';
        }
        elseif($status == 3){
            return '<span class="badge badge-success">Start</span>';
        }
        elseif($status == 4){
            return '<span class="badge badge-danger">End</span>';
        }
        elseif($status == 0){
            return '<span class="badge badge-danger">Decline</span>';
        }
        else{
            return '<span class="badge badge-danger">Error Status</span>';
        }
    }

}
