<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use \Spatie\Tags\HasTags;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\Video');
    }

    public function sentMessage()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function receiptedMessage()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function lottos()
    {
        return $this->belongsToMany('App\Models\Lotto')->withTimestamps();
    }

    public function prizes()
    {
        return $this->belongsToMany('App\Models\Prize')->withTimestamps()->withPivot('status','activatecode_id');
    }

    public function choices()
    {
        return $this->belongsToMany('App\Models\Choice')->withTimestamps();
    }

    public function contests()
    {
        return $this->belongsToMany('App\Models\Contest')->withTimestamps();
    }

    public function getRoleAttribute()
    {
        return $this->roles[0]->name;
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Active';
        }
        elseif($this->status == 2){
            return 'Suspended';
        }
        elseif($this->status == 0){
            return 'Not Activate';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Active</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-danger">Suspended</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-warning">Not Activate</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public function getRolesBadgeAttribute(){

        $string = '';
        foreach($this->roles as $role)
        {
            $string .= '<span class="label" style="background-color: dimgrey">'.$role->name.'</span>';
        }
        return $string;
    }



    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }

        return $check;
    }

}
