@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-event">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">
                            <div class="entry-image nobottommargin">
                                <img src="{{ asset('images/lottos/'.$lotto->thumbnail_image) }}" >
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                            <div class="card events-meta mb-3">

                                <div class="card-body" style="background-color: #333;color: white">
                                    <ul class="iconlist nobottommargin">
                                        <h3 style="color: white">{{ $lotto->name }}</h3>
                                        <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($lotto->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($lotto->end)->format('d M Y') }}</li>
                                        @if(!empty($lotto->reward))<li><i class="icon-gift"></i>{{ $lotto->reward }}</li>@endif
                                        {{--<li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($lotto->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($lotto->end)->format('d M Y') }}</li>--}}
                                    </ul>
                                </div>
                            </div>
                            @if($errors->any())
                                <ul class="alert alert-danger" style="list-style-type: none">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if($isPastLotto)
                                <button class="btn btn-dark btn-block btn-lg disabled">Expired</button>
                            @elseif(Auth::check())
                            @if(empty($gambler) || empty($gambler->activatecode))
                                {!! Form::open(['method' => 'POST' , 'action' => ['LottoController@redeem' , $lotto],'id'=>'my-form-id']) !!}
                                {!! Form::text('redeem_code',old('redeem_code'),['placeholder'=>'Enter code XXXX-XXXX-XXXX-XXXX','class'=>'form-control', 'style'=>'margin-bottom:15px;']) !!}
                                {!! NoCaptcha::displaySubmit('my-form-id', 'Register', ['class' => 'btn btn-dark btn-block btn-lg']) !!}
                                {!! Form::close() !!}
                            @else
                                <button class="btn btn-dark btn-block btn-lg disabled">Registered</button>
                            @endif
                            @else
                                <a href="{{ route('signin') }}"><button class="btn btn-dark btn-block btn-lg">Sign In to register</button></a>
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_three_fourth">
                        <div style="padding: 30px 0;">
                            <div class="fancy-title title-border">
                                <h3>Description</h3>
                            </div>
                            {!! $lotto->content !!}
                        <!-- Tag Cloud
                                ============================================= -->
                            @if(!empty(count($lotto->tags)))
                                <div class="tagcloud clearfix bottommargin">
                                    @foreach($lotto->tags as $tag)
                                        <a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>
                                    @endforeach
                                </div><!-- .tagcloud end -->
                            @endif
                            <div class="clear"></div>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {!! NoCaptcha::renderJs() !!}
@endsection