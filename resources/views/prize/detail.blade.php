@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-event">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="padding-bottom: 20px;">
                            <div class="entry-image nobottommargin">
                                <img src="{{ asset('images/prizes/'.$prize->thumbnail_image) }}" >
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                            <div class="card events-meta mb-3">

                                <div class="card-body" style="background-color: #333;color: white">
                                    <ul class="iconlist nobottommargin">
                                        <h3 style="color: white">{{ $prize->name }}</h3>
                                        <li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($prize->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($prize->end)->format('d M Y') }}</li>
                                        @if(!empty($prize->reward))<li><i class="icon-gift"></i>{{ $prize->reward }}</li>@endif
                                        {{--<li><i class="icon-calendar3"></i>{{ \Carbon\Carbon::parse($lotto->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($lotto->end)->format('d M Y') }}</li>--}}
                                    </ul>
                                </div>
                            </div>
                            @if($isBeforePrize)
                                <button class="btn btn-dark btn-block btn-lg disabled">Please waiting until start date</button>
                            @elseif($isPastPrize)
                                <button class="btn btn-dark btn-block btn-lg disabled">Expired</button>
                            @elseif(Auth::check())
                            @if(empty($hunter))
                                {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@register' , $prize],'id'=>'my-form-id', 'enctype' => 'multipart/form-data']) !!}
                                    Image: @if($errors->has('file'))
                                        <span class="invalid-feedback" role="alert" style="display: inline;">
                                        <strong> {{ $errors->first('file') }}</strong>
                                    </span>
                                    @endif
                                    {!! Form::file('file', [ 'style' => 'margin-bottom:5px;','class'=>'form-control'.($errors->has('file') ? ' error' : '' ), 'accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                    Link: @if($errors->has('link'))
                                        <span class="invalid-feedback" role="alert" style="display: inline;">
                                        <strong> {{ $errors->first('link') }}</strong>
                                    </span>
                                    @endif
                               <input type="text" name="link" class="form-control{{($errors->has('link') ? ' error' : '' )}}">
                                {!! NoCaptcha::displaySubmit('my-form-id', 'Register', ['class' => 'btn btn-dark btn-block btn-lg', 'style' => 'margin-top:10px;']) !!}
                                {!! Form::close() !!}
                            @else
                                <button class="btn btn-dark btn-block btn-lg disabled">Registered</button>
                            @endif
                            @else
                                <a href="{{ route('signin') }}"><button class="btn btn-dark btn-block btn-lg">Sign In to register</button></a>
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_three_fourth">
                        <div style="padding: 30px 0;">
                            <div class="fancy-title title-border">
                                <h3>Description</h3>
                            </div>
                            {!! $prize->content !!}
                        <!-- Tag Cloud
                                ============================================= -->
                            @if(!empty(count($prize->tags)))
                                <div class="tagcloud clearfix bottommargin">
                                    @foreach($prize->tags as $tag)
                                        <a href="{{ url('tag/'.$tag->id.'/'.$tag->name) }}">{{ $tag->name }}</a>
                                    @endforeach
                                </div><!-- .tagcloud end -->
                            @endif
                            <div class="clear"></div>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {!! NoCaptcha::renderJs() !!}
@endsection