<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Post;
use Illuminate\Http\Request;
use Spatie\Tags\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        \Auth::user()->attachTags(['concert', 'BLACKPINK']);

        $view = 'tag.index';
        $tag = Tag::whereIn('id', [$id])->get();

        $posts = Post::withAnyTags($tag)->orderBy('published_at', 'desc')->paginate(6);

        $events = Event::withAnyTags($tag)->orderBy('published_at', 'desc')->paginate(6);

        return view($view, [
            'posts' => $posts,
            'events' => $events,
            'tag' => $tag->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxnews(Request $request)
    {
        $input = $request->input();

        $tag = Tag::whereIn('id', [$input['tag']])->get();

        $ajaxnews = Post::withAnyTags($tag)->orderBy('published_at', 'desc')->paginate(6);

        if ($request->ajax()) {

            $view = view('tag.loadallnews', compact('ajaxnews'))->render();
            return response()->json(['html'=>$view, 'hasMorePages'=> $ajaxnews->hasMorePages()]);
        }
    }

    public function follow(Request $request)
    {
        $input = $request->input();
        $tag = Tag::whereIn('id', [$input['id']])->first();

        if ($request->ajax()) {
            \Auth::user()->attachTag($tag->name);
            return response()->json(['success'=> true]);
        }
    }

    public function unfollow(Request $request)
    {
        $input = $request->input();
        $tag = Tag::whereIn('id', [$input['id']])->first();

        if ($request->ajax()) {
            \Auth::user()->detachTags($tag->name);
            return response()->json(['success'=> true]);
        }
    }
}
