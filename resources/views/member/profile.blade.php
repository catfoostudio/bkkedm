@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row clearfix">
                    <div class="col-md-9">
                        {{--                        <img src="{{ asset('images/icons/avatar.jpg') }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}
                        <div class="heading-block noborder" style="margin-bottom: 0px;">
                            <h3>Profile</h3>
                            {{--                            <span style="font-size: 15px;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y') }}</span>--}}
                        </div>
                        {{--<div class="clear"></div>--}}
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <!-- Comments
						============================================= -->
                                <div id="comments" class="clearfix" style="margin-top: 5px;padding-top: 30px;">
                                    <!-- Comment Form
                                    ============================================= -->
                                    <div id="respond" class="clearfix">

                                        {!! Form::open(array('method'=>'put','action' => ['MemberController@update', $member],'id' => 'register-form', 'name'=> 'register-form', 'class'=>'nobottommargin' )); !!}
                                        @csrf
                                        <div class="col_full">
                                            <label for="register-form-email">Email Address:</label>
                                            <input type="text" id="register-form-email" name="email_register" value="{{ old('email_register', $member->email) }}" class="form-control {{$errors->has('email_register') ? 'error' : '' }}" disabled="disabled" />
                                        </div>

                                        <div class="col_full">
                                            <label for="register-form-firstname">First name:</label>
                                            <input type="text" id="register-form-firstname" name="firstname" value="{{ old('firstname', $member->firstname) }}" class="form-control {{$errors->has('firstname') ? 'error' : '' }}" />
                                            @if($errors->has('firstname')) <strong style="color:red;">กรุณากรอกชื่อ</strong> @endif
                                        </div>
                                        <div class="col_full">
                                            <label for="register-form-lastname">Last name:</label>
                                            <input type="text" id="register-form-lastname" name="lastname" value="{{ old('lastname', $member->lastname) }}" class="form-control {{$errors->has('lastname') ? 'error' : '' }}" />
                                            @if($errors->has('lastname')) <strong style="color:red;">กรุณากรอกนามสกุล</strong> @endif
                                        </div>
                                        <div class="col_full">
                                            <div class="form-group">
                                                <label for="register-form-birthday">Birthday (Day/Month/Year):</label>
                                                {{--                                    {!! Form::select('year_of_birth', \Carbon\Carbon::now()->subYears(100)->year, \Carbon\Carbon::now()->year , \Carbon\Carbon::now()->year ,array('class' => 'form-control mnth')) !!}--}}
                                                <input autocomplete="off" class="form-control mnth {{$errors->has('birthday') ? 'error' : '' }}" id="register-form-birthday" type="text" name="birthday" value="{{ old('birthday', \Carbon\Carbon::parse($member->birthday)->format('d/m/Y')) }}"   >
                                                @if($errors->has('birthday')) <strong style="color:red;">กรุณาเลือกวันเกิดของคุณ</strong> @endif
                                            </div>
                                        </div>
                                        <div class="col_full">
                                            <label for="register-form-gender">Gender:</label>
                                            <div class="form-check">
                                                <input class="form-check-input" id="gridRadios1" type="radio" name="gender"  value="2" {{ ($member->gender == 2) ? 'checked=checked' : '' }} >
                                                <label class="form-check-label" for="gridRadios1">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" id="gridRadios2" type="radio" name="gender"  value="3" {{ ($member->gender == 3) ? 'checked=checked' : '' }}  >
                                                <label class="form-check-label" for="gridRadios2">
                                                    Female
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" id="gridRadios3" type="radio" name="gender"  value="1" {{ ($member->gender == 1) ? 'checked=checked' : '' }}  >
                                                <label class="form-check-label" for="gridRadios3">
                                                    Unspecified
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col_full">
                                            <label for="register-form-mobile">Mobile phone number:</label>
                                            <input type="text" name="mobile" id="register-form-mobile" value="{{ old('mobile', $member->mobile) }}" class="form-control {{$errors->has('mobile') ? 'error' : '' }}" />
                                            <small id="mobileHelp" class="form-text text-muted">กรุณาใช้เบอร์โทรจริง เพื่อการติดต่อกลับ</small>
                                            @if($errors->has('mobile')) <strong style="color:red;">กรุณากรอกเบอร์โทร 10หลัก</strong> @endif
                                        </div>
                                        <h3>Credentials</h3>
                                        <div class="col_full">
                                            <label for="register-form-password">Password:</label>
                                            <input type="password" id="register-form-password" name="password" value="" class="form-control {{$errors->has('password_register') ? 'error' : '' }}" />
                                            @if($errors->has('password')) <strong style="color:red;">รหัสผ่านขั้นต่ำ 8ตัวอักษร และกรุณาพิมยืนยันรหัสผ่านอีกครั้ง</strong> @endif
                                        </div>

                                        <div class="col_full">
                                            <label for="register-form-repassword">Confirm Password:</label>
                                            <input type="password" id="register-form-repassword" name="password_confirmation" value="" class="form-control {{$errors->has('password_confirmation') ? 'error' : '' }}" />
                                        </div>
                                        <div class="col_full">
                                            <label for="register-form-repassword">Current Password:</label>
                                            <input type="password" id="register-form-repassword" name="current_password" value="" class="form-control {{$errors->has('current_password') ? 'error' : '' }}" />
                                        </div>
                                        <div class="col_full nobottommargin">
                                            <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div><!-- #respond end -->

                                </div><!-- #comments end -->
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    @include('member.sidebar')

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function () {
            $('.mnth').datepicker({
                autoclose: true,
                // viewMode: "years",
                // minViewMode: "years",
                format: "dd/mm/yyyy",
                yearRange: "-100:+0",
            });

        });
    </script>
@endsection
