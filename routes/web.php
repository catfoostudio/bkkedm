<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::group(['middleware' => 'auth'], function () {


    Route::get('/','HomeController@index');
    Route::get('index','HomeController@index');

    Route::resource('music', 'MusicController');
    Route::resource('lifestyle', 'LifestyleController');
    Route::resource('event', 'EventController');
    Route::resource('club', 'ClubController');
//    Route::resource('lotto', 'LottoController');
//    Route::resource('prize', 'PrizeController');
    Route::resource('vote', 'VoteController');
    Route::resource('contest', 'ContestController');
    Route::resource('video', 'VideoController');

    Route::get('/reward','PrizeController@index');
    Route::get('/reward/create','PrizeController@create');
    Route::post('/reward','PrizeController@store');
    Route::get('/reward/{prize}','PrizeController@show');
    Route::get('/reward/{prize}/edit','PrizeController@edit');
    Route::put('/reward/{prize}','PrizeController@update');
    Route::delete('/reward/{prize}','PrizeController@destroy');

    Route::get('loadpastevent','EventController@loadpastevent');
    Route::get('loadpastlotto','LottoController@loadpastlotto');
    Route::get('loadpastprize','PrizeController@loadpastprize');
    Route::get('loadpastvote','VoteController@loadpastvote');
    Route::get('loadprize','PrizeController@loadprize');

    Route::get('music/{id}/{name}','MusicController@show');
    Route::get('lifestyle/{id}/{name}','LifestyleController@show');
    Route::get('event/{id}/{name}','EventController@show');
    Route::get('ajaxnews','MusicController@ajaxnews');
    Route::get('ajaxlifestyle','LifestyleController@ajaxnews');
    Route::get('ajaxtag','TagController@ajaxnews');
    Route::get('follow','TagController@follow');
    Route::get('unfollow','TagController@unfollow');

    Route::get('testpermission','TestController@index');

    Route::get('aboutus','AboutusController@index');
    Route::get('contactus','ContactController@index');
    Route::post('contactus/sendmail','ContactController@sendmail');
    Route::get('advertisting','AdvertisingController@index');
    Route::get('terms','TermsController@index');
    Route::get('policy','PolicyController@index');
    Route::get('jobs','JobController@index');

    Route::get('tag/{id}/{name}','TagController@show');
    Route::post('search','SearchController@search');
    Route::get('searchajaxnews','SearchController@searchajaxnews');


    Route::get('member' , 'MemberController@index');
    Route::get('member/message/compose' , 'MemberController@compose');
    Route::get('member/message/{id}' , 'MemberController@message');

    Route::get('member/lotto' , 'MemberController@lotto');
    Route::get('member/reward' , 'MemberController@prize');
    Route::get('member/contest' , 'MemberController@contest');
    Route::get('member/profile' , 'MemberController@profile');
    Route::put('member/{id}' , 'MemberController@update');

//    Route::post('lotto/redeem/{id}', 'LottoController@redeem');
    Route::post('reward/register/{id}', 'PrizeController@register');


    Route::get('signin', 'Auth\LoginController@signin')->name('signin');


    // Registration Routes...
    //Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    Route::get('verify-user/{code}', 'Auth\RegisterController@activate');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    //Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('checktime', function (){
        date_default_timezone_set("Asia/Bangkok");
        echo date('Y-m-d H:i:s');;
    });

    Route::get('myphpinfo', function (){
        echo phpinfo();
    });

    //Socialite Facebook
    Route::get('facebook', function () {
        return view('facebook');
    });
    Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
    Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

//});

// Authentication Routes...
Route::get('cfadmin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('maintenance',function () {
    return view('maintenance.maintenance');
})->name('maintenance');

Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|writer|moderator']), function() {
    Route::get('/exportuser', 'UserController@export');
    Route::get('/registeredprizeexport/{id}', 'PrizeController@export');
    Route::get('/codeprizeexport/{id}', 'PrizeController@exportcode');

    Route::get('/', 'CfAdminDashboardController@index');
    Route::resource('music', 'MusicController');
    Route::post('/music/{id}', 'MusicController@update');
    Route::get('/topstories', 'MusicController@topstories');
    Route::resource('lifestyle', 'LifestyleController');
    Route::post('/lifestyle/{id}', 'LifestyleController@update');
    Route::get('/lifetopstories', 'LifestyleController@topstories');

    Route::resource('podcast', 'PodcastController');
    Route::post('/podcast/{id}', 'PodcastController@update');

    Route::post('/musicactive/{id}', 'MusicController@active');
    Route::get('/getmusicnottopstory','MusicController@getmusicnottopstory');
    Route::get('/getlifestylenottopstory','LifestyleController@getlifestylenottopstory');


    Route::post('/lifestyleactive/{id}', 'LifestyleController@active');
    Route::post('/categoryactive/{id}', 'CategoryController@active');
    Route::post('/podcastactive/{id}', 'PodcastController@active');

    Route::post('/addtopstory', 'MusicController@addtopstory');
    Route::post('/removetopstory', 'MusicController@removetopstory');
    Route::post('/reordertopstory', 'MusicController@reordertopstory');

    Route::post('/lifestyleaddtopstory', 'LifestyleController@addtopstory');
    Route::post('/lifestyleremovetopstory', 'LifestyleController@removetopstory');
    Route::post('/lifestylereordertopstory', 'LifestyleController@reordertopstory');

    Route::resource('category', 'CategoryController');
    Route::get('/categorytype/{type}', 'CategoryController@index');
    Route::get('/category/create/{type}', 'CategoryController@create');
    Route::post('/category/{id}', 'CategoryController@active');

    Route::resource('video', 'VideoController');
    Route::post('/videoactive/{id}', 'VideoController@active');
    Route::get('/videoslide', 'VideoController@videoslide');

});


Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|moderator']), function() {

    Route::get('/', 'CfAdminDashboardController@index');

    Route::get('/profile', 'ProfileController@index');
    Route::post('/profile/{id}', 'ProfileController@update');

    Route::resource('user', 'UserController');

    Route::get('/aboutus', 'AboutusController@index');
    Route::post('/aboutus/{id}', 'AboutusController@update');

    //Job
    Route::get('/job', 'JobController@index');
    Route::get('/job/create', 'JobController@create');
    Route::get('/job/{id}', 'JobController@show');
    Route::put('/job/{id}', 'JobController@update');
    Route::post('/job/store', 'JobController@store');
    Route::delete('/job/{id}', 'JobController@destroy');
    Route::post('/job/{id}', 'JobController@active');

    Route::get('/contact', 'ContactController@index');
    Route::post('/contact/{id}', 'ContactController@update');

    Route::resource('lotto', 'LottoController');
    Route::post('/lotto/{id}', 'LottoController@active');
    Route::get('/lottocode/{id}', 'LottoController@code');
    Route::delete('/lottocode/{id}', 'LottoController@destroyactivatecode');
    Route::get('/lottouser/{id}', 'LottoController@user');
    Route::post('/lottodecline/{id}', 'LottoController@decline');
    Route::post('/lottoapprove/{id}', 'LottoController@approve');

    //Prize (Reward)
    Route::get('/prizeuser/{id}', 'PrizeController@user');
    Route::post('/prizedecline/{id}', 'PrizeController@decline');
    Route::post('/prizeapprove/{id}', 'PrizeController@approve');
    Route::post('/prize/{id}', 'PrizeController@active');
    Route::post('/prize/luckydraw/{id}', 'PrizeController@luckydraw');
    Route::post('/prize/reward/{id}', 'PrizeController@reward');
    Route::get('/prizecode/{id}', 'PrizeController@code');
    Route::delete('/prizecode/{id}', 'PrizeController@destroyactivatecode');
    Route::get('/geteventbyclub/{id}', 'PrizeController@getevent');
    Route::get('/getallevent', 'PrizeController@getallevent');
    Route::get('/getallclub', 'PrizeController@getallclub');
    Route::post('/resetreward/{id}', 'PrizeController@resetreward');

    Route::resource('prize', 'PrizeController');
    Route::get('/sendToWon/{id}', 'PrizeController@sendToWon');
    //Vote
    Route::resource('vote', 'VoteController');
    Route::get('/voteuser/{id}', 'VoteController@user');
    Route::post('/vote/{id}', 'VoteController@active');

    //Contest
    Route::resource('contest', 'ContestController');
    Route::post('/contest/register/{id}', 'ContestController@register');
    Route::post('/contest/{id}', 'ContestController@active');
    Route::get('/contestuser/{id}', 'ContestController@user');
    Route::post('/contestapprove/{id}', 'ContestController@approve');
    Route::post('/contestdecline/{id}', 'ContestController@decline');
    Route::post('/contestreset/{id}', 'ContestController@reset');

    Route::post('/generatecode/{id}', 'LottoController@generateCode');

    Route::get('member', 'MemberController@index');
    Route::delete('/memberinactive/{id}', 'MemberController@destroy');
    Route::post('/memberactive/{id}', 'MemberController@active');

});

Route::group(array('middleware' => ['auth']), function() {
    Route::get('redeem', 'RedeemController@index');
    Route::post('redeem', 'RedeemController@redeem')->name('redeem_member');
    Route::post('redeem/confirm/{id}', 'RedeemController@confirm')->name('confirm_member');
    Route::post('/vote/choose/{id}', 'VoteController@choose');
});

Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|club|event|moderator']), function() {
    Route::get('redeem', 'RedeemController@index');
    Route::post('redeem', 'RedeemController@redeem')->name('redeem_admin');
    Route::post('redeem/confirm/{id}', 'RedeemController@confirm');
});
Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|club|moderator']), function() {
    Route::resource('club', 'ClubController');
    Route::post('/club/{id}', 'ClubController@update');
    Route::post('/clubactive/{id}', 'ClubController@active');
});

Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|event|moderator']), function() {
    Route::get('/featuredevent', 'EventController@featured');
    Route::resource('event', 'EventController');
    Route::post('/event/{id}', 'EventController@update');
    Route::post('/eventactive/{id}', 'EventController@active');
    Route::post('/addfeatured', 'EventController@addfeatured');
    Route::post('/removefeatured', 'EventController@removefeatured');
    Route::post('/reorderfeatured', 'EventController@reorderfeatured');
    Route::get('/geteventnotfeatured','EventController@geteventnotfeatured');
});

Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth','role:super-admin|admin|club|event|writer|moderator']), function() {
    Route::get('/', 'CfAdminDashboardController@index');
});

//Route::group(['middleware' => 'auth','role:super-admin|admin|author|editor'], function () {
//    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
//    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
//    // list all lfm routes here...
//});

//Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['auth', 'role:super-admin|admin|club|event|writer|moderator']], function () {
//    'vendor\UniSharp\LaravelFilemanager\Lfm::routes()';
//});
