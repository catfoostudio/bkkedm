@extends('layouts.main')

@section('content')

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A verification link has been sent to your email address. Please check your email') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}

            </div>

        </div>

    </section><!-- #content end -->

@endsection
