@extends('layouts.main')
@section('content')
    <style>
        .mpost .entry-image{
            /*height: 110px;*/
        }
        .eventslider-element .owl-dots .owl-dot{
            margin: 0px 4px 0 4px;
        }
        .eventslider-element .owl-dots{
            margin-top: -20px;
        }
        .eventslider2-element .owl-dots{
            margin-top: -20px;
        }
        .eventslider2-element .owl-dots .owl-dot{
            margin: 0px 4px 0 4px;
        }
        .eventslider2-element .owl-carousel .owl-stage {
            padding: 0px 0;
        }
    </style>
    <section id="slider" class="slider-element revslider-wrap clearfix">

        <div id="rev_slider_224_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-magazine-slider69" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.4.8.1 auto mode -->
            <div id="rev_slider_224_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.1">
                <ul>	<!-- SLIDE  -->
                    <li data-index="rs-582" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="include/rs-plugin/demos/assets/images/transparent.png"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="include/rs-plugin/demos/assets/images/transparent.png" data-bgcolor='#FFF' style='background:#FFF' alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption     rev_group"
                             id="slide-582-layer-11"
                             data-x="['left','left','left','left']" data-hoffset="['30','30','30','20']"
                             data-y="['top','top','top','top']" data-voffset="['30','30','30','20']"
                             data-width="['580','580','718','440']"
                             data-height="['500','500','350','350']"
                             data-whitespace="nowrap"

                             data-type="group"
                             data-responsive_offset="on"

                             data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"x:[100%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;fbr:100;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;fbr:100;","ease":"Power4.easeOut"},{"delay":"wait","speed":650,"frame":"999","to":"opacity:0;fbr:100%;","ease":"Power4.easeOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:125%;","style":"c:rgb(255,255,255);"}]'
                             data-margintop="[0,0,0,0]"
                             data-marginright="[0,0,0,0]"
                             data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]"
                             data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 5; min-width: 580px; max-width: 580px; max-width: 500px; max-width: 500px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;cursor:pointer;">
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-8"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['580','580','718','440']"
                                 data-height="['500','500','350','350']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 6;">
                                <div class="tp-element-background" style=" background: url('{{ asset('images/posts/'.$postsMusic[0]->thumbnail_image) }}') no-repeat center center; background-size: cover; opacity: 1;"></div>  </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-10"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['580','580','718','440']"
                                 data-height="['500','500','350','350']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 data-actions='[{

                                        "event": "click",
                                        "action": "simplelink",
                                        "target": "_self",
                                        "url": "{{url('music/'.$postsMusic[0]->id)}}"

                                    }]'
                                 style="z-index: 7;background:linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1.00) 0%, rgba(0,0,0,0) 75%, rgba(0,0,0,0) 100%);"></div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-4"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['140','170','170','170']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[5,5,5,5]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[5,5,5,5]"

                                 style="z-index: 8; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;background-color:rgb(94,53,177);">{{strtoupper($postsMusic[0]->type)}} </div>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-5"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','50','50','50']"
                                 data-width="['360','360','360','320']"
                                 data-height="none"
                                 data-whitespace="normal"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 9; min-width: 360px; max-width: 360px; white-space: normal; font-size: 30px; line-height: 35px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">{{$postsMusic[0]->name}} </div>
                        </div>
                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption     rev_group"
                             id="slide-582-layer-12"
                             data-x="['left','left','left','left']" data-hoffset="['615','615','30','20']"
                             data-y="['top','top','top','top']" data-voffset="['30','30','385','375']"
                             data-width="['595','595','355','440']"
                             data-height="['280','280','325','160']"
                             data-whitespace="nowrap"

                             data-type="group"
                             data-responsive_offset="on"

                             data-frames='[{"delay":600,"speed":1500,"frame":"0","from":"x:[100%];sX:1;sY:1;opacity:1;fbr:100;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;fbr:100;","ease":"Power4.easeOut"},{"delay":"wait","speed":650,"frame":"999","to":"opacity:0;fbr:100%;","ease":"Power4.easeOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:125%;","style":"c:rgb(255,255,255);"}]'
                             data-margintop="[0,0,0,0]"
                             data-marginright="[0,0,0,0]"
                             data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]"
                             data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 12; min-width: 595px; max-width: 595px; max-width: 280px; max-width: 280px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;cursor:pointer;">
                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-13"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['595','595','355','440']"
                                 data-height="['280','280','325','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 13;">
                                <div class="tp-element-background" style=" background: url('{{ asset('images/posts/'.$postsLifestyle[0]->thumbnail_image) }}') no-repeat center center; background-size: cover; opacity: 1;"></div> </div>

                            <!-- LAYER NR. 10 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-19"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['595','595','355','440']"
                                 data-height="['280','280','325','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 data-actions='[{

                                        "event": "click",
                                        "action": "simplelink",
                                        "target": "_self",
                                        "url": "{{url('lifestyle/'.$postsLifestyle[0]->id)}}"

                                    }]'

                                 style="z-index: 14;background:linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1.00) 0%, rgba(0,0,0,0) 75%, rgba(0,0,0,0) 100%);"> </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-14"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','30','20','20']"
                                 data-fontsize="['30','30','30','20']"
                                 data-lineheight="['35','35','35','25']"
                                 data-width="['360','360','315','315']"
                                 data-height="none"
                                 data-whitespace="normal"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 15; min-width: 360px; max-width: 360px; white-space: normal; font-size: 30px; line-height: 35px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">{{$postsLifestyle[0]->name}} </div>

                            <!-- LAYER NR. 12 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-15"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['130','130','130','110']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[5,5,5,5]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[5,5,5,5]"

                                 style="z-index: 16; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;background-color:rgb(215,0,37);">{{strtoupper($postsLifestyle[0]->type)}} </div>
                        </div>
                        <!-- LAYER NR. 15 -->
                        <div class="tp-caption     rev_group"
                             id="slide-582-layer-27"
                             data-x="['left','left','left','left']" data-hoffset="['615','615','390','20']"
                             data-y="['top','top','top','top']" data-voffset="['315','315','385','540']"
                             data-width="['295','295','358','440']"
                             data-height="['215','215','160','160']"
                             data-whitespace="nowrap"

                             data-type="group"
                             data-responsive_offset="on"

                             data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:[100%];sX:1;sY:1;opacity:1;fbr:100;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;fbr:100;","ease":"Power4.easeOut"},{"delay":"wait","speed":650,"frame":"999","to":"opacity:0;fbr:100%;","ease":"Power4.easeOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:125%;","style":"c:rgb(255,255,255);"}]'
                             data-margintop="[0,0,0,0]"
                             data-marginright="[0,0,0,0]"
                             data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]"
                             data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 19; min-width: 295px; max-width: 295px; max-width: 215px; max-width: 215px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;cursor:pointer;">
                            <!-- LAYER NR. 16 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-28"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['295','295','358','440']"
                                 data-height="['215','215','160','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 20;">
                                <div class="tp-element-background" style=" background: url({{ asset('images/posts/'.$postsMusic[1]->thumbnail_image) }}) no-repeat center center; background-size: cover; opacity: 1;"></div> </div>

                            <!-- LAYER NR. 17 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-33"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['295','295','358','440']"
                                 data-height="['215','215','160','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 data-actions='[{

                                        "event": "click",
                                        "action": "simplelink",
                                        "target": "_self",
                                        "url": "{{url('music/'.$postsMusic[1]->id)}}"

                                    }]'

                                 style="z-index: 21;background:linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1.00) 0%, rgba(0,0,0,0) 75%, rgba(0,0,0,0) 100%);"> </div>

                            <!-- LAYER NR. 18 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-29"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['30','30','30','30']"
                                 data-width="255"
                                 data-height="none"
                                 data-whitespace="normal"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 22; min-width: 255px; max-width: 255px; white-space: normal; font-size: 20px; line-height: 25px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">{{ $postsMusic[1]->name }} </div>

                            <!-- LAYER NR. 19 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-30"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['110','110','110','110']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[5,5,5,5]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[5,5,5,5]"

                                 style="z-index: 23; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: #000000; letter-spacing: 0px;font-family:Roboto;background-color:rgb(124,255,159);">{{ strtoupper($postsMusic[1]->type) }} </div>

                        </div>
                        <!-- LAYER NR. 22 -->
                        <div class="tp-caption     rev_group"
                             id="slide-582-layer-20"
                             data-x="['left','left','left','left']" data-hoffset="['915','915','390','20']"
                             data-y="['top','top','top','top']" data-voffset="['315','315','550','705']"
                             data-width="['295','295','358','440']"
                             data-height="['215','215','160','160']"
                             data-whitespace="nowrap"

                             data-type="group"
                             data-responsive_offset="on"

                             data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];sX:1;sY:1;opacity:1;fbr:100;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;fbr:100;","ease":"Power4.easeOut"},{"delay":"wait","speed":650,"frame":"999","to":"opacity:0;fbr:100%;","ease":"Power4.easeOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:125%;","style":"c:rgb(255,255,255);"}]'
                             data-margintop="[0,0,0,0]"
                             data-marginright="[0,0,0,0]"
                             data-marginbottom="[0,0,0,0]"
                             data-marginleft="[0,0,0,0]"
                             data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 26; min-width: 295px; max-width: 295px; max-width: 215px; max-width: 215px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;cursor:pointer;">
                            <!-- LAYER NR. 23 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-21"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['295','295','358','440']"
                                 data-height="['215','215','160','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 27;">
                                <div class="tp-element-background" style=" background: url({{ asset('images/posts/'.$postsMusic[2]->thumbnail_image) }}) no-repeat center center; background-size: cover; opacity: 1;"></div> </div>

                            <!-- LAYER NR. 24 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                 id="slide-582-layer-26"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-width="['295','295','358','440']"
                                 data-height="['215','215','160','160']"
                                 data-whitespace="normal"

                                 data-type="shape"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 data-actions='[{

                                        "event": "click",
                                        "action": "simplelink",
                                        "target": "_self",
                                        "url": "{{url('music/'.$postsMusic[2]->id)}}"

                                    }]'
                                 style="z-index: 28;background:linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1.00) 0%, rgba(0,0,0,0) 75%, rgba(0,0,0,0) 100%);"> </div>

                            <!-- LAYER NR. 25 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-22"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['30','30','30','30']"
                                 data-width="255"
                                 data-height="none"
                                 data-whitespace="normal"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 29; min-width: 255px; max-width: 255px; white-space: normal; font-size: 20px; line-height: 25px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">{{$postsMusic[2]->name}}</div>

                            <!-- LAYER NR. 26 -->
                            <div class="tp-caption   tp-resizeme"
                                 id="slide-582-layer-23"
                                 data-x="['left','left','left','left']" data-hoffset="['20','20','20','20']"
                                 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['110','110','110','110']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"

                                 data-type="text"
                                 data-responsive_offset="on"

                                 data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                 data-margintop="[0,0,0,0]"
                                 data-marginright="[0,0,0,0]"
                                 data-marginbottom="[0,0,0,0]"
                                 data-marginleft="[0,0,0,0]"
                                 data-textAlign="['inherit','inherit','inherit','inherit']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[5,5,5,5]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[5,5,5,5]"

                                 style="z-index: 30; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;background-color:rgb(0,109,210);">{{ strtoupper($postsMusic[2]->type) }} </div>

                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div><!-- END REVOLUTION SLIDER -->

    </section>
    <section id="content">
        <div class="content-wrap" style="padding-top: 15px;padding-bottom: 0px;">
            <div class="container clearfix">
                <div class="fancy-title title-border" style="margin-bottom: 5px;">
                    <h3>TOP NEWS</h3>
                </div>
                {{--<div class="row bottommargin-sm clearfix" id="allStories">--}}
                    {{--<div class="col-lg-8" style="margin-bottom: 10px;">--}}
                        {{--@if(!empty($firstMusicStory))--}}
                            {{--<div class="ipost clearfix">--}}
                                {{--<div class="entry-image" style="margin-bottom: 5px">--}}
                                    {{--<a href="{{ url($firstMusicStory->type.'/'.$firstMusicStory->id.'/'.str_replace(' ','-',$firstMusicStory->name)) }}"><img class="image_fade"--}}
                                                                                            {{--src="{{ asset('images/posts/'.$firstMusicStory->thumbnail_image) }}"--}}
                                                                                            {{--alt="{{ $firstMusicStory->name }}"></a>--}}
                                {{--</div>--}}
                                {{--<div class="entry-title">--}}
                                    {{--<span style="font-weight: 400">{{ ucfirst($firstMusicStory->type) }}</span>--}}
                                    {{--<h2>--}}
                                        {{--<a href="{{ url($firstMusicStory->type.'/'.$firstMusicStory->id.'/'.str_replace(' ','-',$firstMusicStory->name)) }}">{{ $firstMusicStory->name }}</a>--}}
                                    {{--</h2>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-4">--}}
                        {{--<span style="font-weight: bold;font-size:18px;text-transform:uppercase;">Top Stories</span>--}}
                        {{--<div class="btn-group">--}}
                            {{--<a href="#" class="button button-3d button-rounded button-black dropdown-toggle"--}}
                               {{--data-toggle="dropdown"--}}
                               {{--aria-haspopup="true" aria-expanded="false">All</a>--}}
                            {{--<div class="dropdown-menu">--}}
                                {{--<a class="dropdown-item selectMusicStories" href="#">Music</a>--}}
                                {{--<a class="dropdown-item selectLifestyleStories" href="#">Lifestyle</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@if(!empty($nextAllThreeStories))--}}
{{--                            @foreach($nextAllThreeStories as $story)--}}
                                {{--<!-- 2 -->--}}
                                {{--<div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">--}}
                                    {{--<div class="entry-image" style="width: 135px;">--}}
                                        {{--<a href="{{url($firstLifestyleStory->type.'/'.$firstLifestyleStory->id.'/'.str_replace(' ','-',$firstLifestyleStory->name))}}"><img--}}
                                                    {{--src="{{ asset('images/posts/'.$firstLifestyleStory->thumbnail_image) }}"--}}
                                                    {{--style="object-fit: cover;height: auto;width: 135px;"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="entry-c">--}}
                                        {{--<div class="entry-title">--}}
                                            {{--<span style="font-weight: 400">{{ ucfirst($firstLifestyleStory->type) }}</span>--}}
                                            {{--<h4 style="font-size: 15px;"><a--}}
                                                        {{--href="{{url($firstLifestyleStory->type.'/'.$firstLifestyleStory->id.'/'.str_replace(' ','-',$firstLifestyleStory->name))}}">{{ $firstLifestyleStory->name }}</a></h4>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- 3 -->--}}
                                {{--<div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">--}}
                                    {{--<div class="entry-image" style="width: 135px;">--}}
                                        {{--<a href="{{url($nextMusicThreeStories[0]->type.'/'.$nextMusicThreeStories[0]->id.'/'.str_replace(' ','-',$nextMusicThreeStories[0]->name))}}"><img--}}
                                                    {{--src="{{ asset('images/posts/'.$nextMusicThreeStories[0]->thumbnail_image) }}"--}}
                                                    {{--style="object-fit: cover;height: auto;width: 135px;"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="entry-c">--}}
                                        {{--<div class="entry-title">--}}
                                            {{--<span style="font-weight: 400">{{ ucfirst($nextMusicThreeStories[0]->type) }}</span>--}}
                                            {{--<h4 style="font-size: 15px;"><a--}}
                                                        {{--href="{{url($nextMusicThreeStories[0]->type.'/'.$nextMusicThreeStories[0]->id.'/'.str_replace(' ','-',$nextMusicThreeStories[0]->name))}}">{{ $nextMusicThreeStories[0]->name }}</a></h4>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- 4 -->--}}
                                {{--<div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">--}}
                                    {{--<div class="entry-image" style="width: 135px;">--}}
                                        {{--<a href="{{url($nextLifestyleThreeStories[0]->type.'/'.$nextLifestyleThreeStories[0]->id.'/'.str_replace(' ','-',$nextLifestyleThreeStories[0]->name))}}"><img--}}
                                                    {{--src="{{ asset('images/posts/'.$nextLifestyleThreeStories[0]->thumbnail_image) }}"--}}
                                                    {{--style="object-fit: cover;height: auto;width: 135px;"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="entry-c">--}}
                                        {{--<div class="entry-title">--}}
                                            {{--<span style="font-weight: 400">{{ ucfirst($nextLifestyleThreeStories[0]->type) }}</span>--}}
                                            {{--<h4 style="font-size: 15px;"><a--}}
                                                        {{--href="{{url($nextLifestyleThreeStories[0]->type.'/'.$nextLifestyleThreeStories[0]->id.'/'.str_replace(' ','-',$nextLifestyleThreeStories[0]->name))}}">{{ $nextLifestyleThreeStories[0]->name }}</a></h4>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- 5 -->--}}
                                {{--<div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">--}}
                                    {{--<div class="entry-image" style="width: 135px;">--}}
                                        {{--<a href="{{url($nextMusicThreeStories[1]->type.'/'.$nextMusicThreeStories[1]->id.'/'.str_replace(' ','-',$nextMusicThreeStories[1]->name))}}"><img--}}
                                                    {{--src="{{ asset('images/posts/'.$nextMusicThreeStories[1]->thumbnail_image) }}"--}}
                                                    {{--style="object-fit: cover;height: auto;width: 135px;"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="entry-c">--}}
                                        {{--<div class="entry-title">--}}
                                            {{--<span style="font-weight: 400">{{ ucfirst($nextMusicThreeStories[1]->type) }}</span>--}}
                                            {{--<h4 style="font-size: 15px;"><a--}}
                                                        {{--href="{{url($nextMusicThreeStories[1]->type.'/'.$nextMusicThreeStories[1]->id.'/'.str_replace(' ','-',$nextMusicThreeStories[1]->name))}}">{{ $nextMusicThreeStories[1]->name }}</a></h4>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="row bottommargin-sm clearfix" id="musicStories">
                    <div class="col-lg-8" style="margin-bottom: 10px;">
                        @if(!empty($firstMusicStory))
                            <div class="ipost clearfix">
                                <div class="entry-image" style="margin-bottom: 5px;">
                                    <a href="{{ url('music/'.$firstMusicStory->id.'/'.str_replace(' ','-',$firstMusicStory->name)) }}"><img class="image_fade" src="{{ asset('images/posts/'.$firstMusicStory->thumbnail_image) }}" alt="{{ $firstMusicStory->name }}"></a>
                                </div>
                                <div class="entry-title">
                                    <span style="font-weight: 400">{{ ucfirst('music') }}</span>
                                    <h2>
                                        <a href="{{ url('music/'.$firstMusicStory->id.'/'.str_replace(' ','-',$firstMusicStory->name)) }}">{{ $firstMusicStory->name }}</a>
                                    </h2>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        {{--<span style="font-weight: bold;font-size:18px;text-transform:uppercase;">Top Stories</span>--}}
                        <div class="btn-group">
                            <a href="#" class="button button-3d button-rounded button-black dropdown-toggle"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">Music</a>
                            <div class="dropdown-menu">
                                {{--<a class="dropdown-item selectAllStories" href="#">All</a>--}}
                                <a class="dropdown-item selectLifestyleStories" href="#">Lifestyle</a>
                            </div>
                        </div>
                        @if(!empty($nextMusicThreeStories))
                            @foreach($nextMusicThreeStories as $story)
                                <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">
                                    <div class="entry-image" style="margin-bottom: 5px">
                                        <a href="{{url('music/'.$story->id.'/'.str_replace(' ','-',$story->name))}}"><img
                                                    src="{{ asset('images/posts/'.$story->thumbnail_image) }}" alt=""
                                                    style="object-fit: cover;height: auto;"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <span style="font-weight: 400">{{ ucfirst($story->type) }}</span>
                                            <h4 style="font-size: 16px;"><a
                                                        href="{{url('music/'.$story->id.'/'.str_replace(' ','-',$story->name))}}">{{ $story->name }}</a></h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row bottommargin-sm clearfix" id="lifestyleStories" style="display: none">

                    <div class="col-lg-8" style="margin-bottom: 10px;">
                        @if(!empty($firstLifestyleStory))
                            @if(!empty($firstLifestyleStory))
                                <div class="ipost clearfix">
                                    <div class="entry-image" style="margin-bottom: 5px;">
                                        <a href="{{ url('lifestyle/'.$firstLifestyleStory->id.'/'.str_replace(' ','-',$firstLifestyleStory->name)) }}"><img
                                                    class="image_fade"
                                                    src="{{ asset('images/posts/'.$firstLifestyleStory->thumbnail_image) }}"></a>
                                    </div>
                                    <div class="entry-title">
                                        <span style="font-weight: 400">{{ ucfirst($firstLifestyleStory->type) }}</span>
                                        <h2>
                                            <a href="{{ url('lifestyle/'.$firstLifestyleStory->id.'/'.str_replace(' ','-',$firstLifestyleStory->name)) }}">{{ $firstLifestyleStory->name }}</a>
                                        </h2>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>

                    <div class="col-lg-4">
                        {{--<span style="font-weight: bold;font-size:18px;text-transform:uppercase;">Top Stories</span>--}}
                        <div class="btn-group">
                            <a href="#" class="button button-3d button-rounded button-black dropdown-toggle"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">Lifestyle</a>
                            <div class="dropdown-menu">
                                {{--<a class="dropdown-item selectAllStories" href="#">All</a>--}}
                                <a class="dropdown-item selectMusicStories" href="#">Music</a>
                            </div>
                        </div>
                        @if(!empty($nextLifestyleThreeStories))
                            @foreach($nextLifestyleThreeStories as $story)
                                <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">
                                    <div class="entry-image" style="margin-bottom: 5px">
                                        <a href="{{ url('lifestyle/'.$story->id.'/'.str_replace(' ','-',$story->name)) }}"><img
                                                    src="{{ asset('images/posts/'.$story->thumbnail_image) }}"
                                                    style="object-fit: cover;height: auto;"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <span style="font-weight: 400">{{ ucfirst($story->type) }}</span>
                                            <h4 style="font-size: 16px;"><a
                                                        href="{{url('lifestyle/'.$story->id.'/'.str_replace(' ','-',$story->name))}}">{{ $story->name }}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

            <div class="section dark" style="padding: 20px 0;margin: 0px 0;background-color: #000">

                <div class="container clearfix">
                    <h3 style="margin: 0px 0;">EVENTS</h3>
                    {{--<iframe src="http://player.vimeo.com/video/99895335" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>--}}
                    {{--<img src="https://s3-ap-southeast-1.amazonaws.com/tm-img-banner/banner-MHRS Banner.png"--}}
                    {{--alt="Ad" class="aligncenter" style="margin-bottom: 20px">--}}
                    <section  class="eventslider-element boxed-slider" style="padding: 0px 0;">
                    <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1"
                         data-animate-in="zoomIn" data-speed="450" data-animate-out="fadeOut">
                        @foreach($featuredEvents as $featuredEvent)
                            <a href="{{ url('event/'.$featuredEvent->id) }}"><img
                                        src="{{ (empty($featuredEvent->cover_image)) ? asset('images/eventcover_dummy.png') : asset('images/events/cover_images/'.$featuredEvent->cover_image) }}"
                                        alt="Slider"></a>
                        @endforeach
                    </div>
                    </section>
                    <section  class="eventslider2-element boxed-slider" style="padding: 0px 0;">
                    <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="30"
                         data-nav="true" data-items-xs="2" data-items-sm="2" data-items-md="3" data-items-xl="4">
                        @foreach($events as $event)
                            <div class="oc-item">
                                <div class="ipost clearfix">
                                    <div class="entry-image" style="margin-bottom: 10px;">
                                        <a href="{{ url('event/'.$event->id) }}"><img class="image_fade"
                                                                                      style="object-fit: cover"
                                                                                      src="{{ asset('images/events/posters/'.$event->poster_image) }}"></a>
                                    </div>
                                    <div class="entry-title" style="height:48px;overflow: hidden">
                                        <h3><a href="{{ url('event/'.$event->id) }}">{{ $event->name }}</a></h3>
                                    </div>
                                    <ul class="iconlist" style="font-size:13px;margin-left: 0px;height: 35px;">
                                        <li style="height:20px;overflow: hidden;">
                                            <i class="icon-time" style="position:unset;margin-right: 5px;"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS F Y') }}
                                        </li>
                                        <li style="height:20px;overflow: hidden;"><i class="icon-map-marker" style="position:unset;margin-right: 5px;"></i>{{ $event->location }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    </section>
                    <a href="{{ url('event') }}" class="button button-3d button-rounded button-white button-light">MORE
                        EVENTS</a>
                </div>
            </div>
            {{--<div class="container clearfix">--}}

                {{--<div class="fancy-title title-border">--}}
                    {{--<h3>VIDEOS</h3>--}}
                {{--</div>--}}

                {{--<div class="row bottommargin-sm clearfix">--}}

                    {{--<div class="col-lg-8 bottommargin">--}}
                        {{--@if(!empty($firstVideo))--}}
                            {{--<div class="ipost clearfix">--}}
                                {{--<div class="entry-image">--}}
                                    {{--<a href="{{ url('video/'.$firstVideo->id) }}">--}}
                                        {{--<img src="https://img.youtube.com/vi/{{ $firstVideo->youtube_id }}/sddefault.jpg"--}}
                                             {{--style="object-fit: cover;">--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="entry-title">--}}
                                    {{--<h2><a href="{{ url('video/'.$firstVideo->id) }}">{{ $firstVideo->name }}</a></h2>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-4 bottommargin">--}}
                        {{--<a href="{{ url('video') }}" class="button button-3d button-rounded button-black">MORE--}}
                            {{--VIDEOS</a>--}}
                        {{--@foreach($nextVideosThreeStories as $video)--}}
                            {{--<div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">--}}
                                {{--<div class="entry-image">--}}
                                    {{--<a href="{{ url('video/'.$video->id) }}">--}}
                                        {{--<img src="https://img.youtube.com/vi/{{ $video->youtube_id }}/sddefault.jpg"--}}
                                             {{--style="object-fit: cover;">--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="entry-c">--}}
                                    {{--<div class="entry-title">--}}
                                        {{--<h4><a href="{{ url('video/'.$video->id) }}">{{ $video->name }}</a></h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

        </div>

    </section>
@endsection
@section('script')
    <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
    <script src="include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION EXTENSIONS  -->
    <script src="include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
    <script>
        $(document).ready(function () {

            $('.selectMusicStories').click(function (e) {
                $('#allStories').hide();
                $('#lifestyleStories').hide();
                $('#musicStories').show();
            });

            $('.selectLifestyleStories').click(function (e) {
                $('#allStories').hide();
                $('#musicStories').hide();
                $('#lifestyleStories').show();
            });

            $('.selectAllStories').click(function (e) {

                $('#allStories').show();
                $('#musicStories').hide();
                $('#lifestyleStories').hide();
            });

        });
    </script>
    <!-- ADD-ONS JS FILES -->
    <script>
        var revapi224,
            tpj;
        (function() {
            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();

            function onLoad() {
                if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                if(tpj("#rev_slider_224_1").revolution == undefined){
                    revslider_showDoubleJqueryError("#rev_slider_224_1");
                }else{
                    revapi224 = tpj("#rev_slider_224_1").show().revolution({
                        sliderType:"standard",
                        jsFileLocation:"include/rs-plugin/js/",
                        sliderLayout:"auto",
                        dottedOverlay:"none",
                        delay:9000,
                        revealer: {
                            direction: "bltr_skew",
                            color: "#ffffff",
                            duration: "1000",
                            delay: "0",
                            easing: "Power2.easeOut",
                            spinner: "2",
                            spinnerColor: "rgba(0,0,0,",
                        },
                        navigation: {
                            keyboardNavigation:"off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation:"off",
                            mouseScrollReverse:"default",
                            onHoverStop:"off",
                            touch:{
                                touchenabled:"on",
                                touchOnDesktop:"off",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            }
                            ,
                            arrows: {
                                style:"gyges",
                                enable:false,
                                hide_onmobile:false,
                                hide_onleave:false,
                                hide_delay:200,
                                hide_delay_mobile:1200,
                                tmp:'',
                                left: {
                                    h_align:"left",
                                    v_align:"center",
                                    h_offset:20,
                                    v_offset:0
                                },
                                right: {
                                    h_align:"right",
                                    v_align:"center",
                                    h_offset:20,
                                    v_offset:0
                                }
                            }
                        },
                        responsiveLevels:[1240,1240,778,480],
                        visibilityLevels:[1240,1240,778,480],
                        gridwidth:[1240,1240,778,480],
                        gridheight:[560,560,740,895],
                        lazyType:"none",
                        shadow:0,
                        spinner:"spinner0",
                        stopLoop:"on",
                        stopAfterLoops:0,
                        stopAtSlide:1,
                        shuffle:"off",
                        autoHeight:"off",
                        disableProgressBar:"on",
                        hideThumbsOnMobile:"off",
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        debugMode:false,
                        fallbacks: {
                            simplifyAll:"off",
                            nextSlideOnWindowFocus:"off",
                            disableFocusListener:false,
                        }
                    });
                }; /* END OF revapi call */

                RsRevealerAddOn(tpj, revapi224, "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-2'><div class='rsaddon-revealer-2' style='border-top-color: {{ '&#123;&#123;' . 'color'. '&#125;&#125;' }}0.65); border-bottom-color: {{ '&#123;&#123;' . 'color'. '&#125;&#125;' }}0.15); border-left-color: {{ '&#123;&#123;' . 'color'. '&#125;&#125;' }}0.65); border-right-color: {{ '&#123;&#123;' . 'color'. '&#125;&#125;' }}0.15)'><\/div><\/div>");

                if(typeof ExplodingLayersAddOn !== "undefined") ExplodingLayersAddOn(tpj, revapi224);
            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
@endsection