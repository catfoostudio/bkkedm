<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    protected $table = 'choice_user';

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function choice()
    {
        return $this->hasOne('App\Models\Choice', 'id', 'choice_id');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Voted';
        }
        elseif($this->status == 2){
            return 'Not Available';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Decline';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Voted</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Not Available</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Decline</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public static function convertStatusBadge($status)
    {
        if($status == 1){
            return '<span class="label label-success">Voted</span>';
        }
        elseif($status == 2){
            return '<span class="label label-warning">Not Available</span>';
        }
        elseif($status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($status == 0){
            return '<span class="label label-danger">Decline</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }
}
