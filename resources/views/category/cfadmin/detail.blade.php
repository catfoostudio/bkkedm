@extends('.layouts.cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit {{ ucfirst($category->type) }} Category
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/category') }}">Category</a></li>
                <li class="active">Add Category</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            {{--@if (count($errors) > 0)--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>ขออภัย!</strong><br><br>--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Category Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'PUT' , 'action' => ['CategoryController@update', $category],'id' => 'doesntSubmit']) !!}
                        <div class="box-body form">
                            <div class="form-group{{($errors->has('name') ? ' has-error' : '' )}}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Name <span style="color: orangered">*required</span></label>
                                        {!! Form::text('name',old('name' , $category->name),array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Update</button>
                            <a href="{{ url('cfadmin/category') }}">
                                <button type="button" class="btn btn-default pull-right">Cancel</button>
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')

    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
@endsection
