<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class News extends Model
{
    //
    use HasRoles;

    protected $guard_name = 'web'; // or whatever guard you want to use
}
