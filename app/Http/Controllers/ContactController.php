<?php

namespace App\Http\Controllers;

use App\Mail\ReceivedMessage;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if($this->isAdminRequest()) {
            $view = 'contact.cfadmin.detail';
        }
        else{
            $view = 'contact.index';
        }
        $config = Config::where('name', 'contact_1')->first();
        $config2 = Config::where('name', 'contact_2')->first();
        $config3 = Config::where('name', 'contact_3')->first();

        return view($view, [
            'config' => $config,
            'config2' => $config2,
            'config3' => $config3,
        ]);
    }

    public function sendmail(Request $request)
    {
        $input = $request->input();

        Mail::to('bkkedm@gmail.com')->send(new ReceivedMessage($input));

        return Response::json(array('alert' => 'success', 'message' => 'Message has been sent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $config = Config::where('name', 'contact_1')->first();
        $config2 = Config::where('name', 'contact_2')->first();
        $config3 = Config::where('name', 'contact_3')->first();

        $input = $request->input();

        $config->text_value = $input['map'];
        $config->save();

        $config2->text_value = $input['contact'];
        $config2->save();

        $config3->text_value = $input['facebook'];
        $config3->save();

        $request->session()->flash('alert-success', 'Contact was successful updated!');

        return redirect('cfadmin/contact');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
