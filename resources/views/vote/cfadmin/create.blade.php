@extends('.layouts.cfadmin')
@section('content')
    <style>
        .form-group{
            margin-bottom: 5px !important;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Vote
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/vote') }}">Vote</a></li>
                <li class="active">Add Vote</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            {{--@if (count($errors) > 0)--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>ขออภัย!</strong><br><br>--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Vote Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'POST' , 'action' => ['VoteController@store'],'id' => 'form-addmusic']) !!}
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('name') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Name <span style="color: red">*required</span></label>
                                        {!! Form::text('name',old('name'),array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('filepath') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Poster Image (w:264 x h:373) <span style="color: red">*required</span></label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                                    <i class="fa fa-picture-o"></i> Choose
                                                </a>
                                            </span>
                                            {!! Form::text('filepath',old('filepath'),array('class' => 'form-control','id'=>'thumbnail')) !!}
                                        </div>
                                        </div>
                                        {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('start') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Start Date <span style="color: red">*required</span></label>
                                        <input class="form-control" name="start" id="eventdate" value="{{ old('start') }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('end') ? ' has-error' : '' )}}">
                                            <label for="inputEmail3">End Date <span style="color: red">*required</span></label>
                                        <input class="form-control" name="end" id="eventenddate" value="{{ old('end') }}" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Vote type: </label>
                                    <input type="radio" value="1" name="type" {{ (old('type') == '1') ? 'checked=checked' : '' }} checked="checked" id="type_text"> <label for="type_text" style="margin-right: 10px;">Text list</label>
                                    <input type="radio" value="2" name="type" {{ (old('type') == '2') ? 'checked=checked' : '' }} id="type_checkbox" > <label for="type_checkbox" style="margin-right: 10px;">Checkbox</label>
                                    <input type="radio" value="3" name="type" {{ (old('type') == '3') ? 'checked=checked' : '' }} id="type_thumbnail" > <label for="type_thumbnail" style="margin-right: 10px;">Thumbnail with text</label>
                                    <input type="radio" value="4" name="type" {{ (old('type') == '4') ? 'checked=checked' : '' }} id="type_inputbox" > <label for="type_inputbox" style="margin-right: 10px;">Input box</label>
                                </div>
                            </div></div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Custom Tabs -->
                                        <div class="nav-tabs-custom">
                                            <ul class="nav nav-tabs" style="display: none">
                                                <li {{ (old('type') == '1' || empty(old('type'))) ? 'class=active' : '' }}><a href="#tab_1" >Text list</a></li>
                                                <li {{ (old('type') == '2') ? 'class=active' : '' }}><a href="#tab_2" >Checkbox</a></li>
                                                <li {{ (old('type') == '3') ? 'class=active' : '' }}><a href="#tab_3" >Thumbnail with text</a></li>
                                                <li {{ (old('type') == '4') ? 'class=active' : '' }}><a href="#tab_4" >Input box</a></li>
                                            </ul>

                                            @if($errors->has('textlist_name') || $errors->has('checkbox_name') || $errors->has('choice_name'))
                                                <div class="form-group has-error">
                                                <span class="form-group help-block">กรุณาระบุตัวเลือกขั้นต่ำ 2ตัวเลือก</span>
                                                </div>
                                            @endif

                                            <div class="tab-content">
                                                <div class="tab-pane {{ (old('type') == '1' || empty(old('type'))) ? 'active' : '' }}" id="tab_1">
                                                    <label for="inputEmail3">Choices</label> <button type="button" id="add_textlist">Add</button>
                                                    {!! Form::hidden('textlist_amount', old('textlist_amount',1)) !!}
                                                    <div id="textlist_area">
                                                        @if(!empty(old('textlist_name')))
                                                            <? $i=0 ?>
                                                        @foreach(old('textlist_name') as $textlistname)
                                                        <div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">
                                                            <div class="box-body">

                                                                    <div class="col-md-1">
                                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                                                            <i class="fa fa-times"></i></button>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <div class="form-group{{($errors->has('textlist_name.'.$i) ? ' has-error' : '' )}}" style="margin-bottom: 0px;">
                                                                            <input class="form-control" type="text" name="textlist_name[]" value="{{ $textlistname }}">
                                                                        </div>
                                                                    </div>

                                                            </div>
                                                            <!-- /.box-body -->
                                                        </div>
                                                                    <? $i++ ?>
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                                <div class="tab-pane {{ (old('type') == '2') ? 'active' : '' }}" id="tab_2">
                                                    <label for="inputEmail3">Choices</label> <button type="button" id="add_checkbox">Add</button>
                                                    <div id="checkbox_area">
                                                        {!! Form::hidden('checkbox_amount', old('checkbox_amount',1)) !!}
                                                        @if(!empty(old('checkbox_name')))
                                                            <? $i=0 ?>
                                                            @foreach(old('checkbox_name') as $checkbox_name)
                                                            <div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">
                                                                <div class="box-body">

                                                                        <div class="col-md-1">
                                                                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                                                                <i class="fa fa-times"></i></button>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="form-group{{($errors->has('checkbox_name.'.$i) ? ' has-error' : '' )}}" style="margin-bottom: 0px;">
                                                                            <input class="form-control" type="text" name="checkbox_name[]" value="{{ $checkbox_name }}">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /.box-body -->
                                                            </div>
                                                                <? $i++ ?>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                                <div class="tab-pane {{ (old('type') == '3') ? 'active' : '' }}" id="tab_3">
                                                    <label for="inputEmail3">Choices</label> <button type="button" id="add_choice">Add</button>
                                                    {!! Form::hidden('choice_amount', old('choice_amount',1)) !!}
                                                    <div id="choice_area">
                                                        @if(!empty(old('choice_name')))
                                                            <? $i=0 ?>
                                                            @foreach(old('choice_name') as $choice_name)

                                                        <div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">
                                                            <div class="box-body">

                                                                    <div class="col-md-1">
                                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                                                            <i class="fa fa-times"></i></button>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <div class="form-group{{($errors->has('choice_name.'.$i) ? ' has-error' : '' )}}" style="margin-bottom: 0px;">
                                                                            <input class="form-control" type="text" name="choice_name[]" value="{{ $choice_name }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group{{($errors->has('choice_image.'.$i) ? ' has-error' : '' )}}" style="margin-bottom: 0px;">
                                                                            <div class="input-group">
                                                                            <span class="input-group-btn">
                                                                                <a data-input="thumbnail_{{$i}}" data-preview="holder" class="btn btn-primary lfm">
                                                                                    <i class="fa fa-picture-o"></i> Choose
                                                                                </a>
                                                                            </span>
                                                                                {!! Form::text('choice_image[]', old('choice_image.'.$i) ,array('class' => 'form-control','id'=>'thumbnail_'.$i)) !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                            </div>
                                                            <!-- /.box-body -->
                                                        </div>
                                                                    <? $i++ ?>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="tab-pane {{ (old('type') == '4') ? 'active' : '' }}" id="tab_4">
                                                    <div class="form-group{{($errors->has('inputbox_amount') ? ' has-error' : '' )}}">
                                                    <label>Number of answer: </label> <input type="number" class="form-control" name="inputbox_amount" value="{{ old('inputbox_amount') }}">
                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- nav-tabs-custom -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{($errors->has('content') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Description <span style="color: red">*required</span></label>
                                            <div {{($errors->has('content') ? 'class=ckeditor-error' : '' )}}>
                                        <textarea id="my-editor" name="content" class="form-control">{!! old('content', '') !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Remark</label>
                                        <textarea name="remark" class="form-control">{!! old('remark', '') !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Tags (use Tab button or " , ")</label><br>
                                        <input id="tags" name="tags" class="form-control" type="text" value="{{ old('tags') }}" data-role="tagsinput"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input id="status" name="status" type="hidden" value="1">
                            <button type="submit" class="btn btn-success" id="btn-publish">Publish</button>
                            <button type="button" class="btn btn-warning" id="btn-draft" style="margin-left: 5px;">Draft</button>
                            <a href="{{ url('cfadmin/vote') }}">
                                <button type="button" class="btn btn-default pull-right">Cancel</button>
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- page script -->
    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            // filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            //filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removeButtons: 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Flash,Anchor,Language,BidiLtr,BidiRtl,ShowBlocks',
            contentsCss : [ '{{ asset('css/bootstrap.css') }}', '{{ asset('style.css') }}' ],
            height:300,
        };

        $(document).ready(function() {

            $('#btn-draft').click(function () {
                $('#status').val(2);
                $(this).prop("disabled", true);
                $('#btn-publish').prop("disabled", true);
                $('#form-addmusic').submit();
            });

            $('#btn-publish').click(function () {
                $(this).prop("disabled", true);
                $('#btn-draft').prop("disabled", true);
                $('#form-addmusic').submit();
            });

            $('input[name=type]').change(function () {
                var type = $(this).val();
                var domname = "#tab_"+type;
                $('[href="'+domname+'"]').tab('show');
            });




            $('#add_textlist').click(function () {

                var choice = '<div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">\n' +
                    '                                            <div class="box-body">\n' +
                    '                                                <div class="form-group">\n' +
                    '                                                    <div class="col-md-1">\n' +
                    '                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">\n' +
                    '                                                            <i class="fa fa-times"></i></button>\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="col-md-5">\n' +
                    '                                                        <input class="form-control" type="text" name="textlist_name[]">\n' +
                    '\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                            <!-- /.box-body -->\n' +
                    '                                        </div>';

                $('#textlist_area').append(choice);

                $('.box').boxWidget();
            });

            $('#add_checkbox').click(function () {

                var choice = '<div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">\n' +
                    '                                            <div class="box-body">\n' +
                    '                                                <div class="form-group">\n' +
                    '                                                    <div class="col-md-1">\n' +
                    '                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">\n' +
                    '                                                            <i class="fa fa-times"></i></button>\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="col-md-5">\n' +
                    '                                                        <input class="form-control" type="text" name="checkbox_name[]">\n' +
                    '\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                            <!-- /.box-body -->\n' +
                    '                                        </div>';

                $('#checkbox_area').append(choice);

                $('.box').boxWidget();
            });

            $('#add_choice').click(function () {

                var choice_amount = $("input[name='choice_amount']");
                choice_amount.val(+choice_amount.val()+1);

                var choice = '<div class="box" style="border: none;box-shadow: none;margin-bottom: -10px;">\n' +
                    '                                            <div class="box-body">\n' +
                    '                                                <div class="form-group">\n' +
                    '                                                    <div class="col-md-1">\n' +
                    '                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">\n' +
                    '                                                            <i class="fa fa-times"></i></button>\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="col-md-5">\n' +
                    '                                                        <input class="form-control" type="text" name="choice_name[]">\n' +
                    '\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="col-md-6">\n' +
                    '                                                        <div class="input-group">\n' +
                    '                                                        <span class="input-group-btn">\n' +
                    '                                                            <a data-input="thumbnail_'+choice_amount.val()+'" data-preview="holder" class="btn btn-primary lfm">\n' +
                    '                                                                <i class="fa fa-picture-o"></i> Choose\n' +
                    '                                                            </a>\n' +
                    '                                                        </span>\n' +
                    '                                                           <input type="text" name="choice_image[]" class="form-control" id="thumbnail_'+ choice_amount.val()+'"'+
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                            <!-- /.box-body -->\n' +
                    '                                        </div>';

                $('#choice_area').append(choice);

                $('.box').boxWidget();
                $('.lfm').filemanager('image');
            });

            $('.lfm').filemanager('image');

            // If you want inserted images in a CKEditor to be responsive
// you can use the following code. It creates a htmlfilter for the
// image tag that replaces inline "width" and "style" definitions with
// their corresponding attributes and add's (in this example) the
// Bootstrap "img-responsive" class.
            CKEDITOR.on('instanceReady', function (ev) {
                ev.editor.dataProcessor.htmlFilter.addRules( {
                    elements : {
                        img: function( el ) {
                            // Add bootstrap "img-responsive" class to each inserted image
                            el.addClass('img-responsive');

                            // Remove inline "height" and "width" styles and
                            // replace them with their attribute counterparts.
                            // This ensures that the 'img-responsive' class works
                            var style = el.attributes.style;

                            if (style) {
                                // Get the width from the style.
                                var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
                                    width = match && match[1];

                                // Get the height from the style.
                                match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                                var height = match && match[1];

                                // Replace the width
                                if (width) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, '');
                                    // el.attributes.width = width;
                                }

                                // Replace the height
                                if (height) {
                                    el.attributes.style = el.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, 'max-height:'+height+'px;');
                                    // el.attributes.height = height;
                                }
                            }

                            // Remove the style tag if it is empty
                            if (!el.attributes.style)
                                delete el.attributes.style;
                        }
                    }
                });
            });
            CKEDITOR.replace('my-editor', options);
            $('#lfm').filemanager('image');

            $('.bootstrap-tagsinput input').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#tags").tagsinput({
                cancelConfirmKeysOnEmpty: true,
                confirmKeys: [13],
            });

            $("#tags").parent().find('input').on('keydown',function (e) {

                if (e.which == 9)
                {
                    $("#tags").tagsinput('add',$(this).val());
                    $(this).val("");
                    e.preventDefault();
                }

            });

            $('#eventdate').datepicker({
                autoclose: true,
                zIndexOffset: 1050
            })

            $('#eventenddate').datepicker({
                autoclose: true,
                zIndexOffset: 1050
            })
        });

    </script>
@endsection
