@extends('layouts.main')

@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title">
        <div class="container clearfix">
            <h1>Contact</h1>
            <span>Get in Touch with Us</span>
        </div>
    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                {{--<div class="row">--}}
                    {{--<!-- Google Map--}}
                    {{--============================================= -->--}}
                    {{--<div class="col-lg-6 bottommargin">--}}
                        {{--{!! $config->text_value  !!}--}}
                    {{--</div><!-- Google Map End -->--}}

                    {{--<div class="col-lg-6">--}}

                        {{--<!-- Contact Info--}}
                        {{--============================================= -->--}}
                        {{--<div class="col_two_fifth">--}}
                            {{--{!! $config2->text_value  !!}--}}
                        {{--</div><!-- Contact Info End -->--}}

                        {{--<!-- Testimonials--}}
                        {{--============================================= -->--}}
                        {{--<div class="col_three_fifth col_last">--}}

                            {{--<div class="widget notoppadding noborder">--}}
                                {{--{!! $config3->text_value  !!}--}}
                            {{--</div>--}}

                        {{--</div><!-- Testimonial End -->--}}

                        {{--<div class="clear"></div>--}}

                        {{--<!-- Modal Contact Form--}}
                        {{--============================================= -->--}}
                        {{--<a href="#" data-toggle="modal" data-target="#contactFormModal" class="button button-3d nomargin btn-block button-xlarge d-none d-md-block center">Click here to Send an Email</a>--}}
                        {{--<a href="#" data-toggle="modal" data-target="#contactFormModal" class="button button-3d nomargin btn-block d-block d-md-none center">Send an Email</a>--}}

                        {{--<div class="modal fade" id="contactFormModal" tabindex="-1" role="dialog" aria-labelledby="contactFormModalLabel" aria-hidden="true">--}}
                            {{--<div class="modal-dialog">--}}
                                {{--<div class="modal-content">--}}
                                    {{--<div class="modal-header">--}}
                                        {{--<h4 class="modal-title" id="contactFormModalLabel">Send Us an Email</h4>--}}
                                        {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="modal-body">--}}

                                        {{--<div class="contact-widget">--}}
                                            {{--<div class="contact-form-result"></div>--}}
                                            {{--<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">--}}

                                                {{--<div class="form-process"></div>--}}

                                                {{--<div class="col_half">--}}
                                                    {{--<label for="template-contactform-name">Name <small>*</small></label>--}}
                                                    {{--<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />--}}
                                                {{--</div>--}}

                                                {{--<div class="col_half col_last">--}}
                                                    {{--<label for="template-contactform-email">Email <small>*</small></label>--}}
                                                    {{--<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />--}}
                                                {{--</div>--}}

                                                {{--<div class="clear"></div>--}}

                                                {{--<div class="col_half">--}}
                                                    {{--<label for="template-contactform-phone">Phone</label>--}}
                                                    {{--<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />--}}
                                                {{--</div>--}}

                                                {{--<div class="col_half col_last">--}}
                                                    {{--<label for="template-contactform-service">Services</label>--}}
                                                    {{--<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">--}}
                                                        {{--<option value="">-- Select One --</option>--}}
                                                        {{--<option value="Wordpress">Wordpress</option>--}}
                                                        {{--<option value="PHP / MySQL">PHP / MySQL</option>--}}
                                                        {{--<option value="HTML5 / CSS3">HTML5 / CSS3</option>--}}
                                                        {{--<option value="Graphic Design">Graphic Design</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}

                                                {{--<div class="clear"></div>--}}

                                                {{--<div class="col_full">--}}
                                                    {{--<label for="template-contactform-subject">Subject <small>*</small></label>--}}
                                                    {{--<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />--}}
                                                {{--</div>--}}

                                                {{--<div class="col_full">--}}
                                                    {{--<label for="template-contactform-message">Message <small>*</small></label>--}}
                                                    {{--<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>--}}
                                                {{--</div>--}}

                                                {{--<div class="col_full hidden">--}}
                                                    {{--<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />--}}
                                                {{--</div>--}}

                                                {{--<div class="col_full">--}}
                                                    {{--<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>--}}
                                                {{--</div>--}}

                                            {{--</form>--}}

                                        {{--</div>--}}


                                    {{--</div>--}}
                                    {{--<div class="modal-footer">--}}
                                        {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                                    {{--</div>--}}
                                {{--</div><!-- /.modal-content -->--}}
                            {{--</div><!-- /.modal-dialog -->--}}
                        {{--</div><!-- /.modal -->--}}
                        {{--<!-- Modal Contact Form End -->--}}

                    {{--</div>--}}
                {{--</div>--}}
                <div id="section-contact" class="page-section">

                    <h2 class="bottommargin">Get in Touch.</h2>

                    <div class="row clearfix">

                        <div class="col-lg-8">

                            <div class="contact-widget">

                                <div class="contact-form-result"></div>

                                    {!! Form::open(['method' => 'POST' , 'action' => ['ContactController@sendmail'],'id' => 'form-addmusic' , 'class' => 'nobottommargin' ,'name' => 'template-contactform']) !!}

                                    <div class="form-process"></div>

                                    <div class="col_half">
                                        <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control border-form-control required" placeholder="Name" />
                                    </div>
                                    <div class="col_half col_last">
                                        <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control border-form-control" placeholder="Email Address" />
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full">
                                        <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control border-form-control" placeholder="Subject" />
                                    </div>

                                    <div class="col_full">
                                        <textarea class="required sm-form-control border-form-control" id="template-contactform-message" name="template-contactform-message" rows="7" cols="30" placeholder="Your Message"></textarea>
                                    </div>

                                    <div class="col_full">
                                        <button class="button button-black noleftmargin topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full hidden">
                                        <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                                    </div>

                                </form>

                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div style="font-size: 16px; line-height: 1.7;">
                                <abbr title="Phone Number"><strong>Phone:</strong></abbr> (+66)0924954624<br>
                                <abbr title="Email Address"><strong>Email:</strong></abbr> bkkedm@gmail.com
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </section><!-- #content end -->


@endsection