@extends('layouts.cfadmin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: auto">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Redeem
            <small></small>
            {{--<a class="btn btn-app" href="{{ url('cfadmin/music/create') }}">--}}
                {{--<i class="fa fa-plus"></i> Add--}}
            {{--</a>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
            <li class="active">Redeem</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-5">
                                <p class="lead">Confirmation Info.</p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody><tr>
                                            <th style="width:50%">Name:</th>
                                            <td>{{ $activatecode->prize->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Start - End date:</th>
                                            <td>{{ \Carbon\Carbon::parse($activatecode->prize->start)->format('d/m/Y') }} - {{ \Carbon\Carbon::parse($activatecode->prize->end)->format('d/m/Y') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Club:</th>
                                            <td>{{ (!empty($activatecode->prize->club_id)) ? $activatecode->prize->club->name : '-'}}</td>
                                        </tr>
                                        <tr>
                                            <th>Event:</th>
                                            <td>{{ (!empty($activatecode->prize->event_id)) ? $activatecode->prize->event->name : '-'}}</td>
                                        </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                {!! Form::open(['method' => 'POST' , 'action' => ['RedeemController@confirm', $activatecode->id] , 'style' => 'display:inline', 'id' => 'redeem-form']) !!}
                                <div class="form-group">
                                    <label>Redeem Code:</label>
                                    <input type="text" name="redeem_code" class="form-control" data-inputmask="'mask': ['aa-****']" data-mask="" value="{{ $activatecode->code }}" disabled="disabled">
                                </div>
                                <button class="btn btn-success">Confirm</button>
                                {!! Form::close() !!}
                                <a href="{{ url('cfadmin/redeem') }}"><button class="btn btn-danger pull-right">Cancel</button></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="delete-modal">
    <div class="modal modal-danger" id="confirm-delete-post-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Danger Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-outline pull-left" id="confirm-delete">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.delete-modal -->

<div class="active-modal">
    <div class="modal modal-success" id="confirm-active-post-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Active Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-outline pull-left" id="confirm-active">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.delete-modal -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        $(function () {

            // $('[data-mask]').inputmask()
        });
    </script>

@endsection
