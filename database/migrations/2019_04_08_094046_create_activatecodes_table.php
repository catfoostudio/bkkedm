<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivatecodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activatecodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->tinyInteger('type');
            $table->integer('prize_id')->unsigned()->nullable();
            $table->foreign('prize_id')->references('id')->on('prizes');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activatecodes');
    }
}
