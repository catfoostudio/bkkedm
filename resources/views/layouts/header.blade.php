<style>
    #primary-menu ul li a:hover{
        text-decoration: underline !important;
    }
</style>
<header id="header" class="full-header dark" style="height: 80px;">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <!-- Logo
            ============================================= -->
            <div id="logo" style="height: 80px;" >
                <a href="{{ url('index') }}" class="standard-logo" data-dark-logo="{{ asset('images/logo/logo.png') }}"><img src="{{ asset('images/logo/logo.png') }}" alt="Canvas Logo"></a>
                <a href="{{ url('index') }}" class="retina-logo" data-dark-logo="{{ asset('images/logo/logo.png') }}"><img style="height: 60px;margin-top: 10px;" src="{{ asset('images/logo/logo.png') }}" alt="Canvas Logo"></a>
            </div><!-- #logo end -->
            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">
                <ul>
                    <li ><a href="{{ url('music') }}" ><div>Music</div></a>
                    </li>
                    <li><a href="{{ url('event') }}"><div>Event</div></a>
                    </li>
                    <li><a href="{{ url('lifestyle') }}"><div>Lifestyle</div></a>
                    </li>
                    {{--<li >--}}
                        {{--<a href="{{ url('club') }}"><div>Club</div></a>--}}
                    {{--</li>--}}
                    <li ><a href="{{ url('video') }}"><div>Videos</div></a>
                    </li>
                    {{--@if(Auth::check() && Auth::user()->hasRole(['admin','super-admin']))--}}
                    @if(Auth::check())
                    <li><a href="#"><div>Reward</div></a>
                        <ul>
                            <li><a href="{{ url('reward') }}"><div>REWARD</div></a></li>
                            <li><a href="{{ url('contest') }}"><div>CONTEST</div></a></li>
                            <li><a href="{{ url('vote') }}"><div>VOTE</div></a></li>
                        </ul>
                    </li>
                    @endif
                    <li><form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        @guest
                        <a href="{{ url('signin') }}">
                            <div>LOGIN</div>
                        </a>
                        @endguest
                        @auth
                        <a href="{{ url('member') }}" >
                            <div>{{ Auth::user()->name }}</div>
                        </a>
                            @if(Auth::check() && Auth::user()->hasRole(['admin','super-admin','club','event','writer','moderator']))
                                <ul>
                                    <li><a href="{{ url('cfadmin') }}"><div>CFADMIN</div></a></li>
                                    <li><a href="{{ url('logout') }}"><div>LOGOUT</div></a></li>
                                </ul>
                            @else
                            <ul>
                                <li><a href="{{ url('logout') }}"><div>LOGOUT</div></a></li>
                            </ul>
                            @endif
                        @endauth
                    </li>
                </ul>
                {{--<div id="top-cart" style="margin-right: 20px;">--}}
                    {{--<a href="{{ url('signin') }}"><h5>LOGIN</h5></a>--}}
                {{--</div><!-- #top-cart end -->--}}
                <!-- Top Search
                ============================================= -->
                <div id="top-search">
                    <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                    {!! Form::open(['method' => 'POST' , 'action' => ['SearchController@search']]) !!}
                        <input type="text" name="key" class="form-control" value="" placeholder="Type &amp; Hit Enter.." style="padding-top: 0px;margin-top: -10px !important;">
                    {!! Form::close() !!}
                </div><!-- #top-search end -->
            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->
