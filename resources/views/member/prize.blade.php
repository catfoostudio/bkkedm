@extends('layouts.main')
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="row clearfix">

                    <div class="col-md-9">

                        {{--<img src="images/icons/avatar.jpg" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}

                        <div class="heading-block noborder">
                            <h3>Registered Reward</h3>
                            {{--<span>Registered Reward</span>--}}
                            {{--<span>Messages <a href="#" class="button button-3d button-rounded button-black"><i class="icon-mail"></i>Compose</a></span>--}}
                        </div>
                        <div class="clear"></div>
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <div class="tabs tabs-alt clearfix" id="tabs-profile">
                                    {{--<ul class="tab-nav clearfix">--}}
                                        {{--<li><a href="#tab-feeds"><i class="icon-rss2"></i> All</a></li>--}}
                                        {{--<li><a href="#tab-posts"><i class="icon-pencil2"></i> Unread</a></li>--}}
                                        {{--<li><a href="#tab-replies"><i class="icon-reply"></i> Read</a></li>--}}
                                    {{--</ul>--}}
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="tab-feeds">
                                            {{--<p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium harum ea quo! Nulla fugiat earum, sed corporis amet iste non, id facilis dolorum, suscipit, deleniti ea. Nobis, temporibus magnam doloribus. Reprehenderit necessitatibus esse dolor tempora ea unde, itaque odit. Quos.</p>--}}
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Reward end</th>
                                                    <th>Title</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($prizes as $prize)
                                                    <tr>
                                                        <td style="width:30%;">
                                                            {{ \Carbon\Carbon::parse($prize->end)->format('M d, Y') }}
                                                        </td>
                                                        <td><a href="{{ url('reward/'.$prize->id) }}">{{ $prize->name }}</a></td>
                                                        <td style="width:20%;">{!! \App\Models\Hunter::getBadgeByStatus($prize->pivot->status) !!} {!! (!empty($prize->pivot->activatecode_id)) ? \App\Models\Activatecode::find($prize->pivot->activatecode_id)->statusFrontendBadge : '' !!}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    @include('member.sidebar')

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')

@endsection
