@foreach($ajaxnews as $news)
    <div class="col-lg-4 col-md-6 col-xs-6 col-sm-6 col-6">
        <div class="entry clearfix">
            <div class="entry-image" style="margin-bottom:10px;">
                <a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}"><img class="image_fade"
                                                                                                      src="{{ asset('images/posts/'.$news->thumbnail_image) }}"></a>
            </div>
            <div class="entry-title" style="margin-bottom: 10px;">
                <h2 style="height: auto;font-size: 18px;"><a href="{{ url('lifestyle/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}">{{ $news->name }}</a></h2>
            </div>
        </div>
    </div>
@endforeach