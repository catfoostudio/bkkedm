@extends('layouts.main')

@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title">

        <div class="container clearfix">
            <h1>My Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="accordion accordion-lg divcenter nobottommargin clearfix" data-active="{{($errors->any() && !$errors->has('email')) ? 2 : 1}}" style="max-width: 550px;">
                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Login to your Account</div>
                    <div class="acc_content clearfix">
                        {!! Form::open(array('method'=>'post','action' => 'Auth\LoginController@login','id' => 'login-form', 'name'=> 'login-form', 'class'=>'nobottommargin' )); !!}
                            @if($errors->has('email')) <strong style="color:red;">อีเมล หรือรหัสผ่านไม่ถูกต้อง <br>ถ้าท่านยังไม่ได้ยืนยันตัวผ่านอีเมล กรุณายืนยันตัวก่อนเข้าสู่ระบบ</strong> @endif
                            @csrf
                            <div class="col_full">
                                <label for="login-form-username">Email:</label>
                                <input type="text" id="login-form-username" name="email" value="" class="form-control" />
                            </div>

                            <div class="col_full">
                                <label for="login-form-password">Password:</label>
                                <input type="password" id="login-form-password" name="password" value="" class="form-control" />
                            </div>

                            <div class="col_full nobottommargin">
                                <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button><label style="margin-left:10px;">or</label>
                                <a href="{{ url('auth/facebook') }}" class="button button-rounded si-facebook si-colored">Continue with Facebook</a>
                                {{--<a href="#" class="fright">Forgot Password?</a>--}}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="acctitle"><i class="acc-closed icon-user4"></i><i class="acc-open icon-ok-sign"></i>New Signup? Register for an Account</div>
                    <div class="acc_content clearfix" >
                            {!! Form::open(array('method'=>'post','action' => 'Auth\RegisterController@register','id' => 'register-form', 'name'=> 'register-form', 'class'=>'nobottommargin' )); !!}
                            @csrf
                            <div class="col_full">
                                <label for="register-form-email">Email Address:</label>
                                <input type="text" id="register-form-email" name="email_register" value="{{ old('email_register') }}" class="form-control {{$errors->has('email_register') ? 'error' : '' }}" />
                                <small id="emailHelp" class="form-text text-muted">กรุณาใช้อีเมลจริง เพื่อการติดต่อกลับ</small>
                                @if($errors->has('email_register')) <strong style="color:red;">กรุณาตรวจสอบอีเมลอีกครั้ง อีเมลไม่ถูกต้อง หรือถูกใช้งานแล้ว</strong> @endif
                            </div>
                            <div class="col_full">
                                <label for="register-form-password">Choose Password:</label>
                                <input type="password" id="register-form-password" name="password_register" value="" class="form-control {{$errors->has('password_register') ? 'error' : '' }}" />
                                @if($errors->has('password_register')) <strong style="color:red;">รหัสผ่านขั้นต่ำ 8ตัวอักษร และกรุณาพิมยืนยันรหัสผ่านอีกครั้ง</strong> @endif
                            </div>

                            <div class="col_full">
                                <label for="register-form-repassword">Re-enter Password:</label>
                                <input type="password" id="register-form-repassword" name="password_confirmation" value="" class="form-control {{$errors->has('password_confirmation') ? 'error' : '' }}" />
                            </div>
                            <div class="col_full">
                                <label for="register-form-firstname">First name:</label>
                                <input type="text" id="register-form-firstname" name="firstname" value="{{ old('firstname') }}" class="form-control {{$errors->has('firstname') ? 'error' : '' }}" />
                                @if($errors->has('firstname')) <strong style="color:red;">กรุณากรอกชื่อ หรือชื่อกับนามสกุลของท่านได้ใช้ถูกไปแล้ว</strong> @endif
                            </div>
                            <div class="col_full">
                                <label for="register-form-lastname">Last name:</label>
                                <input type="text" id="register-form-lastname" name="lastname" value="{{ old('lastname') }}" class="form-control {{$errors->has('lastname') ? 'error' : '' }}" />
                                @if($errors->has('lastname')) <strong style="color:red;">กรุณากรอกนามสกุล</strong> @endif
                            </div>
                            <div class="col_full">
                                <div class="form-group">
                                    <label for="register-form-birthday">Birthday (Day/Month/Year):</label>
{{--                                    {!! Form::select('year_of_birth', \Carbon\Carbon::now()->subYears(100)->year, \Carbon\Carbon::now()->year , \Carbon\Carbon::now()->year ,array('class' => 'form-control mnth')) !!}--}}
                                    <input autocomplete="off" class="form-control mnth {{$errors->has('birthday') ? 'error' : '' }}" id="register-form-birthday" type="text" name="birthday" value="{{ old('birthday') }}"   >
                                    @if($errors->has('birthday')) <strong style="color:red;">กรุณาเลือกวันเกิดของคุณ</strong> @endif
                                </div>
                            </div>
                            <div class="col_full">
                                <label for="register-form-gender">Gender:</label>
                                <div class="form-check">
                                    <input class="form-check-input" id="gridRadios1" type="radio" name="gender"  value="2" >
                                    <label class="form-check-label" for="gridRadios1">
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="gridRadios2" type="radio" name="gender"  value="3" >
                                    <label class="form-check-label" for="gridRadios2">
                                        Female
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="gridRadios3" type="radio" name="gender"  value="1" checked="checked">
                                    <label class="form-check-label" for="gridRadios3">
                                        Unspecified
                                    </label>
                                </div>
                            </div>
                            <div class="col_full">
                                <label for="register-form-mobile">Mobile phone number:</label>
                                <input type="text" name="mobile" id="register-form-mobile" value="{{ old('mobile') }}" class="form-control {{$errors->has('mobile') ? 'error' : '' }}" />
                                <small id="mobileHelp" class="form-text text-muted">กรุณาใช้เบอร์โทรจริง เพื่อการติดต่อกลับ</small>
                                @if($errors->has('mobile')) <strong style="color:red;">กรุณากรอกเบอร์โทร 10หลัก หรือเบอร์โทรนี้ได้ถูกใช้ไปแล้ว</strong> @endif
                            </div>
                            <div class="col_full nobottommargin">
                                <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->

@endsection

@section('script')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<script>
    $(function () {
        $('.mnth').datepicker({
            autoclose: true,
            // viewMode: "years",
            // minViewMode: "years",
             format: "dd/mm/yyyy",
            yearRange: "-100:+0",
        });

        $('.accordion').find('.acctitle:eq('+ Number(accordionActive) +')').addClass('acctitlec').next().show();
    });
</script>
@endsection