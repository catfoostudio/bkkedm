<?php

namespace App\Http\Controllers;

use App\Models\Contest;
use App\Models\Contestant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($this->isAdminRequest()) {
            $view = 'contest.cfadmin.index';

            $contests = Contest::orderBy('created_at', 'desc')->get();

            return view($view, [
                'contests' => $contests,
            ]);
        } else {

            $view = 'contest.index';
            $contests = Contest::where('status', 1)->whereDate('end', '>=', Carbon::now())->orderBy('end')->paginate(4);
            $pastcontests = Contest::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->paginate(4);

            if ($request->ajax()) {
                $view = view('contest.loadcontests', compact('contests'))->render();
                return response()->json(['html' => $view]);
            }

            return view($view, [
                'contests' => $contests,
                'pastcontests' => $pastcontests,
            ]);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contest.cfadmin.create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

//        dd();
        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'reward' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after:start'
        ];

        $this->validate($request, $validate);

//        DB::beginTransaction();
//        try {
        $contest = Contest::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'thumbnail_image' => $input['filepath'],
            'original_image' => $input['filepath'],
            'user_id' => Auth::user()->id,
            'status' => $input['status'],
            'reward' => $input['reward'],
            'remark' => $input['remark'],
            'views' => 0,
            'quota' => $input['quota'],
            'start' => Carbon::parse($input['start'])->format('Y-m-d'),
            'end' => Carbon::parse($input['end'])->format('Y-m-d'),
            'link_req' => (!empty($input['link_req'])) ? true : false,
            'file_req' => (!empty($input['file_req'])) ? true : false,
            'content_req' => (!empty($input['content_req'])) ? true : false,
            'link_name' => (!empty($input['link_name'])) ? $input['link_name'] : 'Link',
            'file_name' => (!empty($input['file_name'])) ? $input['file_name'] : 'File',
            'content_name' => (!empty($input['content_name'])) ? $input['content_name'] : 'Detail',
        ]);

        if(!empty($input['tags'])){
            $contest->tags = explode(',', $input['tags']);
        }

        $time = time();
        $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
        $img->fit(842, 1191);
        $filename = $time . '_contest_' . $contest->id . '.jpg';
        $filepath = public_path('images/contests') . '/' . $filename;
        $img->save($filepath);

        $contest->thumbnail_image = $filename;

        if ($contest->status == 1) {
            $contest->published_at = Carbon::now();
        }

        $contest->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/contest')->with('alert-success', 'Contest #'.$contest->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //
        $contest = Contest::find($id);

        if (empty($contest))
            return abort(404);

        if ($this->isAdminRequest()) {
            $view = 'contest.cfadmin.detail';

            return view($view, [
                'contest' => $contest,
            ]);
        } else {
            $view = 'contest.detail';

            if($contest->status == 0){
                return abort(404);
            }

            return view($view, [
                'contest' => $contest,
                'contested' => (!empty(count(Contestant::where('contest_id',$id)->get()))) ,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'reward' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after:start'
        ];

        $this->validate($request, $validate);

//        DB::beginTransaction();
//        try {

        $contest = Contest::find($id);

        $contest->name = $input['name'];
        $contest->content = $input['content'];


        if(!empty($input['tags'])){
            $contest->syncTags(explode(',',$input['tags']));
        }
        else{
            $contest->syncTags([]);
        }

        $contest->status = $input['status'];
        $contest->remark = $input['remark'];
        $contest->quota = $input['quota'];
        $contest->reward = $input['reward'];
        $contest->start = Carbon::parse($input['start'])->format('Y-m-d');
        $contest->end = Carbon::parse($input['end'])->format('Y-m-d');

        $contest->link_req = (!empty($input['link_req'])) ? true : false;
        $contest->file_req = (!empty($input['file_req'])) ? true : false;
        $contest->content_req = (!empty($input['content_req'])) ? true : false;
        $contest->link_name = (!empty($input['link_name'])) ? $input['link_name'] : 'Link';
        $contest->file_name = (!empty($input['file_name'])) ? $input['file_name'] : 'File';
        $contest->content_name = (!empty($input['content_name'])) ? $input['content_name'] : 'Detail';

        if($input['filepath'] != $contest->original_image) {
            $time = time();
            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
            $img->fit(842, 1191);
            $filename = $time . '_vote_' . $contest->id . '.jpg';
            $filepath = public_path('images/contests') . '/' . $filename;
            $img->save($filepath);

            $contest->thumbnail_image = $filename;
            $contest->original_image = $input['filepath'];
        }

        if ($contest->status == 1) {
            $contest->published_at = Carbon::now();
        }

        $contest->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/contest')->with('alert-success', 'Contest #'.$contest->id.' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contest = Contest::find($id);
        $contest->status = 0;
        $contest->save();
        return redirect('cfadmin/contest')->with('alert-danger', 'Contest #'.$contest->id.' has been inactivated!');
    }

    public function active($id)
    {
        $contest = Contest::find($id);
        $contest->status = 1;
        if ($contest->status == 1) {
            $contest->published_at = Carbon::now();
        }
        $contest->save();
        return redirect('cfadmin/contest')->with('alert-success', 'Contest #'.$contest->id.' has been activated!');
    }

    public function register(Request $request, $id)
    {
        $validate = [
            'g-recaptcha-response' => 'required|captcha',
        ];

        $contest = Contest::find($id);

        if($contest->link_req){
            $validate['link'] = 'required';
        }

        if($contest->file_req){
            $validate['file'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4072';
        }

        if($contest->content_req){
            $validate['content'] = 'required';
        }

        $validator = Validator::make($request->all(), $validate);

        $validator->validate();
//


        if ($validator->fails()) {
            dd();
        }
        else{
            $contest = Contest::find($id);
            $input = $request->input();
            $filename = '';
            if(Input::file('file')){
                $time = time();
                $filename = $time . '_contestant_' . $contest->id . '_'.Auth::user()->id.'.jpg';
                $filepath = public_path('contestants') . '/' . $filename;
                Image::make(Input::file('file'))->save($filepath);
            }
            $contest->users()->attach(Auth::user()->id,['status' => 2,'link' => $input['link'],'file' => $filename,'content' => $input['content']]);
            $contest->save();
        }

        return redirect('contest/'.$id);


    }

    public function user(Request $request, $id)
    {
        $contest = Contest::find($id);

        $contestants = Contestant::where('contest_id',$id)->get();

        return view('contest.cfadmin.registered',[
            'contest' => $contest,
            'contestants' => $contestants
        ]);
    }

    public function decline(Request $request,$id)
    {
        $contestant = Contestant::find($id);
        $contestant->status = 0;
        $contestant->save();

//        $contestants = Contestant::where('contest_id',$contestant->contest->id)->get();

        return redirect('cfadmin/contestuser/'.$contestant->contest->id);
//        return view('contest.cfadmin.registered',[
//            'contest' => $contestant->contest,
//            'contestants' => $contestants
//        ]);
    }

    public function approve($id)
    {
        $contestant = Contestant::find($id);
        $contestant->status = 1;
        $contestant->save();

        return redirect('cfadmin/contestuser/'.$contestant->contest->id);

//        $contestants = Contestant::where('contest_id',$contestant->contest->id)->get();
//
//        return view('contest.cfadmin.registered',[
//            'contest' => $contestant->contest,
//            'contestants' => $contestants
//        ]);
    }

    public function reset($id)
    {
        $contestant = Contestant::find($id);
        $contestant->status = 2;
        $contestant->save();

        return redirect('cfadmin/contestuser/'.$contestant->contest->id);

    }
}
