@extends('layouts.main')
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="row clearfix">

                    <div class="col-md-9">

                        {{--                        <img src="{{ asset('images/icons/avatar.jpg') }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}

                        <div class="heading-block noborder" style="margin-bottom: 0px;">
                            <h3>New Message</h3>
                            {{--                            <span style="font-size: 15px;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y') }}</span>--}}
                        </div>
                        {{--<div class="clear"></div>--}}
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <!-- Comments
						============================================= -->
                                <div id="comments" class="clearfix" style="margin-top: 5px;padding-top: 30px;">

                                    <!-- Comment Form
                                    ============================================= -->
                                    <div id="respond" class="clearfix">

                                        <form class="clearfix" action="#" method="post" id="commentform">

                                            <div class="col_one_third">
                                                <label for="author">Name</label>
                                                <input type="text" name="author" id="author" value="" size="22" tabindex="1" class="sm-form-control" />
                                            </div>

                                            <div class="col_one_third">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" id="email" value="" size="22" tabindex="2" class="sm-form-control" />
                                            </div>

                                            <div class="col_one_third col_last">
                                                <label for="url">Website</label>
                                                <input type="text" name="url" id="url" value="" size="22" tabindex="3" class="sm-form-control" />
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col_full">
                                                <label for="comment">Comment</label>
                                                <textarea name="comment" cols="58" rows="7" tabindex="4" class="sm-form-control"></textarea>
                                            </div>

                                            <div class="col_full nobottommargin">
                                                <button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d nomargin">Submit Comment</button>
                                            </div>

                                        </form>

                                    </div><!-- #respond end -->

                                </div><!-- #comments end -->
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    <div class="col-md-3 clearfix">

                        <div class="list-group">
                            {{--<a href="#" class="list-group-item list-group-item-action clearfix">Profile <i class="icon-user float-right"></i></a>--}}
                            {{--<a href="#" class="list-group-item list-group-item-action clearfix">Servers <i class="icon-laptop2 float-right"></i></a>--}}
                            <a href="#" class="list-group-item list-group-item-action clearfix active" style="color: #292929;background-color: #f2f2f2;border: 1px solid rgba(0, 0, 0, 0.125);">Messages <i class="icon-envelope2 float-right"></i></a>
                            <a href="#" class="list-group-item list-group-item-action clearfix">Lotto <i class="icon-credit-cards float-right"></i></a>
                            <a href="#" class="list-group-item list-group-item-action clearfix">Prize <i class="icon-credit-cards float-right"></i></a>
                            <a href="#" class="list-group-item list-group-item-action clearfix">Settings <i class="icon-cog float-right"></i></a>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="list-group-item list-group-item-action clearfix">Logout <i class="icon-line2-logout float-right"></i></a>
                        </div>

                        <div class="fancy-title topmargin title-border">
                            <h4>About Me</h4>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum laboriosam, dignissimos veniam obcaecati. Quasi eaque, odio assumenda porro explicabo laborum!</p>

                        <div class="fancy-title topmargin title-border">
                            <h4>Social Profiles</h4>
                        </div>

                        <a href="#" class="social-icon si-facebook si-small si-rounded si-light" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-gplus si-small si-rounded si-light" title="Google+">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon si-dribbble si-small si-rounded si-light" title="Dribbble">
                            <i class="icon-dribbble"></i>
                            <i class="icon-dribbble"></i>
                        </a>

                        <a href="#" class="social-icon si-flickr si-small si-rounded si-light" title="Flickr">
                            <i class="icon-flickr"></i>
                            <i class="icon-flickr"></i>
                        </a>

                        <a href="#" class="social-icon si-linkedin si-small si-rounded si-light" title="LinkedIn">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>

                        <a href="#" class="social-icon si-twitter si-small si-rounded si-light" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')

@endsection
