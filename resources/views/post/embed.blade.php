<div class="row">
    {{--<div class="col-lg-6">--}}
    {{--<h4 style="margin-bottom: 5px;">Description</h4>--}}
    {{--<div class="line" style="margin:5px 0;"></div>--}}
    {{--{!! $prize->content !!}--}}
    {{--</div>--}}
    <div class="col-lg-12">
        @if(!empty($post->vote_id) && $vote->status == 1)
            <div class="fancy-title title-border" style="margin-bottom: 10px;">
                <h3>Vote</h3><br>
                @if($errors->has('checkbox'))<strong
                        style="color:red;">กรุณาเลือกอย่างน้อยหนึ่งตัวเลือก</strong>
                @elseif($errors->has('inputbox.*'))<strong
                        style="color:red;">กรุณากรอกตัวเลือกให้ครบทุกช่อง และห้ามซ้ำ</strong>
                @endif
            </div>
            <h4 style="margin-bottom: 5px;">VOTE - {{ $vote->name }}</h4>
            <div class="line" style="margin:5px 0;"></div>

            {!! Form::open(['method' => 'POST' , 'action' => ['VoteController@choose', $vote],'id'=>'vote-form']) !!}
            {!! Form::hidden('vote', $vote->id) !!}
            <?php $checked = true;?>
        <!-- VOTE TEXTLIST -->
            @if($vote->type == 1)
                @if($isBeforeVote)
                    <h4>Please waiting until start date</h4>
                @elseif($isPastVote)
                    <h4>Voting has closed</h4>
                @elseif(Auth::check() && !$voted)
                    <div class="row">
                        @foreach($vote->choices as $choice)
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                                <div class="col-12">
                                    <div class="form-check" style="margin-bottom: 10px">
                                        <input class="form-check-input" type="radio" value="{{$choice->id}}"
                                               name="textlist"
                                               id="choice_{{$choice->id}}" {{ ($checked) ? 'checked=checked' : '' }}>
                                        <label class="form-check-label" for="choice_{{$choice->id}}">
                                            {{$choice->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php $checked = false;?>
                        @endforeach
                    </div>
                    <div class="clear"></div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                        </div>
                    </div>
                @elseif(Auth::check() && $voted)
                    <div class="row">
                        @foreach($vote->choices as $choice)
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12">
                                <div class="col-12">
                                    <div class="form-check" style="margin-bottom: 10px">
                                        <input class="form-check-input" type="radio" value="{{$choice->id}}"
                                               name="textlist"
                                               id="choice_{{$choice->id}}" {{ ($userChooses->first()->id == $choice->id) ? 'checked=checked' : '' }}
                                               disabled="disabled" >
                                        <label class="form-check-label" for="choice_{{$choice->id}}">
                                            {{$choice->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <? $checked = false;?>
                        @endforeach
                    </div>
                    <div class="clear"></div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                        </div>
                    </div>
                @endif
            <!-- VOTE CHECKBOX -->
            @elseif($vote->type == 2)
                @if($isBeforeVote)
                    <h4>Please waiting until start date</h4>
                @elseif($isPastVote)
                    <h4>Voting has closed</h4>
                @elseif(Auth::check() && !$voted)
                    <div class="row">
                        @foreach($vote->choices as $choice)

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 10px;">

                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="{{$choice->id}}"
                                               name="checkbox[]" id="choice_{{$choice->id}}">
                                        <label class="form-check-label" for="choice_{{$choice->id}}">
                                            {{$choice->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="clear"></div>
                    <div class="row" style="margin-top: 30px">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                        </div>
                    </div>
                @elseif(Auth::check() && $voted)
                    <div class="row">
                        @foreach($vote->choices as $choice)

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 10px;">

                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="{{$choice->id}}"
                                               name="checkbox[]" id="choice_{{$choice->id}}" disabled="disabled" {{ (in_array($choice->id,$userChooses->pluck('id')->toArray())) ? 'checked=checked' : '' }}>
                                        <label class="form-check-label" for="choice_{{$choice->id}}">
                                            {{$choice->name}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="clear"></div>
                    <div class="row" style="margin-top: 30px">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                        </div>
                    </div>
                @endif
            <!-- VOTE THUMBNAIL WITH TEXT -->
            @elseif($vote->type == 3)
                @if($isBeforeVote)
                    <h4>Please waiting until start date</h4>
                @elseif($isPastVote)
                    <h4>Voting has closed</h4>
                @elseif(Auth::check() && !$voted)
                    <div class="row">
                        {!! Form::hidden('choice',0) !!}
                        @foreach($vote->choices as $choice)
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 30px;">
                                <div class="col-12" style="text-align: center">
                                    <img src="{{ asset($choice->content)}}"
                                         style="width: 100%;height: 300px;object-fit: cover;{{ (!Auth::check() || $voted || $isPastVote) ? 'pointer-events:none;opacity:0.3;' : '' }}"/><br>
                                    <button data-choice="{{$choice->id}}" class="btn btn-dark choice-item"
                                            style="margin-top:5px;width: 100%;{{ (!Auth::check() || $voted || $isPastVote) ? 'pointer-events:none;opacity:0.3;' : '' }}">{{$choice->name}}</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @elseif(Auth::check() && $voted)
                    <div class="row">
                        {!! Form::hidden('choice',0) !!}
                        @foreach($vote->choices as $choice)
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-12" style="margin-bottom: 30px;">
                                <div class="col-12" style="text-align: center">
                                    <img src="{{ asset($choice->content)}}"
                                         style="width: 100%;height: 300px;object-fit: cover; {{ ($userChooses->first()->id != $choice->id) ? 'pointer-events:none;opacity:0.3;' : '' }}"/><br>
                                    <button data-choice="{{$choice->id}}" class="btn btn-dark choice-item"
                                            style="margin-top:5px;width: 100%;{{ ($userChooses->first()->id != $choice->id) ? 'pointer-events:none;opacity:0.3;' : '' }}" disabled="disabled">{{$choice->name}}</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            <!-- VOTE INPUTBOX -->
            @elseif($vote->type == 4)
                @if($isBeforeVote)
                    <h4>Please waiting until start date</h4>
                @elseif($isPastVote)
                    <h4>Voting has closed</h4>
                @elseif(Auth::check() && !$voted)
                    @for($i=0;$i<$vote->input_amount;$i++)
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-lg-4">
                                <input type="text" name="inputbox[]"
                                       class="form-control {{$errors->has('inputbox.'.$i) ? 'error' : '' }}"
                                       value="{{ old('inputbox.'.$i) }}" >
                            </div>
                        </div>
                        <div class="clear"></div>
                    @endfor
                    <div class="row">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit">VOTE</button>
                        </div>
                    </div>
                @elseif(Auth::check() && $voted)
                    @for($i=0;$i<count($userChooses);$i++)
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-lg-4">
                                <input type="text" name="inputbox[]"
                                       class="form-control {{$errors->has('inputbox.'.$i) ? 'error' : '' }}"
                                       value="{{ old('inputbox.'.$i,$userChooses[$i]->name) }}" disabled="disabled" >
                            </div>
                        </div>
                        <div class="clear"></div>
                    @endfor
                    <div class="row">
                        <div class="col-3">
                            <button class="btn btn-dark choice-item" type="submit" disabled="disabled">VOTED</button>
                        </div>
                    </div>
                @endif
            @endif
            {!! Form::close() !!}
        @elseif(!empty($post->prize_id) && $prize->status == 1)
            <h4 style="margin-bottom: 5px;">REWARD - {{ $prize->name }}</h4>
            <div class="line" style="margin:5px 0;"></div>
            @if($isBeforePrize)
                <button class="btn btn-dark btn-block btn-lg disabled">Please waiting until start date</button>
            @elseif($isPastPrize)
                <button class="btn btn-dark btn-block btn-lg disabled">Expired</button>
            @elseif(Auth::check())
                @if(empty($hunter))
                    {!! Form::open(['method' => 'POST' , 'action' => ['PrizeController@register' , $post->prize],'id'=>'my-form-id', 'enctype' => 'multipart/form-data']) !!}
                    Image: @if($errors->has('file'))
                        <span class="invalid-feedback" role="alert" style="display: inline;">
                                        <strong> {{ $errors->first('file') }}</strong>
                                    </span>
                    @endif
                    {!! Form::file('file', [ 'style' => 'margin-bottom:5px;','class'=>'form-control'.($errors->has('file') ? ' error' : '' ), 'accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                    Link: @if($errors->has('link'))
                        <span class="invalid-feedback" role="alert" style="display: inline;">
                                        <strong> {{ $errors->first('link') }}</strong>
                                    </span>
                    @endif
                    <input type="text" name="link" class="form-control{{($errors->has('link') ? ' error' : '' )}}">
                    {!! NoCaptcha::displaySubmit('my-form-id', 'Register', ['class' => 'btn btn-dark btn-block btn-lg', 'style' => 'margin-top:10px;']) !!}
                    {!! Form::close() !!}
                @else
                    <button class="btn btn-dark btn-block btn-lg disabled">Registered</button>
                @endif
            @else
                <a href="{{ route('signin') }}"><button class="btn btn-dark btn-block btn-lg">Sign In to register</button></a>
            @endif
        @elseif(!empty($post->contest_id) && $contest->status == 1)
            <h3>CONTEST - {{ $contest->name }}</h3>
            @if(Auth::check())
                {!! Form::open(['method' => 'POST' , 'action' => ['ContestController@register', $contest->id],'id' => 'form-contest', 'enctype' => 'multipart/form-data']) !!}
            @endif
            <div class="fancy-title title-border" style="margin-bottom: 0px;">
                <h3>Register</h3>
            </div>
            <div>
                @if(!Auth::check())<div style="position: absolute;opacity:1;color: black; top: 330px;left:35%;font-weight: bold;font-size: 20px;z-index: 1;">LOGIN to Vote</div>
                @elseif($contested)<div style="position: absolute;opacity:1;color: black; top: 330px;left:35%;font-weight: bold;font-size: 25px;z-index: 1;"></div>@endif
                <div style="{{ (!Auth::check() || $contested) ? 'pointer-events:none;opacity:0.3;' : '' }}">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-6">
                            {{ $contest->link_name }}: {!! Form::text('link' , '', ['class'=>'form-control'.($errors->has('link') ? ' error' : '')]) !!}
                        </div>
                        <div class="col-lg-6">
                            {{ $contest->file_name }}: <span style="font-size: 14px;">(รูปขนาดไม่เกิน 4MB)</span> {!! Form::file('file', ['class'=>'form-control'.($errors->has('file') ? ' error' : '')]) !!}
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-12">
                            {{ $contest->content_name }}: {!! Form::textarea('content' , '', ['class'=>'form-control '.(($errors->has('content')) ? 'error' : ''), 'rows'=>5]) !!}
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-12">
                            {!! NoCaptcha::displaySubmit('form-contest', ($contested) ? 'Registered' : 'Register', ['class' => 'btn btn-dark btn-block btn-lg']) !!}
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::check())
                {!! Form::close() !!}
            @endif
        @endif
    </div>
</div>