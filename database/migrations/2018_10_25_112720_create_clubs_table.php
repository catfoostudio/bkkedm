<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->text('content')->nullable();
            $table->string('thumbnail_image');
            $table->string('original_image');
            $table->tinyInteger('status')->default(1);
            $table->text('quote')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('line_id')->nullable();
            $table->string('tel')->nullable();
            $table->string('mobile')->nullable();
            $table->text('remark')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->string('type')->nullable();
            $table->string('logo_image');
            $table->string('logo_original_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
