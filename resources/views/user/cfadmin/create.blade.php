@extends('.layouts.cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add User
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/user') }}">Users</a></li>
                <li class="active">Add User</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            {{--@if (count($errors) > 0)--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>ขออภัย!</strong><br><br>--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">User Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'POST' , 'action' => ['UserController@store'],'id' => 'form-adduser']) !!}
                        <div class="box-body form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{($errors->has('firstname') ? ' has-error' : '' )}}">
                                    <label for="inputEmail3">Firstname <span style="color: red">*required</span></label>
                                    {!! Form::text('firstname',old('firstname'),array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{($errors->has('lastname') ? ' has-error' : '' )}}">
                                    <label for="inputEmail3">Lastname <span style="color: red">*required</span></label>
                                    {!! Form::text('lastname',old('lastname'),array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('email_register') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Email <span style="color: red">*required</span></label>
                                        {!! Form::text('email_register',old('email_register'),array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Role</label>
                                        <select class="form-control" name="role" id="role">
                                            <option value="admin" {{ (old('role') == 'admin') ? 'selected=selected' : ''}}>Admin</option>
                                            <option value="moderator" {{ (old('role') == 'moderator') ? 'selected=selected' : ''}}>Moderator</option>
                                            <option value="writer" {{ (old('role') == 'writer') ? 'selected=selected' : ''}}>Writer</option>
                                            <option value="club" {{ (old('role') == 'club') ? 'selected=selected' : ''}}>Club</option>
                                            <option value="event" {{ (old('role') == 'event') ? 'selected=selected' : ''}}>Event</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6" style="display: none" id="club_area">
                                        <label for="inputEmail3">Club</label>
                                        <select class="form-control select2" name="club" id="club" style="width: 100%">
                                           @foreach($clubs as $club)
                                               <option value="{{$club->id}}">{{$club->name}}</option>
                                           @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6" style="display: none" id="event_area">
                                        {{--<label for="inputEmail3">Event</label>--}}
                                        {{--<select name="event" class="form-control select2" id="event" style="width: 100%">--}}
                                            {{--@foreach($events as $event)--}}
                                                {{--<option value="{{$event->id}}">{{$event->name}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>
                            </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('password_register') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Password <span style="color: red">*required</span></label>
                                        {!! Form::password('password_register',array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('password_confirmation') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Confirm Password <span style="color: red">*required</span></label>
                                        {!! Form::password('password_confirmation',array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('mobile') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Mobile <span style="color: red">*required (10digits and unique from other users) </span></label>
                                        {!! Form::text('mobile', '',array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{($errors->has('birthday') ? ' has-error' : '' )}}">
                                        <label for="inputEmail3">Birthday <span style="color: red">*required (must before today)</span></label>
                                        {!! Form::text('birthday','',array('class' => 'form-control', 'id' => 'birthday', 'autocomplete' => 'off')) !!}
                                        </div>
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Quote</label>
                                        <textarea class="form-control" name="quote" >{{ old('quote') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input id="status" name="status" type="hidden" value="1">
                            <button type="submit" class="btn btn-success" id="btn-publish">Add</button>
                            <a href="{{ url('cfadmin/user') }}">
                                <button type="button" class="btn btn-default pull-right">Cancel</button>
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- page script -->
    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            // filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            //filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removeButtons: 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Flash,Anchor,Language,BidiLtr,BidiRtl,ShowBlocks',
            contentsCss : [ '{{ asset('css/bootstrap.css') }}', '{{ asset('style.css') }}' ],
            height:300,
        };

        $(document).ready(function() {

            $('.select2').select2();

            $('#birthday').datepicker({
                autoclose: true,
                zIndexOffset: 1050
            });

            $('#role').change(function () {
                if($("#role").select2().find(":selected").val() == 'club'){
                    $('#club_area').show();
                    $('#event_area').hide();
                    // $('.select2').select2();
                }
                else if($("#role").select2().find(":selected").val() == 'event'){
                    $('#club_area').hide();
                    $('#event_area').show();
                    // $('.select2').select2();
                }
                else{
                    $('#club_area').hide();
                    $('#event_area').hide();
                }
            });

            $('#btn-draft').click(function () {
                $('#status').val(2);
                $('#form-addmusic').submit();
            });

            $('#lfm').filemanager('image');

        });

    </script>
@endsection
