<?php

namespace App\Http\Controllers;

use App\Models\Gambler;
use App\Models\Hunter;
use App\Models\Lotto;
use App\Models\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->isAdminRequest()){
            $view = 'member.cfadmin.index';
            $users = User::role(['member'])->get();
            return view($view, [
                'users' => $users,
            ]);
        }
        else{

            $messages = Message::where('receipt_id', Auth::user()->id)->orderBy('created_at','DESC')->get();
            //
            return view('member.index',[
                'member' => Auth::user(),
                'messages' => $messages
            ]);
        }

    }

    public function message(Request $request, $id)
    {
        $input = $request->input();
        $message = Message::find($id);
        //
        return view('member.message',[
            'member' => Auth::user(),
            'message' => $message
        ]);
    }

    public function compose()
    {

        return view('member.compose',[
            'member' => Auth::user(),
//            'message' => $message
        ]);
    }

    public function lotto()
    {
        return view('member.lotto',[
            'member' => Auth::user(),
            'gamblers' => Auth::user()->lottos()->orderBy('end','DESC')->get()
        ]);
    }

    public function prize()
    {
//        dd(Auth::user()->prizes()->orderBy('end','DESC')->get()->first()->pivot->status));
        return view('member.prize',[
            'member' => Auth::user(),
            'prizes' => Auth::user()->prizes()->orderBy('end','DESC')->get()
        ]);
    }

    public function contest()
    {
        return view('member.contest',[
            'member' => Auth::user(),
            'gamblers' => Auth::user()->contests()->orderBy('end','DESC')->get()
        ]);
    }

    public function profile()
    {
        return view('member.profile',[
            'member' => Auth::user(),
//            'gamblers' => Auth::user()->contests()->orderBy('end','DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validate = [
            'password' => 'nullable|string|min:8',
            'password_confirmation' => 'required_with:password|same:password',
            'current_password' => 'required_with:password',
        ];

        $this->validate($request, $validate);

        $input = $request->input();
        $user = User::find($id);
        $user->firstname = $input['firstname'];
        $user->lastname = $input['lastname'];
        $user->birthday = Carbon::createFromFormat('d/m/Y',$input['birthday'])->format('Y-m-d');
        $user->gender = $input['gender'];
        $user->mobile = $input['mobile'];

        if (Auth::attempt(['email' => $user->email, 'password' => $input['current_password']])) {
            // The user is active, not suspended, and exists.
            $user->password =  Hash::make($input['password']);
        }

        $user->save();

        return redirect('member/profile')->with('message', 'Update Successful');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->status = 2;
        $user->save();
        return redirect('cfadmin/member')->with('alert-danger', 'User #'.$user->id.' has been suspended');
    }

    public function active($id)
    {
        //
        $user = User::find($id);
        $user->status = 1;
        $user->save();
        return redirect('cfadmin/member')->with('alert-success', 'User #'.$user->id.' has been activated');
    }

    public function reset($id)
    {
        $user = User::find($id);
        $user->status = 0;
        $user->save();
        return redirect('cfadmin/member')->with('alert-success', 'User #'.$user->id.' has been inactivated');
    }
}
