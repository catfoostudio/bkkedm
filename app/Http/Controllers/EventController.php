<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\Event;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $featuredevents = Event::where('status', 1)->where('featured_flag' , 1)->orderBy('featured_order')->get();
        //
        if($this->isAdminRequest()){
            $view = 'event.cfadmin.index';

            if(Auth::user()->hasRole('event')){
                $events = Event::where('user_id',Auth::user()->id)->orderBy('created_at', 'desc')->get();
            }
            else{
                $events = Event::orderBy('created_at', 'desc')->get();
            }

            return view($view, [
                'events' => $events,
                'featuredevents' => $featuredevents,
            ]);
        }
        else{
            $view = 'event.index';
            $events = Event::where('status', 1)->whereDate('eventdate', '>=' , Carbon::now())->orderBy('eventdate')->paginate(4);
            $pastevents = Event::where('status', 1)->whereDate('eventdate', '<' , Carbon::now())->orderBy('eventdate')->paginate(4);

            if ($request->ajax()) {
                $view = view('event.loadevents', compact('events'))->render();
                return response()->json(['html'=>$view]);
            }

            return view($view, [
                'events' => $events,
                'pastevents' => $pastevents,
                'featuredevents' => $featuredevents,
            ]);
        }

    }

    public function loadpastevent(Request $request)
    {
        $events = Event::where('status', 1)->whereDate('eventdate', '<' , Carbon::now())->orderBy('eventdate')->paginate(4);

        if ($request->ajax()) {

            $view = view('event.loadevents', compact('events'))->render();
            return response()->json(['html'=>$view]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('event.cfadmin.create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'poster_image' => 'required',
            'eventdate' => 'required',
            'location' => 'required',
        ];

        if(!empty($input['featured_flag'])){
            $validate['cover_image'] = 'required';
        }

        $this->validate($request, $validate);

        $event = Event::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'poster_original_image' => $input['poster_image'],
            'cover_original_image' => !empty($input['cover_image']) ? $input['cover_image'] : '',
            'poster_image' => $input['poster_image'], //ori
            'cover_image' => !empty($input['cover_image']) ? $input['cover_image'] : '',
            'location' => $input['location'],
            'price' => !empty($input['price']) ? $input['price'] : 0,
            'ticket' => $input['ticket'],
            'eventdate' => Carbon::parse($input['eventdate'])->format('Y-m-d'),
//            'reservationtime' => $input['reservationtime'],
            'club_id' => (empty($input['club_id'])) ? null : $input['club_id'],
            'featured_flag' => (empty($input['featured_flag'])) ? 0 : 1,
            'featured_order' => (empty($input['featured_flag'])) ? 0 : Event::lastOrderFeatured() + 1,
            'user_id' => Auth::user()->id,
            'status' => $input['status'],
        ]);

        if(!empty($input['tags'])){
            $event->tags = explode(',', $input['tags']);
        }

        $time = time();

        if(!empty($input['cover_image'])) {
            $img = Image::make(asset(str_replace(' ', '%20', $input['cover_image'])));
            $img->fit(1140, 400);
            $filename = $time . '_event_' . $event->id . '.jpg';
            $filepath = public_path('images/events/cover_images') . '/' . $filename;
            $img->save($filepath);
            $event->cover_image = $filename;
        }

        $img = Image::make(asset(str_replace(' ', '%20', $input['poster_image'])));
        $img->fit(842 , 1191);
        $filename = $time.'_event_'.$event->id.'.jpg';
        $filepath = public_path('images/events/posters').'/'.$filename;
        $img->save($filepath);

        $event->poster_image = $filename;

        if($event->status == 1){
            $event->published_at = Carbon::now();
        }

        $event->save();

        return redirect('cfadmin/event')->with('alert-success', 'Event #'.$event->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $event = Event::find($id);

        if(empty($event))
            return abort(404);

        if($this->isAdminRequest()){
            $view = 'event.cfadmin.detail';

            if(Auth::user()->hasRole('event')){
                if($event->user_id != Auth::user()->id){
                    return abort(404);
                }
            }

            return view($view, [
                'event' => $event,
            ]);
        }
        else{

            if($event->status == 0){
                return abort(404);
            }

            $view = 'event.detail';

            $event->views = $event->views + 1;
            $event->timestamps = false;
            $event->save();

//            $mostPopularPosts = Event::orderBy('updated_at')->get();
//            $relatednews = Event::withAnyTags($event->tags)->get();

            return view($view, [
                'event' => $event,
//                'mostPopularPosts' => $mostPopularPosts,
//                'relatednews' => $relatednews,
            ]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'poster_image' => 'required',
            'eventdate' => 'required',
            'location' => 'required',
        ];

        if(!empty($input['featured_flag'])){
            $validate['cover_image'] = 'required';
        }

        $this->validate($request, $validate);

        $event = Event::find($id);
        $event->name = $input['name'];
        $event->content = $input['content'];

        $time = time();

        if($input['cover_image'] != $event->cover_original_image){
            $event->cover_original_image = $input['cover_image'];
            $img = Image::make(asset(str_replace(' ', '%20', $input['cover_image'])));
            $img->fit(1140 , 400);
            $filename = $time.'_event_'.$event->id.'.jpg';
            $filepath = public_path('images/events/cover_images').'/'.$filename;
            $img->save($filepath);
            $event->cover_image = $filename;
        }

        if($input['poster_image'] != $event->poster_original_image){
            $event->poster_original_image = $input['poster_image'];
            $img = Image::make(asset(str_replace(' ', '%20', $input['poster_image'])));
            $img->fit(842 , 1191);
            $filename = $time.'_event_'.$event->id.'.jpg';
            $filepath = public_path('images/events/posters').'/'.$filename;
            $img->save($filepath);
            $event->poster_image = $filename;
        }

        $event->location = $input['location'];
        $event->price = !empty($input['price']) ? $input['price'] : 0;
        $event->ticket = $input['ticket'];
        $event->eventdate = Carbon::parse($input['eventdate'])->format('Y-m-d');
//        $event->reservationtime = $input['reservationtime'];
        $event->club_id =  (empty($input['club_id'])) ? null : $input['club_id'];
        if(!empty($input['tags'])){
            $event->tags = explode(',', $input['tags']);
        }
        else{
            $event->syncTags([]);
        }

        $old_flag = $event->featured_flag;
        $new_flag = (empty($input['featured_flag'])) ? 0 : 1;
        $event->featured_flag = $new_flag;

        /*
         * comment because dont update we need to know whos create this
         * */
//        $event->user_id = Auth::user()->id;

        $event->status = $input['status'];
        if($event->status == 1){
            $event->published_at = Carbon::now();
        }
        $event->save();

        //reorder featured
        if($old_flag == 1 && $new_flag == 0){
            $event->featured_order = 0;
            $event->save();
            $featuredevents = Event::where('featured_flag' , 1)->orderBy('featured_order')->get();
            for ($i = 0; $i < count($featuredevents); $i++) {
                $featuredevents[$i]->featured_order = $i+1;
                $featuredevents[$i]->save();
            }
        }
        elseif($old_flag == 0 && $new_flag == 1){
            $event->featured_order = Event::lastOrderFeatured() + 1;
            $event->save();
        }

        return redirect('cfadmin/event')->with('alert-success', 'Event #'.$event->id.' has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->status = 0;
        $event->featured_flag = 0;
        $event->featured_order = 0;
        $event->save();

        $featuredevents = Event::where('featured_flag' , 1)->orderBy('featured_order')->get();
        for ($i = 0; $i < count($featuredevents); $i++) {
            $featuredevents[$i]->featured_order = $i+1;
            $featuredevents[$i]->save();
        }

        return redirect('cfadmin/event')->with('alert-danger', 'Event #'.$event->id.' has been inactivated!');
    }

    public function active($id){
        $event = Event::find($id);
        $event->status = 1;
        if(empty($event->published_at)){
            $event->published_at = Carbon::now();
        }
        $event->save();
        return redirect('cfadmin/event')->with('alert-success', 'Event #'.$event->id.' has been activated!');
    }

    public function getMoreEvents()
    {

        return '';
    }

    public function featured(Request $request)
    {
        $featuredevents = Event::where('featured_flag' , 1)->orderBy('featured_order')->get();

        $events = Event::where('status',1)->where('featured_flag' , 0)->orderBy('created_at', 'desc')->get();

        return view('event.cfadmin.featured', [
            'featuredevents' => $featuredevents,
            'events' => $events,
        ]);
    }

    public function geteventnotfeatured(Request $request)
    {
        $events = Event::where('status',1)->where('featured_flag' , 0)->orderBy('created_at', 'desc')->get();

        if ($request->ajax()) {
            $view = view('event.cfadmin.loadeventnotfeatured', compact('events'))->render();
            return response()->json(['html' => $view]);
        }
    }

    public function addfeatured(Request $request)
    {
        $input = $request->input();

        $event = Event::find($input['eventid']);
        if($event->featured_flag == 1){
            return 1;
        }
        else{
            $event->featured_flag = 1;
            $event->featured_order = Event::lastOrderFeatured() + 1;
            $event->save();
            return 0;
        }
    }

    public function removefeatured(Request $request)
    {
        $input = $request->input();
        $event = Event::find($input['id']);
        $event->featured_flag = 0;
        $event->featured_order = 0;
        $event->save();

        $featuredevents = Event::where('featured_flag' , 1)->orderBy('featured_order')->get();
        for ($i = 0; $i < count($featuredevents); $i++) {
            $featuredevents[$i]->featured_order = $i+1;
            $featuredevents[$i]->save();
        }
    }

    public function reorderfeatured(Request $request)
    {

        $input = $request->input();

        $ids = explode(",", $input['ids']);
        for ($i = 0; $i < count($ids); $i++) {

            $event = Event::find($ids[$i]);
            $event->featured_order = $i+1;
            $event->save();
        }
    }

}
