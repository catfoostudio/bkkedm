<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    use \Spatie\Tags\HasTags;

    protected $table = 'posts';

//    protected $fillable = ['title', 'content', 'thumbnail_image','type','tags'];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Published';
        }
        elseif($this->status == 2){
            return 'Draft';
        }
        elseif($this->status == 0){
            return 'Inactive';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Published</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Draft</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Inactive</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

    public static function lastOrderTopstory()
    {
        $post = (new static)::where('topstory_flag' , 1)->orderBy('topstory_order' , 'desc')->first();

        if(empty($post)){
            return 0;
        }
        else{
            return $post->topstory_order;
        }

    }

    public static function getActiveMusicByCatPaginate($catid , $paginate)
    {
        if(!empty($catid)){
            return Post::where('type','music')->where('category_id',$catid)->where('status',1)->orderBy('published_at','desc')->paginate($paginate);
        }
        else{
            return Post::where('type','music')->where('status',1)->orderBy('published_at','desc')->paginate($paginate);
        }

    }

    public function vote()
    {
        return $this->belongsTo('App\Models\Vote');
    }

    public function prize()
    {
        return $this->belongsTo('App\Models\Prize');
    }

    public function contest()
    {
        return $this->belongsTo('App\Models\Contest');
    }

    public function getEmbedTypeAttribute()
    {
        $embedtype = 'none';

        if(!empty($this->prize_id)){
            $embedtype = 'reward';
        }
        elseif (!empty($this->vote_id)){
            $embedtype = 'vote';
        }
        elseif (!empty($this->contest_id)){
            $embedtype = 'contest';
        }

        return $embedtype;
    }
}
