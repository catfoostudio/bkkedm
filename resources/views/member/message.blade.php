@extends('layouts.main')
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="row clearfix">

                    <div class="col-md-9">

{{--                        <img src="{{ asset('images/icons/avatar.jpg') }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">--}}

                        <div class="heading-block noborder" style="margin-bottom: 0px;">
                            <h3>{{ $message->title }}</h3>
{{--                            <span style="font-size: 15px;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y') }}</span>--}}
                        </div>
                        {{--<div class="clear"></div>--}}
                        <div class="row clearfix">
                            <div class="col-lg-12">
                                <!-- Comments
						============================================= -->
                                <div id="comments" class="clearfix" style="margin-top: 5px;padding-top: 30px;">

                                    {{--<h3 id="comments-title"><span>3</span> Comments</h3>--}}

                                    <!-- Comments List
                                    ============================================= -->
                                    <ol class="commentlist clearfix">

                                        <li class="comment even thread-even depth-1" id="li-comment-1">

                                            <div id="comment-1" class="comment-wrap clearfix">

                                                <div class="comment-meta">

                                                    <div class="comment-author vcard">

												{{--<span class="comment-avatar clearfix">--}}
												{{--<img alt='' src='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60' class='avatar avatar-60 photo avatar-default' height='60' width='60' /></span>--}}

                                                    </div>

                                                </div>

                                                <div class="comment-content clearfix">

                                                    <div class="comment-author">
                                                        {{--{{ $message->sender->name }}--}}
                                                        <span>
                                                            <a href="#" title="Permalink to this comment" style="font-size:13px;font-family: 'Lato', sans-serif;">{{ \Carbon\Carbon::parse($message->created_at)->format('dS F Y - h:i A') }}</a>
                                                        </span>
                                                    </div>

                                                    <p>{!! $message->body  !!}</p>

                                                    {{--<a class='comment-reply-link' href='#'><i class="icon-reply"></i></a>--}}

                                                </div>

                                                <div class="clear"></div>

                                            </div>



                                        </li>

                                    </ol><!-- .commentlist end -->

                                    <div class="clear"></div>

                                    <!-- Comment Form
                                    ============================================= -->
                                    {{--<div id="respond" class="clearfix">--}}

                                        {{--<h3>Leave a <span>Comment</span></h3>--}}

                                        {{--<form class="clearfix" action="#" method="post" id="commentform">--}}

                                            {{--<div class="col_one_third">--}}
                                                {{--<label for="author">Name</label>--}}
                                                {{--<input type="text" name="author" id="author" value="" size="22" tabindex="1" class="sm-form-control" />--}}
                                            {{--</div>--}}

                                            {{--<div class="col_one_third">--}}
                                                {{--<label for="email">Email</label>--}}
                                                {{--<input type="text" name="email" id="email" value="" size="22" tabindex="2" class="sm-form-control" />--}}
                                            {{--</div>--}}

                                            {{--<div class="col_one_third col_last">--}}
                                                {{--<label for="url">Website</label>--}}
                                                {{--<input type="text" name="url" id="url" value="" size="22" tabindex="3" class="sm-form-control" />--}}
                                            {{--</div>--}}

                                            {{--<div class="clear"></div>--}}

                                            {{--<div class="col_full">--}}
                                                {{--<label for="comment">Comment</label>--}}
                                                {{--<textarea name="comment" cols="58" rows="7" tabindex="4" class="sm-form-control"></textarea>--}}
                                            {{--</div>--}}

                                            {{--<div class="col_full nobottommargin">--}}
                                                {{--<button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d nomargin">Submit Comment</button>--}}
                                            {{--</div>--}}

                                        {{--</form>--}}

                                    {{--</div><!-- #respond end -->--}}

                                </div><!-- #comments end -->
                            </div>
                        </div>
                    </div>
                    <div class="w-100 line d-block d-md-none"></div>

                    @include('member.sidebar')

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section('script')

@endsection
