<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->default(1);
            $table->string('name');
            $table->text('content');
            $table->string('thumbnail_image');
            $table->string('original_image');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('remark')->nullable();
            $table->integer('quota')->unsigned()->nullable();
            $table->integer('views')->unsigned()->nullable();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('input_amount')->nullable();
            $table->timestamps();
            $table->dateTime('published_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
