<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    //
    use \Spatie\Tags\HasTags;

    protected $fillable = ['title', 'content', 'thumbnail_image'];

}
