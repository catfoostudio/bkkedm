<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <title>@yield('title')BKK EDM - BANGKOK THAILAND ELECTRONIC DANCE MUSIC AND NEWS!!</title>
    @yield('fb-og')
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- Stylesheets
    <meta name="author" content="SemiColonWeb"/>
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
          rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700%7CRaleway:300,400,500,600,700,800,900%7CRoboto:700%2C500%2C400" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('style.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/fonts/font-awesome/css/font-awesome.css">

    <!-- basic stylesheet -->
    <link rel="stylesheet" href="{{ asset('royalslider/royalslider.css') }}">

    <!-- skin stylesheet (change it if you use another) -->
    <link rel="stylesheet" href="{{ asset('royalslider/skins/default/rs-default.css') }}">
    <link href="{{ asset('royalslider/skins/minimal-white/rs-minimal-white.css') }}" rel="stylesheet">
    <!-- Plugin requires jQuery 1.8+, use the latest version  -->
    <!-- If you already have jQuery on your page, you shouldn't include it second time. -->
    {{--<script src={{ asset('royalslider/jquery-1.8.3.min.js') }}></script>--}}


    <!-- Document Title
    ============================================= -->
    <style>
        .row.display-flex {
            display: flex;
            flex-wrap: wrap;
        }
        .row.display-flex > [class*='col-'] {
            display: flex;
            flex-direction: column;
        }

    </style>
    <style>
        .revo-slider-emphasis-text {
            font-size: 64px;
            font-weight: 700;
            letter-spacing: -1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }
        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }
        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }
        .tp-video-play-button { display: none !important; }
        .tp-caption { white-space: nowrap; }
    </style>
</head>
<body class="stretched sticky-responsive-menu">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">
<!-- Header
    ============================================= -->
@include('layouts.header')
<!-- Content
    ============================================= -->
    @yield('content')
    <!-- #content end -->
    <!-- Footer
    ============================================= -->
    @include('layouts.footer')
</div><!-- #wrapper end -->
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>
<!-- External JavaScripts
============================================= -->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- Footer Scripts
============================================= -->
<script src="{{ asset('js/functions.js') }}"></script>
@yield('script')
</body>
</html>