<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    //

    protected $table = 'choices';

    protected $guarded = [];

    public function vote()
    {
        return $this->belongsTo('App\Models\Vote');
    }

    public function users()
    {
        return $this->belongsToMany('App\User','choice_user')->withPivot('status')->withTimestamps();
    }
}
