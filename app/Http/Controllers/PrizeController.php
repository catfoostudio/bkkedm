<?php

namespace App\Http\Controllers;

use App\Exports\CodeRewardExport;
use App\Exports\RegisteredRewardExport;
use App\Mail\Reward;
use App\Models\Activatecode;
use App\Models\Club;
use App\Models\Event;
use App\Models\Hunter;
use App\Models\Message;
use App\Models\Prize;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class PrizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($this->isAdminRequest()) {
            $view = 'prize.cfadmin.index';

            $prizes = Prize::orderBy('created_at', 'desc')->get();

            return view($view, [
                'prizes' => $prizes,
            ]);
        } else {

            $view = 'prize.index';
            $prizes = Prize::where('status', 1)->whereDate('end', '>=', Carbon::now())->orderBy('end')->orderBy('created_at')->paginate(4);
            $pastprizes = Prize::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->orderBy('created_at')->paginate(4);

            if ($request->ajax()) {
                $view = view('prize.loadprizes', compact('prizes'))->render();
                return response()->json(['html' => $view]);
            }

            return view($view, [
                'prizes' => $prizes,
                'pastprizes' => $pastprizes,
            ]);
        }
    }

    public function loadpastprize(Request $request)
    {
        $prizes = Prize::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->orderBy('created_at')->paginate(4);

        if ($request->ajax()) {
            $view = view('prize.loadprizes', compact('prizes'))->render();
            return response()->json(['html' => $view]);
        }
    }

    public function loadprize(Request $request)
    {
        $prizes = Prize::where('status', 1)->whereDate('end', '>=', Carbon::now())->orderBy('end')->orderBy('created_at')->paginate(4);

        if ($request->ajax()) {
            $view = view('prize.loadprizes', compact('prizes'))->render();
            return response()->json(['html' => $view]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('prize.cfadmin.create', [
            'clubs' => Club::all(),
            'events' => Event::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
            'quota' => 'required',
            'reward' => 'required',
        ];


        if($input['type'] == 'club'){
            $validate['club'] = 'required|not_in:none';
        }
        elseif($input['type'] == 'event') {
            $validate['event'] = 'required|not_in:none';
        }
        else{

        }

        $this->validate($request, $validate);

//        DB::beginTransaction();
//        try {
            $prize = Prize::create([
                'name' => $input['name'],
                'content' => $input['content'],
                'thumbnail_image' => $input['filepath'],
                'original_image' => $input['filepath'],
                'user_id' => Auth::user()->id,
                'status' => $input['status'],
                'required_file' => (!empty($input['required_file'])) ? true : false,
                'required_link' => (!empty($input['required_link'])) ? true : false,
                'remark' => $input['remark'],
                'reward' => $input['reward'],
                'views' => 0,
                'quota' => $input['quota'],
                'start' => Carbon::parse($input['start'])->format('Y-m-d'),
                'end' => Carbon::parse($input['end'])->format('Y-m-d'),
                'club_id' => (!empty($input['club'])) ? $input['club']: null,
                'event_id' => (!empty($input['event'])) ? $input['event']: null,
            ]);

            if(!empty($input['tags'])){
                $prize->tags = explode(',', $input['tags']);
            }

            $time = time();
            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
            $img->fit(842, 1191);
            $filename = $time . '_prize_' . $prize->id . '.jpg';
            $filepath = public_path('images/prizes') . '/' . $filename;
            $img->save($filepath);

            $prize->thumbnail_image = $filename;

            if ($prize->status == 1) {
                $prize->published_at = Carbon::now();
            }

            $prize->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/prize')->with('alert-success', 'Reward #'.$prize->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //

        $prize = Prize::find($id);

        if (empty($prize))
            return abort(404);

        if ($this->isAdminRequest()) {
            $view = 'prize.cfadmin.detail';

            return view($view, [
                'prize' => $prize,
                'clubs' => Club::all(),
                'events' => Event::all(),
            ]);
        } else {
            $view = 'prize.detail';

            if($prize->status == 0){
                return abort(404);
            }

            if(Auth::check()){
                $hunter = Hunter::where('user_id',Auth::user()->id)->where('prize_id',$prize->id)->first();
            }
            else{
                $hunter = null;
            }

            $isPastPrize = (Carbon::parse($prize->end)->endOfDay()->lt(Carbon::now()));
            $isBeforePrize = (Carbon::parse($prize->start)->startOfDay()->gt(Carbon::now()));

            return view($view, [
                'prize' => $prize,
                'hunter' => $hunter,
                'isPastPrize' => $isPastPrize,
                'isBeforePrize' => $isBeforePrize,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
            'quota' => 'required',
            'reward' => 'required',
        ];


        if($input['type'] == 'club'){
            $validate['club'] = 'required|not_in:none';
        }
        elseif($input['type'] == 'event') {
            $validate['event'] = 'required|not_in:none';
        }
        else{

        }

        $this->validate($request, $validate);

//        DB::beginTransaction();
//        try {
            $prize = Prize::find($id);
            $prize->name = $input['name'];
            $prize->content = $input['content'];

            $prize->user_id = Auth::user()->id;
            if(!empty($input['status'])){
                $prize->status = $input['status'];
            }

            $prize->required_file = (!empty($input['required_file'])) ? true : false;
            $prize->required_link = (!empty($input['required_link'])) ? true : false;
            $prize->remark = $input['remark'];
            $prize->reward = $input['reward'];
            $prize->quota = $input['quota'];
            $prize->start = Carbon::parse($input['start'])->format('Y-m-d');
            $prize->end = Carbon::parse($input['end'])->format('Y-m-d');

            if($input['club'] != 'none'){
                $prize->club_id = $input['club'];
                $prize->event_id = null;
            }
            elseif($input['event'] != 'none'){
                $prize->event_id = $input['event'];
                $prize->club_id = null;
            }
            else{
                $prize->club_id = null;
                $prize->event_id = null;
            }


            if(!empty($input['tags'])){
                $prize->syncTags(explode(',',$input['tags']));
            }
            else{
                $prize->syncTags([]);
            }

            if($input['filepath'] != $prize->original_image) {
                $time = time();
                $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
                $img->fit(842, 1191);
                $filename = $time . '_prize_' . $prize->id . '.jpg';
                $filepath = public_path('images/prizes') . '/' . $filename;
                $img->save($filepath);
                $prize->original_image = $input['filepath'];
                $prize->thumbnail_image = $filename;
            }

            if ($prize->status == 1) {
                $prize->published_at = Carbon::now();
            }

            $prize->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/prize')->with('alert-success', 'Reward #'.$prize->id.' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prize = Prize::find($id);
        $prize->status = 0;
        $prize->save();
        return redirect('cfadmin/prize')->with('alert-danger', 'Reward #'.$prize->id.' has been inactivated!');
    }

    public function active($id)
    {
        $prize = Prize::find($id);
        $prize->status = 1;
        if ($prize->status == 1) {
            $prize->published_at = Carbon::now();
        }
        $prize->save();
        return redirect('cfadmin/prize')->with('alert-success', 'Reward #'.$prize->id.' has been activated!');
    }

    public function register(Request $request, $id)
    {
        $validate = [
            'g-recaptcha-response' => 'required|captcha',
        ];

        $prize = Prize::find($id);


        if($prize->required_link){
            $validate['link'] = 'required';
        }

        if($prize->required_file){
            $validate['file'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240';
        }

        $validator = Validator::make($request->all(), $validate);

        $validator->validate();

        if ($validator->fails()) {
            return redirect('reward/'.$prize->id)->withErrors($validator)->withInput();
        }
        else{


            $filename = '';
            if(Input::file('file')){
                $time = time();
                $filename = $time . '_prizes_' . $prize->id . '_'.Auth::user()->id.'.jpg';
                $filepath = public_path('registered') . '/' . $filename;
                Image::make(Input::file('file'))->save($filepath);
            }

            $prize->users()->attach([Auth::user()->id => ['file' => $filename,'link' => $request->input('link')]]);
            $prize->save();
            return redirect('reward/'.$prize->id);
        }

    }

    public function user(Request $request, $id)
    {
        $prize = Prize::find($id);

        $hunters = Hunter::where('prize_id',$id)->get();

        return view('prize.cfadmin.registered',[
            'prize' => $prize,
            'hunters' => $hunters
        ]);
    }

    public function export(Request $request, $id)
    {
        $prize = Prize::find($id);

        return Excel::download(new RegisteredRewardExport($id), 'registered-'.str_replace(' ', '', $prize->name).'.xlsx');
    }

    public function exportcode(Request $request, $id)
    {
        $prize = Prize::find($id);

        if (empty($prize)){
            return redirect('cfadmin/prize');
        }
        else {

            return Excel::download(new CodeRewardExport($id), 'code-'.str_replace(' ', '', $prize->name).'.xlsx');
        }


    }

    public function decline(Request $request,$id)
    {
        $hunter = Hunter::find($id);
        if($hunter->status == 2){
            Prize::find($hunter->prize->id)->activatecodes()->where('status',1)->take(1)->delete();
        }
        $hunter->status = 0;
        $hunter->save();

        return redirect('cfadmin/prizeuser/'.$hunter->prize->id);
    }

    public function approve($id)
    {
        $hunter = Hunter::find($id);
        $hunter->status = 1;
        $hunter->save();

        $hunters = Hunter::where('prize_id',$hunter->prize->id)->get();

        return view('prize.cfadmin.registered',[
            'prize' => $hunter->prize,
            'hunters' => $hunters
        ]);
    }

    public function luckydraw(Request $request, $id)
    {
        $number = $request->input('gen_code_amount');

        $hunters = Hunter::where('prize_id',$id)->where('status', 1)->inRandomOrder()->take($number);


        foreach ($hunters->get() as $hunter){

            $activatecode = new Activatecode();
            $activatecode->generatecode();
            $activatecode->type = 1;
            $activatecode->prize_id = $id;
            $activatecode->status = 1;
            $activatecode->save();

        }

        $hunters->update(['status' => 2]);

//        for ($i = 0; $i < $number; $i++) {
//            $activatecode = new Activatecode();
//            $activatecode->generatecode();
//            $activatecode->type = 1;
//            $activatecode->prize_id = $id;
//            $activatecode->status = 1;
//            $activatecode->save();
//        }

        return redirect('cfadmin/prizeuser/'.$id);
    }

    public function reward($id)
    {
        $hunter = Hunter::find($id);
        $hunter->status = 2;
        $hunter->save();

        $activatecode = new Activatecode();
        $activatecode->generatecode();
        $activatecode->type = 1;
        $activatecode->prize_id = $hunter->prize->id;
        $activatecode->status = 1;
        $activatecode->save();

        $hunters = Hunter::where('prize_id',$hunter->prize->id)->get();

        return view('prize.cfadmin.registered',[
            'prize' => $hunter->prize,
            'hunters' => $hunters
        ]);
    }

    public function code(Request $request, $id)
    {
        $prize = Prize::find($id);

        if (empty($prize)){
            return redirect('cfadmin/prize');
        }
        else {
            $activatecodes = $prize->activatecodes()->orderBy('created_at', 'desc')->get();

            return view('prize.cfadmin.code', [
                'prize' => $prize,
                'activatecodes' => $activatecodes,
            ]);
        }
    }

    public function destroyactivatecode($id)
    {

        $activatecode = Activatecode::find($id);
        $activatecode->status = 0;
        $activatecode->save();
        return redirect('cfadmin/prizecode/'.$activatecode->prize->id);
    }

    public function getevent(Request $request, $id)
    {
            $club = Club::find($id);

            $result = '<option value="none">None</option>';

            if (empty(count($club->events))) {
                return $result;
            }

            foreach ($club->events as $event) {
                $result .= '<option value="' . $event->id . '">' . $event->name . '</option>';
            }

            return $result;

    }

    public function getallevent(Request $request)
    {
        $events = Event::all();

        $result = '<option value="none">None</option>';

        if(empty(count($events))){
            return $result;
        }

        foreach($events as $event){
            $result .= '<option value="'.$event->id.'">'.$event->name.'</option>';
        }

        return $result;
    }

    public function getallclub(Request $request, $id)
    {
        $clubs = Club::all();

        $result = '<option value="none">None</option>';

        if(empty(count($clubs))){
            return $result;
        }

        foreach($clubs as $club){
            $result .= '<option value="'.$club->id.'">'.$club->name.'</option>';
        }

        return $result;
    }

    public function sendToWon(Request $request, $id)
    {
        $prize = Prize::find($id);
        $won = Hunter::where('prize_id',$id)->where('status', 2)->get();

        $activatecodes = $prize->activatecodes()->orderBy('created_at', 'desc')->get();
        $count = 0;
        foreach ($won as $item){
            $msg = new Message();
            $msg->title = "Congratulations! You've won a prize on ".Prize::find($id)->name;
            $msg->body = "Congratulations! You've won a prize on ".Prize::find($id)->name."<br>This is your redeem code for reward : <strong>".$activatecodes[$count]->code."</strong>";
            $msg->receipt_id = $item->user_id;
            $msg->save();

            $user = User::find($item->user_id);
            $input['prizename'] = Prize::find($id)->name;
            $input['code'] = $activatecodes[$count]->code;
            $input['username'] = $user->name;
            Mail::to($user->email)->send(new Reward($input));
            $count++;
        }

        return 'send';

    }

    public function resetreward(Request $request, $id)
    {

//        if(count(Prize::find($id)->activatecodes()->where('status',2)->get()) > 0)
//        {
//            return redirect('cfadmin/prizeuser/'.$id)->with('alert-danger', 'You can\'t reset reward, Because Redeem code has been redeemed!');
//        }
//        else{
//            $hunters = Hunter::where('prize_id',$id)->update(['status' => 1]);
//            Prize::find($id)->activatecodes()->delete();
//            return redirect('cfadmin/prizeuser/'.$id);
//        }

        $hunters = Hunter::where('prize_id',$id)->update(['status' => 1]);
        Prize::find($id)->activatecodes()->where('status',1)->delete();
        return redirect('cfadmin/prizeuser/'.$id);

    }

}
