<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gambler extends Model
{
    //
    protected $table = 'lotto_user';

    protected $guarded = [];

    public function activatecode()
    {
        return $this->hasOne('App\Models\Activatecode', 'id', 'activatecode_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function lotto()
    {
        return $this->hasOne('App\Models\Lotto', 'id', 'lotto_id');
    }

    public function getStatustextAttribute()
    {
        //do whatever you want to do
        if($this->status == 1){
            return 'Registered';
        }
        elseif($this->status == 2){
            return 'Not Available';
        }
        elseif($this->status == 3){
            return 'Start';
        }
        elseif($this->status == 4){
            return 'End';
        }
        elseif($this->status == 0){
            return 'Decline';
        }
        else{
            return $this->status;
        }

    }

    public function getStatusBadgeAttribute(){

        if($this->status == 1){
            return '<span class="label label-success">Registered</span>';
        }
        elseif($this->status == 2){
            return '<span class="label label-warning">Not Available</span>';
        }
        elseif($this->status == 3){
            return '<span class="label label-success">Start</span>';
        }
        elseif($this->status == 4){
            return '<span class="label label-danger">End</span>';
        }
        elseif($this->status == 0){
            return '<span class="label label-danger">Decline</span>';
        }
        else{
            return '<span class="label label-danger">Error Status</span>';
        }
    }

}
