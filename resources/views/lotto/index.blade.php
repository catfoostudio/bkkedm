@extends('layouts.main')
@section('content')
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding-bottom: 0px;padding-top: 30px;">
            <div class="container clearfix">
                <h3>LOTTOS</h3>
                <div id="lottos" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($lottos as $lotto)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('lotto/'.$lotto->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/lottos/'.$lotto->thumbnail_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('lotto/'.$lotto->id) }}">{{ $lotto->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($lotto->start)->format('dS M Y') }}</li>
                                <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($lotto->end)->format('dS M Y') }}</li>
                                @if(!empty($lotto->reward))<li><i class="icon-gift"></i>{{ $lotto->reward }}</li>@endif
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData('present')">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
            </div>
        </div>
    </section><!-- #content end -->

    @if(count($pastlottos) > 0)
    <div class="section dark" style="padding-bottom: 0px;padding-top: 40px;margin: 0px 0;">
        <div class="container clearfix">

            <h3>PAST LOTTOS</h3>
            <div id="pastlottos" class="post-grid clearfix" data-layout="fitRows">
                @foreach($pastlottos as $pastlotto)
                    <div class="entry clearfix">
                        <div class="entry-image" style="margin-bottom: 10px;">
                            <a href="{{ url('lotto/'.$pastlotto->id) }}">
                                <img class="image_fade" style="object-fit: cover" src="{{ asset('images/lottos/'.$pastlotto->thumbnail_image) }}" alt="Image">
                            </a>
                        </div>
                        <div class="entry-title" style="height: 51px;overflow: hidden;">
                            <h2><a href="{{ url('lotto/'.$pastlotto->id) }}">{{ $pastlotto->name }}</a></h2>
                        </div>
                        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($pastlotto->start)->format('dS M Y') }}</li>
                            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($pastlotto->end)->format('dS M Y') }}</li>
                            @if(!empty($pastlotto->reward))<li><i class="icon-gift"></i>{{ $pastlotto->reward }}</li>@endif
                        </ul>
                    </div>
                @endforeach
            </div>
            <!-- Pagination
                      ============================================= -->
            <div class="row mb-3">
                <div class="col-12" align="center">
                    <button class="button button-3d button-rounded button-black" onclick="loadMoreData('past')">Load more</button>
                </div>
            </div>
            <!-- .pager end -->
        </div>

    </div>
    @endif
@endsection
@section('script')
    <script>
        var pageNumber = 2;
        var pastPageNumber = 2;

        function loadMoreData(type){
            if(type == 'present'){
                if(pageNumber <= {{ $lottos->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "lotto?page=" +pageNumber,
                        success : function(data){
                            pageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#lottos').append(data.html);
                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }
            else
            {
                if(pastPageNumber <= {{ $pastlottos->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "loadpastlotto?page=" +pastPageNumber,
                        success : function(data){
                            pastPageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#pastlottos').append(data.html);
                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }

        }
    </script>
@endsection
