<?php

namespace App\Http\Controllers;

use App\Models\Choice;
use App\Models\Vote;
use App\Models\Voter;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        //
        if ($this->isAdminRequest()) {
            $view = 'vote.cfadmin.index';

            $votes = Vote::orderBy('created_at', 'desc')->get();

            return view($view, [
                'votes' => $votes,
            ]);
        } else {

            $view = 'vote.index';
            $votes = Vote::where('status', 1)->whereDate('end', '>=', Carbon::now())->orderBy('end')->paginate(4);
            $pastvotes = Vote::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->paginate(4);

            if ($request->ajax()) {
                $view = view('vote.loadvotes', compact('votes'))->render();
                return response()->json(['html' => $view]);
            }

            return view($view, [
                'votes' => $votes,
                'pastvotes' => $pastvotes,
            ]);

        }
    }

    public function loadpastvote(Request $request)
    {
        $votes = Vote::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->paginate(4);

        if ($request->ajax()) {
            $view = view('vote.loadvotes', compact('votes'))->render();
            return response()->json(['html' => $view]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('vote.cfadmin.create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
            'type' => 'required',
        ];

        // Static rules that don't change
        $v = Validator::make($request->all(), $validate);

        // Conditional rules that do change
        $v->sometimes('textlist_name', 'required|array|min:2', function ($input) {
            return $input->type === '1';
        })->sometimes('textlist_name.*', 'required|string', function ($input) {
            return $input->type === '1';
        })->sometimes('checkbox_name', 'required|array|min:2', function ($input) {
            return $input->type === '2';
        })->sometimes('checkbox_name.*', 'required|string', function ($input) {
            return $input->type === '2';
        })->sometimes('choice_name', 'required|array|min:2', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_name.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_image.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('inputbox_amount', 'required|integer|between:3,5', function ($input) {
            return $input->type === '4';
        });

//        $this->validate($request, $validate);
        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        else{

        }

//        DB::beginTransaction();
//        try {
            $vote = Vote::create([
                'name' => $input['name'],
                'content' => $input['content'],
                'thumbnail_image' => $input['filepath'],
                'original_image' => $input['filepath'],
                'user_id' => Auth::user()->id,
                'status' => $input['status'],
                'remark' => $input['remark'],
                'views' => 0,
                'quota' => 0,
                'type' => $input['type'],
                'start' => Carbon::parse($input['start'])->format('Y-m-d'),
                'end' => Carbon::parse($input['end'])->format('Y-m-d'),
            ]);

            if(!empty($input['tags'])){
                $vote->tags = explode(',', $input['tags']);
            }

            $choice_name = '';
            if($input['type'] == '1'){
                $choice_name = 'textlist_name';
            }
            else if($input['type'] == '2'){
                $choice_name = 'checkbox_name';
            }
            else if($input['type'] == '3'){
                $choice_name = 'choice_name';
            }

            if($input['type'] != '4') {
                for ($i = 0; $i < count($input[$choice_name]); $i++) {
                    $choice = new Choice();
                    $choice->name = $input[$choice_name][$i];
                    $choice->status = 1;
                    $choice->vote_id = $vote->id;
                    if ($input['type'] == '3') {
                        $choice->content = $input['choice_image'][$i];
                    }
                    $choice->save();
                }
            }
            else{
                $vote->input_amount = $input['inputbox_amount'];
            }

            $time = time();
            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
            $img->fit(842, 1191);
            $filename = $time . '_vote_' . $vote->id . '.jpg';
            $filepath = public_path('images/votes') . '/' . $filename;
            $img->save($filepath);

            $vote->thumbnail_image = $filename;

            if ($vote->status == 1) {
                $vote->published_at = Carbon::now();
            }

            $vote->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/vote')->with('alert-success', 'Vote #'.$vote->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vote = Vote::find($id);

        if (empty($vote))
            return abort(404);

        if ($this->isAdminRequest()) {
            $view = 'vote.cfadmin.detail';
//            dd($vote->choices);
            return view($view, [
                'vote' => $vote,
            ]);
        } else {
            $view = 'vote.detail';

            if($vote->status == 0){
                return abort(404);
            }

            $isPastVote = (Carbon::parse($vote->end)->endOfDay()->lt(Carbon::now()));
            $isBeforeVote = (Carbon::parse($vote->start)->startOfDay()->gt(Carbon::now()));

            if(Auth::check()) {

                $userChooses = Choice::whereHas('users', function ($query) {
                    $query->where('users.id', Auth::user()->id);
                })->where('vote_id', $id)->get();

                $isVoted = !empty(count($userChooses));
            }
            else{
                $isVoted = false;
                $userChooses = null;
            }

            return view($view, [
                'vote' => $vote,
                'voted' => $isVoted,
                'isPastVote' => $isPastVote,
                'isBeforeVote' => $isBeforeVote,
                'userChooses' => $userChooses,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after_or_equal:start',
            'type' => 'required',
        ];



        // Static rules that don't change
        $v = Validator::make($request->all(), $validate);

        // Conditional rules that do change
        $v->sometimes('textlist_name', 'required|array|min:2', function ($input) {
            return $input->type === '1';
        })->sometimes('textlist_name.*', 'required|string', function ($input) {
            return $input->type === '1';
        })->sometimes('textlist_name_new.*', 'required|string', function ($input) {
            return $input->type === '1';
        })->sometimes('checkbox_name', 'required|array|min:2', function ($input) {
            return $input->type === '2';
        })->sometimes('checkbox_name.*', 'required|string', function ($input) {
            return $input->type === '2';
        })->sometimes('checkbox_name_new.*', 'required|string', function ($input) {
            return $input->type === '2';
        })->sometimes('choice_name', 'required|array|min:2', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_name.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_image.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_name_new.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('choice_image_new.*', 'required|string', function ($input) {
            return $input->type === '3';
        })->sometimes('inputbox_amount', 'required|integer|between:3,5', function ($input) {
            return $input->type === '4';
        });

        if ($v->fails()) {
            return back()->withErrors($v)->withInput();
        }
        else{

        }

        $vote = Vote::find($id);

        $vote->name = $input['name'];
        $vote->content = $input['content'];

        $vote->status = $input['status'];
        $vote->remark = $input['remark'];

        if(!empty($input['tags'])){
            $vote->syncTags(explode(',',$input['tags']));
        }
        else{
            $vote->syncTags([]);
        }


        $vote->start = Carbon::parse($input['start'])->format('Y-m-d');
        $vote->end = Carbon::parse($input['end'])->format('Y-m-d');

        if($input['filepath'] != $vote->original_image) {
            $time = time();
            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
            $img->fit(842, 1191);
            $filename = $time . '_vote_' . $vote->id . '.jpg';
            $filepath = public_path('images/votes') . '/' . $filename;
            $img->save($filepath);

            $vote->thumbnail_image = $filename;
            $vote->original_image = $input['filepath'];
        }

        $choice_name = '';
        if($input['type'] == '1'){
            $choice_name = 'textlist_name';
        }
        else if($input['type'] == '2'){
            $choice_name = 'checkbox_name';
        }
        else if($input['type'] == '3'){
            $choice_name = 'choice_name';
        }

        if($input['type'] != '4') {

//
//            if($vote->status == 2){
//                $vote->type = $input['type'];
//
//                $vote->choices->delete();
//
//
//            }
//
//            $keys = array_keys($input[$choice_name]);

//            foreach($vote->choices as $item)
//            {
//                if(!in_array($item->id , $keys)){
//                    $item->delete();
//                }
//            }

            foreach ($input[$choice_name] as $key => $value){
                $choice = Choice::find($key);
                $choice->name = $value;
                if ($input['type'] == '3') {
                    $choice->content = $input['choice_image'][$key];
                }
                $choice->save();
            }

            if(!empty($input[$choice_name.'_new'])) {
                foreach ($input[$choice_name . '_new'] as $key => $value) {
                    $choice = new Choice();
                    $choice->name = $value;
                    $choice->vote_id = $id;
                    $choice->status = 1;
                    if ($input['type'] == '3') {
                        $choice->content = $input['choice_image_new'][$key];
                    }
                    $choice->save();
                }
            }
        }
        else{
            $vote->input_amount = $input['inputbox_amount'];
        }

        if ($vote->status == 1) {
            $vote->published_at = Carbon::now();
        }

        $vote->save();


//
//        DB::beginTransaction();
//        try {
//
//        $vote = Vote::find($id);
//
//        $vote->name = $input['name'];
//        $vote->content = $input['content'];
//
//
//        $vote->status = Auth::user()->id;
//        $vote->remark = $input['remark'];
//
//        if(!empty($input['tags'])){
//            $vote->syncTags(explode(',',$input['tags']));
//        }
//
//        $vote->start = Carbon::parse($input['start'])->format('Y-m-d');
//        $vote->end = Carbon::parse($input['end'])->format('Y-m-d');
//
////        if(count(Choice::has('users')->where('vote_id',$id)->get()) == 0){
////            //Choices update
////            $vote->choices()->delete();
////            for($i=0;$i<count($input['choice_name']);$i++){
////                $choice = Choice::create([
////                    'name' => $input['choice_name'][$i],
////                    'content' => $input['choice_image'][$i],
////                    'status' => 1,
////                    'vote_id' => $vote->id,
////                ]);
////                $vote->choices()->save($choice);
////            }
////        }
//
//
//
//            if($input['filepath'] != $vote->original_image) {
//            $time = time();
//            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
//            $img->fit(842, 1191);
//            $filename = $time . '_vote_' . $vote->id . '.jpg';
//            $filepath = public_path('images/votes') . '/' . $filename;
//            $img->save($filepath);
//
//            $vote->thumbnail_image = $filename;
//            $vote->original_image = $input['filepath'];
//        }
//
//        if ($vote->status == 1) {
//            $vote->published_at = Carbon::now();
//        }
//
//        $vote->save();
//
//        } catch (\Exception $e) {
//
//            DB::rollBack();
//            throw $e;
//        }
//
//        DB::commit();

        return redirect('cfadmin/vote')->with('alert-success', 'Vote #'.$vote->id.' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vote = Vote::find($id);
        $vote->status = 0;
        $vote->save();
        return redirect('cfadmin/vote')->with('alert-danger', 'Vote #'.$vote->id.' has been inactivated!');
    }

    public function active($id)
    {
        $vote = Vote::find($id);
        $vote->status = 1;
        if ($vote->status == 1) {
            $vote->published_at = Carbon::now();
        }
        $vote->save();
        return redirect('cfadmin/vote')->with('alert-success', 'Vote #'.$vote->id.' has been activated!');
    }

    public function choose(Request $request, $id)
    {
        $vote = Vote::find($id);
        $validate = [];

        if($vote->type == 2){
            $validate['checkbox'] = 'required';
        }
        elseif($vote->type == 4){
            $validate['inputbox.*'] = 'required|string|distinct';
        }

        $this->validate($request, $validate);


        if(!$vote->isVoted()) {
            $input = $request->input();
            if ($vote->type == 1) {
                $choice = Choice::find($input['textlist']);
                $choice->users()->attach(Auth::user()->id, ['status' => 1]);
                $choice->save();
            } elseif ($vote->type == 2) {
                foreach ($input['checkbox'] as $checkbox) {
                    $choice = Choice::find($checkbox);
                    $choice->users()->attach(Auth::user()->id, ['status' => 1]);
                    $choice->save();
                }
            } elseif ($vote->type == 3) {
                $choice = Choice::find($input['choice']);
                $choice->users()->attach(Auth::user()->id, ['status' => 1]);
                $choice->save();
            } elseif ($vote->type == 4) {

                foreach ($input['inputbox'] as $inputbox) {

                    $choice = Choice::where('vote_id', $vote->id)->where('name',trim($inputbox))->get()->first();
                    if(empty(count($choice))){

                        $choice = new Choice();
                        $choice->name = $inputbox;
                        $choice->status = 1;
                        $choice->vote_id = $vote->id;

                        $choice->user_id = Auth::user()->id;
                        $choice->save();

                        $choice->users()->attach(Auth::user()->id, ['status' => 1]);
                        $choice->save();

                    }
                    else{
                        if(empty($choice->users()->where('user_id',Auth::user()->id)->first())){
                            $choice->users()->attach(Auth::user()->id, ['status' => 1]);
                            $choice->save();
                        }
                    }

                }
            } else {
                //error
            }
        }
        else{
            return redirect('vote/'.$vote->id)->with('message', 'Update Successful');;
        }

        return redirect('vote/'.$choice->vote->id)->with('message', 'Update Successful');
    }

    public function user(Request $request, $id)
    {
        $vote = Vote::find($id);
        $choices = Choice::has('users')->where('vote_id',$id)->get();
        $counts = null;
        $all = 0;
        foreach($choices as $choice){

            $counts[$choice->name] = 0;
            foreach($choice->users as $voted){
                $counts[$choice->name] += 1;
                $all++;
            }
        }

        if(!empty($counts)) {
            arsort($counts);
        }

        return view('vote.cfadmin.registered',[
            'vote' => $vote,
            'choices' => $choices,
            'counts' => $counts,
            'all' => $all,
        ]);
    }
}
