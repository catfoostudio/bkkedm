@extends('layouts.main')
@section('content')
    <!-- Page Title
		============================================= -->
    <section id="page-title" style="background: url('images/parallax/home/5.jpg');background-size: cover;">
        <div class="container clearfix" align="center">
            <h2>Search "{{$key}}" - {{count($allnewsmusic)}} results</h2>
        </div>
    </section><!-- #page-title end -->
    @if(!empty(count($allnewsmusic)))
        <section style="position: relative;overflow: hidden;background-color: #FFF;padding-top: 30px;">
            <div class="container clearfix">
                <div id="posts" class="row">

                    @foreach($allnewsmusic as $news)
                        <div class="col-lg-4 col-md-6 col-xs-6 col-sm-6 col-6">
                            <div class="entry clearfix" >
                                <div class="entry-image" style="margin-bottom:10px;">
                                    <a href="{{ url('music/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}"><img class="image_fade"
                                                                                                                      src="{{ asset('images/posts/'.$news->thumbnail_image) }}"></a>
                                </div>
                                <div class="entry-title" style="margin-bottom: 10px;">
                                    <h2 style="height: auto;font-size: 18px;"><a href="{{ url('music/'.$news->id.'/'.str_replace(' ','-',$news->name)) }}">{{ $news->name }}</a></h2>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black"
                                onclick="loadMoreData('{{$key}}');">Load more
                        </button>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection

@section('script')
    <script>
        // $(document).ready(function () {
        // $('#myTab a[href="#profile"]').tab('show') // Select tab by name
        // $('#myTab li:first-child a').tab('show') // Select first tab
        // $('#myTab li:last-child a').tab('show') // Select last tab
        // $('#myTab li:nth-child(3) a').tab('show') // Select third tab

        var pageNumber = 2;


        function loadMoreData(cat) {
            var html = "<div class=\"css3-spinner\" style='background-color: #ffffff85'><div class=\"css3-spinner-bounce1\"></div><div class=\"css3-spinner-bounce2\"></div><div class=\"css3-spinner-bounce3\"></div></div>";
            $(html).hide().appendTo("body").fadeIn(500);
            $.ajax({
                type: 'GET',
                url: "/searchajaxnews?key="+cat+"&page=" + pageNumber,
                success: function (data) {
                    pageNumber += 1;
                    if (data.length == 0) {
                        // :( no more articles
                    } else {
                        $('#posts').append(data.html);
                    }
                    $('.css3-spinner').fadeOut(500);
                }, error: function (data) {
                    console.log('fail to load more event.');
                    $('.css3-spinner').fadeOut(500);
                },
                timeout: 10000 // sets timeout to 10 seconds
            })
        }

        var $loading = $('.css3-spinner').hide();
        //Attach the event handler to any element
        $(document)
            .ajaxStart(function () {
                //ajax request went so show the loading image
                $loading.show();
            })
            .ajaxStop(function () {
                //got response so hide the loading image
                $loading.hide();
            });
        // });
    </script>
@endsection