<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Contest;
use App\Models\Post;
use App\Models\Prize;
use App\Models\Video;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->isAdminRequest()){
            $view = 'videos.cfadmin.index';
            $posts = Video::orderBy('created_at','desc')->get();
            return view($view, [
                'posts' => $posts,
            ]);
        }
        else{

            $view = 'videos.index';
            $topvideos = Video::where('topstory_flag',1)->where('status',1)->orderBy('published_at','desc')->get();

            $allnewsmusic = Post::where('type','video')->where('status',1)->orderBy('published_at','desc')->paginate(6);
            $musichilights = Post::where('type','video')->where('status',1)->orderBy('published_at','desc')->take(5)->get();

            $categories = Category::where('type','video')->where('status',1)->get();

            return view($view, [
                'topvideos' => $topvideos,
                'allnewsmusic' => $allnewsmusic,
                'musichilights' => $musichilights,
                'categories' => $categories,
            ]);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('type','video')->where('status',1)->get();

        return view('videos.cfadmin.create', [
            'topstoryfull' => (count(Post::where('type', 'music')->where('status', 1)->where('topstory_flag', 1)->get()) >= 8) ? true : false,
            'categories' => $categories,
//            'rewards' => Prize::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
//            'votes' => Vote::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
//            'contests' => Contest::where('status', 1)->where('end','>=',Carbon::now()->startOfDay())->get(),
//            'rewards' => Prize::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//            'votes' => Vote::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
//            'contests' => Contest::where('status', 1)->where('start','<=',Carbon::now())->where('end','>=',Carbon::now())->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'youtube_id' => 'required',
            'artist' => 'required',
        ];


        $this->validate($request, $validate);

        $video = Video::create([
            'name' => $input['name'],
            'description' => $input['content'],
            'youtube_id' => $input['youtube_id'],
            'type' => 'youtube',
            'url' => $input['artist'],
            'user_id' => Auth::user()->id,
            'status' => $input['status'],
            'topstory_flag' => (empty($input['topstory_flag'])) ? 0 : 1,
            'category_id' => $input['category'],
        ]);

        if (!empty($input['tags'])) {
            $video->tags = explode(',', $input['tags']);
        }

        if ($video->status == 1) {
            $video->published_at = Carbon::now();
        }

        $video->save();

        return redirect('cfadmin/video')->with('alert-success', 'Video #'.$video->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $video = Video::find($id);

        if($this->isAdminRequest()){
            $view = 'videos.cfadmin.detail';
            $categories = Category::where('type','video')->where('status',1)->get();
            return view($view, [
                'post' => $video,
                'categories' => $categories,
            ]);
        }
        else{

            if($video->status == 0){
                return abort(404);
            }

//            if($video->type == 'lifestyle') {
                $view = 'videos.detail';
                $mostPopularPosts = Video::where('status',1)->orderBy('updated_at')->get();
                $relatednews = Video::where('status',1)->withAnyTags($video->tags)->get();

                return view($view, [
                    'post' => $video,
                    'mostPopularPosts' => $mostPopularPosts,
                    'relatednews' => $relatednews,
                ]);
//            }
//            else{
//                return abort(404);
//            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'youtube_id' => 'required',
            'artist' => 'required',
        ];

        $this->validate($request, $validate);

        $post = Video::find($id);
        $post->name = $input['name'];
        $post->description = $input['content'];
        $post->user_id = Auth::user()->id;
        $post->status = $input['status'];
        $post->url = $input['artist'];
        $post->category_id = $input['category'];
        $post->youtube_id = $input['youtube_id'];
        $post->topstory_flag =  (empty($input['topstory_flag'])) ? 0 : 1;

        if (!empty($input['tags'])) {
            $post->syncTags(explode(',', $input['tags'])); // all other tags on this model will be detached
        }
        else{
            $post->syncTags([]);
        }

        if ($post->status == 1) {
            $post->published_at = Carbon::now();
        }

        $post->save();

        return redirect('cfadmin/video')->with('alert-success', 'Video #'.$post->id.' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Video::find($id);
        $post->status = 0;
        $post->save();
        return redirect('cfadmin/video')->with('alert-danger', 'Video #'.$post->id.' has been inactivated.');
    }

    public function active($id)
    {
        $post = Video::find($id);
        $post->status = 1;
        if ($post->status == 1) {
            $post->published_at = Carbon::now();
        }
        $post->save();
        return redirect('cfadmin/video')->with('alert-success', 'Video #'.$post->id.' has been activated.');
    }
}
