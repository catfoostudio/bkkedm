<?php

namespace App\Http\Controllers;

use App\Models\Activatecode;
use App\Models\Gambler;
use App\Models\Lotto;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class LottoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($this->isAdminRequest()) {
            $view = 'lotto.cfadmin.index';
            $lottos = Lotto::orderBy('created_at', 'desc')->get();

            return view($view, [
                'lottos' => $lottos,
            ]);
        } else {

            $view = 'lotto.index';
            $lottos = Lotto::where('status', 1)->whereDate('end', '>=', Carbon::now())->orderBy('end')->paginate(4);
            $pastlottos = Lotto::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->paginate(4);

            if ($request->ajax()) {
                $view = view('lotto.loadlottos', compact('lottos'))->render();
                return response()->json(['html' => $view]);
            }

            return view($view, [
                'lottos' => $lottos,
                'pastlottos' => $pastlottos,
            ]);
        }
    }

    public function loadpastlotto(Request $request)
    {
        $lottos = Lotto::where('status', 1)->whereDate('end', '<', Carbon::now())->orderBy('end')->paginate(4);

        if ($request->ajax()) {
            $view = view('lotto.loadlottos', compact('lottos'))->render();
            return response()->json(['html' => $view]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('lotto.cfadmin.create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after:start',
            'quota' => 'required',
            'reward' => 'required',
        ];

        $this->validate($request, $validate);

//        DB::beginTransaction();
//        try {
            $lotto = Lotto::create([
                'name' => $input['name'],
                'content' => $input['content'],
                'thumbnail_image' => $input['filepath'],
                'original_image' => $input['filepath'],
                'user_id' => Auth::user()->id,
                'status' => $input['status'],
                'views' => 0,
                'quota' => $input['quota'],
                'reward' => $input['reward'],
                'start' => Carbon::parse($input['start'])->format('Y-m-d'),
                'end' => Carbon::parse($input['end'])->format('Y-m-d'),
            ]);

            $lotto->remark = $input['remark'];

            if(!empty($input['tags'])){
                $lotto->tags = explode(',', $input['tags']);
            }

            for ($i = 0; $i < $lotto->quota; $i++) {
                $activatecode = new Activatecode();
                $activatecode->generatecode();
                $activatecode->type = 1;
                $activatecode->lotto_id = $lotto->id;
                $activatecode->status = 1;
                $activatecode->save();
            }

            $time = time();
            $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
            $img->fit(842, 1191);
            $filename = $time . '_lotto_' . $lotto->id . '.jpg';
            $filepath = public_path('images/lottos') . '/' . $filename;
            $img->save($filepath);

//        $img->fit(1200 , 630);

            $lotto->thumbnail_image = $filename;

            if ($lotto->status == 1) {
                $lotto->published_at = Carbon::now();
            }

            $lotto->save();

//        } catch (\Exception $e) {
//            DB::rollBack();
//        }
//
//        DB::commit();

        return redirect('cfadmin/lotto')->with('message', 'Update Successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $lotto = Lotto::find($id);

        if (empty($lotto)){
            return abort(404);
        }
        else {

            if ($this->isAdminRequest()) {
                $view = 'lotto.cfadmin.detail';

                return view($view, [
                    'lotto' => $lotto,
                ]);

            } else {
                if($lotto->status == 1){
                    $view = 'lotto.detail';

                    if (Auth::check()) {
                        $gambler = Gambler::where('user_id', Auth::user()->id)->where('lotto_id', $lotto->id)->first();
                    } else {
                        $gambler = null;
                    }

                    $isPastLotto = (Carbon::parse($lotto->end)->endOfDay()->lt(Carbon::now()));

                    return view($view, [
                        'lotto' => $lotto,
                        'gambler' => $gambler,
                        'isPastLotto' => $isPastLotto,
                    ]);
                }
                else{
                    return abort(404);
                }

            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'start' => 'required|date',
            'end' => 'required|date|after:start',
            'quota' => 'required',
            'reward' => 'required',
        ];

        $this->validate($request, $validate);

        DB::beginTransaction();
        try {
            $lotto = Lotto::find($id);
            $lotto->name = $input['name'];
            $lotto->content = $input['content'];

            if(!empty($input['tags'])){
                $lotto->syncTags(explode(',',$input['tags']));
            }

            $lotto->user_id = Auth::user()->id;
            $lotto->status = $input['status'];
            $lotto->remark = $input['remark'];
            $lotto->quota = $input['quota'];
            $lotto->start = Carbon::parse($input['start'])->format('Y-m-d');
            $lotto->end = Carbon::parse($input['end'])->format('Y-m-d');

            if($input['filepath'] != $lotto->original_image) {
                $time = time();
                $img = Image::make(asset(str_replace(' ', '%20', $input['filepath'])));
                $img->fit(842, 1191);
                $filename = $time . '_lotto_' . $lotto->id . '.jpg';
                $filepath = public_path('images/lottos') . '/' . $filename;
                $img->save($filepath);
                $lotto->original_image = $input['filepath'];
                $lotto->thumbnail_image = $filename;
            }

            if ($lotto->status == 1) {
                $lotto->published_at = Carbon::now();
            }

            $lotto->save();
//
        } catch (\Exception $e) {
            DB::rollBack();
        }

        DB::commit();

        return redirect('cfadmin/lotto')->with('message', 'Update Successful');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Lotto::find($id);
        $post->status = 0;
        $post->save();
        return redirect('cfadmin/lotto')->with('message', 'IT WORKS!');
    }

    public function active($id)
    {
        $lotto = Lotto::find($id);
        $lotto->status = 1;
        $lotto->save();
        return redirect('cfadmin/lotto')->with('message', 'IT WORKS!');
    }

    public function redeem(Request $request, $id)
    {
        $validate = [
            'g-recaptcha-response' => 'required|captcha',
            'redeem_code' => [
                'required',
                Rule::exists('activatecodes','code')->where('status', 1)->where('lotto_id', $id),
            ],
        ];

        $validator = Validator::make($request->all(), $validate);

        $lotto = Lotto::find($id);

        if ($validator->fails()) {

            return redirect('lotto/'.$lotto->id)->withErrors($validator)->withInput();
        }
        else{


            $input = $request->input();
            $activatecode = Activatecode::where('status',1)->where('code', $input['redeem_code'])->get()->first();
            $lotto->users()->sync([Auth::user()->id => ['activatecode_id' => $activatecode->id]]);

            $lotto->save();
            $activatecode->status = 2;
            $activatecode->save();

            return redirect('lotto/'.$lotto->id);
        }

    }

    public function code(Request $request, $id)
    {
        $lotto = Lotto::find($id);

        if (empty($lotto)){
            return redirect('cfadmin/lotto');
        }
        else {
            $activatecodes = $lotto->activatecodes()->orderBy('created_at', 'desc')->get();

            return view('lotto.cfadmin.code', [
                'lotto' => $lotto,
                'activatecodes' => $activatecodes,
            ]);
        }
    }

    public function generateCode(Request $request, $id)
    {
        $input = $request->input();

        if(!empty($input['gen_code_amount']) && $input['gen_code_amount'] > 0) {
            $lotto = Lotto::find($id);
            for ($i = 0; $i < $input['gen_code_amount']; $i++) {
                $activatecode = new Activatecode();
                $activatecode->generatecode();
                $activatecode->type = 1;
                $activatecode->lotto_id = $lotto->id;
                $activatecode->status = 1;
                $activatecode->save();
            }

            $lotto->quota = $lotto->quota + $input['gen_code_amount'];
            $lotto->save();

        }

        return redirect('cfadmin/lottocode/'.$id);
    }

    public function destroyactivatecode($id)
    {

        $activatecode = Activatecode::find($id);
        $activatecode->status = 0;
        $activatecode->save();
        return redirect('cfadmin/lottocode/'.$activatecode->lotto->id);
    }

    public function user(Request $request, $id)
    {
        $lotto = Lotto::find($id);

        $gamblers = Gambler::where('lotto_id',$id)->get();

        return view('lotto.cfadmin.registered',[
            'lotto' => $lotto,
            'gamblers' => $gamblers
        ]);
    }

    public function decline(Request $request,$id)
    {
        $gambler = Gambler::find($id);
        $gambler->status = 0;
        $gambler->save();

        $gamblers = Gambler::where('lotto_id',$gambler->lotto->id)->get();

        return view('lotto.cfadmin.registered',[
            'lotto' => $gambler->lotto,
            'gamblers' => $gamblers
        ]);
    }

    public function approve($id)
    {
        $gambler = Gambler::find($id);
        $gambler->status = 1;
        $gambler->save();

        $gamblers = Gambler::where('lotto_id',$gambler->lotto->id)->get();

        return view('lotto.cfadmin.registered',[
            'lotto' => $gambler->lotto,
            'gamblers' => $gamblers
        ]);
    }
}
