@extends('layouts.main')
@section('content')
    <section id="page-title">
        <div class="container clearfix">
            <h1>Services</h1>
            {{--<span>We provide Amazing Solutions</span>--}}
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Services</li>
            </ol>
        </div>
    </section>

    <section id="content">

        <div class="content-wrap">

            {{--<div class="container clearfix">--}}
                {{--<div class="col_one_third">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-screen i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Responsive Layout</h3>--}}
                        {{--<p>Powerful Layout with Responsive functionality that can be adapted to any screen size. Resize browser to view.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col_one_third">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-eye i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Retina Graphics</h3>--}}
                        {{--<p>Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Retina Icons, Fonts &amp; all others graphics are optimized.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col_one_third col_last">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-beaker i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Powerful Performance</h3>--}}
                        {{--<p>Canvas includes tons of optimized code that are completely customizable and deliver unmatched fast performance.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="clear"></div>--}}
                {{--<div class="col_one_third nobottommargin">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-stack i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Premium Sliders</h3>--}}
                        {{--<p>Canvas included 20+ custom designed Slider Pages with Premium Sliders like Layer, Revolution, Swiper &amp; others.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col_one_third nobottommargin">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-tint i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Unlimited Colors</h3>--}}
                        {{--<p>Change the color scheme of the Theme in a flash just by changing the 6-digit HEX code in the colors.php file.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col_one_third nobottommargin col_last">--}}
                    {{--<div class="feature-box fbox-rounded fbox-effect">--}}
                        {{--<div class="fbox-icon">--}}
                            {{--<a href="#"><i class="icon-text-width i-alt"></i></a>--}}
                        {{--</div>--}}
                        {{--<h3>Customizable Fonts</h3>--}}
                        {{--<p>Use any Font you like from Google Web Fonts, Typekit or other Web Fonts. They will blend in perfectly.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="clear" style="margin-bottom: 50px;"></div>--}}
                {{--<div class="line"></div>--}}
                {{--<h3 class="center">What <span>Clients</span> Say</h3>--}}

                {{--<div class="fslider testimonial testimonial-full noshadow noborder nopadding divcenter" data-animation="fade" data-arrows="false" style="max-width: 700px;">--}}
                    {{--<div class="flexslider">--}}
                        {{--<div class="slider-wrap">--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--Steve Jobs--}}
                                        {{--<span>Apple Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--Collis Ta'eed--}}
                                        {{--<span>Envato Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="slide">--}}
                                {{--<div class="testi-content">--}}
                                    {{--<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>--}}
                                    {{--<div class="testi-meta">--}}
                                        {{--John Doe--}}
                                        {{--<span>XYZ Inc.</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="line"></div>--}}

                {{--<div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-margin="100" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="5" data-items-xl="6">--}}

                    {{--<div class="oc-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/2.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/3.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/4.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/5.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/6.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/7.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/8.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/9.png" alt="Clients"></a></div>--}}
                    {{--<div class="oc-item"><a href="#"><img src="images/clients/10.png" alt="Clients"></a></div>--}}

                {{--</div>--}}


            {{--</div>--}}
            <div class="promo promo-dark promo-flat promo-full topmargin footer-stick" style="background-color: #555">
                <div class="container clearfix">
                    <h3>Contact us at <span>(+66) 0924954624</span> or email <span>bkkedm@gmail.com</span></h3>
                    {{--<span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span>--}}
                    {{--<a href="#" class="button button-xlarge button-rounded">Start Browsing</a>--}}
                </div>
            </div>
        </div>

    </section><!-- #content end -->
@endsection