<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Home - Magazine Layout | Canvas</title>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Top Bar
    ============================================= -->
    {{--<div id="top-bar">--}}

        {{--<div class="container clearfix">--}}

            {{--<div class="col_half nobottommargin">--}}

                {{--<!-- Top Links--}}
                {{--============================================= -->--}}
                {{--<div class="top-links">--}}
                    {{--<ul>--}}
                        {{--<li><a href="index.html">Home</a></li>--}}
                        {{--<li><a href="faqs.html">FAQs</a></li>--}}
                        {{--<li><a href="contact.html">Contact</a></li>--}}
                        {{--<li><a href="login-register.html">Login</a>--}}
                            {{--<div class="top-link-section">--}}
                                {{--<form id="top-login" role="form">--}}
                                    {{--<div class="input-group" id="top-login-username">--}}
                                        {{--<div class="input-group-prepend">--}}
                                            {{--<div class="input-group-text"><i class="icon-user"></i></div>--}}
                                        {{--</div>--}}
                                        {{--<input type="email" class="form-control" placeholder="Email address" required="">--}}
                                    {{--</div>--}}
                                    {{--<div class="input-group" id="top-login-password">--}}
                                        {{--<div class="input-group-prepend">--}}
                                            {{--<div class="input-group-text"><i class="icon-key"></i></div>--}}
                                        {{--</div>--}}
                                        {{--<input type="password" class="form-control" placeholder="Password" required="">--}}
                                    {{--</div>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" value="remember-me"> Remember me--}}
                                    {{--</label>--}}
                                    {{--<button class="btn btn-danger btn-block" type="submit">Sign in</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div><!-- .top-links end -->--}}

            {{--</div>--}}

            {{--<div class="col_half fright col_last nobottommargin">--}}

                {{--<!-- Top Social--}}
                {{--============================================= -->--}}
                {{--<div id="top-social">--}}
                    {{--<ul>--}}
                        {{--<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>--}}
                        {{--<li><a href="#" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>--}}
                        {{--<li><a href="#" class="si-dribbble"><span class="ts-icon"><i class="icon-dribbble"></i></span><span class="ts-text">Dribbble</span></a></li>--}}
                        {{--<li><a href="#" class="si-github"><span class="ts-icon"><i class="icon-github-circled"></i></span><span class="ts-text">Github</span></a></li>--}}
                        {{--<li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest"></i></span><span class="ts-text">Pinterest</span></a></li>--}}
                        {{--<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>--}}
                        {{--<li><a href="tel:+91.11.85412542" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+91.11.85412542</span></a></li>--}}
                        {{--<li><a href="mailto:info@canvas.com" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">info@canvas.com</span></a></li>--}}
                    {{--</ul>--}}
                {{--</div><!-- #top-social end -->--}}

            {{--</div>--}}

        {{--</div>--}}

    {{--</div><!-- #top-bar end -->--}}

    <!-- Header
    ============================================= -->
   @include('layouts.header')

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap" style="padding-top: 15px;">

            <div class="container clearfix">
                <div class="fancy-title title-border">
                    <h3>Top Stories</h3>
                </div>

                <div class="row bottommargin-sm clearfix">

                    <div class="col-lg-8 bottommargin">

                        <div class="ipost clearfix">
                            <div class="entry-image">
                                <a href="#"><img class="image_fade" src="images/magazine/7.jpg" alt="Image"></a>
                            </div>
                            <div class="entry-title">
                                <h3><a href="blog-single.html">Toyotas next minivan will let you shout at your kids without turning around</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 17th Jan 2014</li>
                                <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 31</a></li>
                                <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                            </ul>
                            <div class="entry-content">
                                <p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, repudiandae.</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 bottommargin">
                        Category
                        <div class="btn-group">
                            <button style="margin-left: 10px;" type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Music
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Event</a>
                                <a class="dropdown-item" href="#">Club</a>
                                <a class="dropdown-item" href="#">Video</a>
                                {{--<div class="dropdown-divider"></div>--}}
                                {{--<a class="dropdown-item" href="#">Separated link</a>--}}
                            </div>
                        </div>
                        @for($i = 1; $i <= 4; $i++)
                        <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">
                            <div class="entry-image">
                                <a href="#"><img src="{{ asset('images/magazine/'.$i.'.jpg') }}" alt="" style="object-fit: cover;"></a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h4><a href="#">UK government weighs Tesla's Model S for its ??5 million electric vehicle fleet</a></h4>
                                </div>
                            </div>
                        </div>
                        @endfor

                    </div>

                </div>

            </div>

            <div class="section dark">

                <div class="container clearfix">
                    <h3>Events</h3>
                    {{--<iframe src="http://player.vimeo.com/video/99895335" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>--}}
                    <img src="{{ asset('images/event_poster/12-30-17-The-Bangkok-Countdown-DJ-Banner-ss.png') }}" alt="Ad" class="aligncenter" style="margin-bottom: 20px">

                    <button class="button button-3d nomargin" type="button" >More events</button>
                    <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="30" data-nav="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">
                        @for($i = 1; $i <= 7; $i++)
                            <div class="oc-item">
                                <div class="ipost clearfix">
                                    <div class="entry-image" >
                                        <a href="#"><img class="image_fade" style="object-fit: cover"  src="{{ asset('images/event_poster/edm_event_poster_0000'.$i.'.jpg') }}" alt="Image"></a>
                                    </div>

                                </div>
                            </div>
                        @endfor
                    </div>


                </div>

            </div>

            <div class="container clearfix">

                <div class="fancy-title title-border">
                    <h3>Video</h3>
                </div>

                <div class="row bottommargin-sm clearfix">

                    <div class="col-lg-8 bottommargin">

                        <div class="ipost clearfix">
                            <div class="entry-image">
                                <a href="#">

                                    <iframe width="560" height="315" src="http://www.youtube.com/embed/SZEflIVnhH8" frameborder="0" allowfullscreen></iframe>


                                </a>
                            </div>
                            <div class="entry-title">
                                <h3><a href="blog-single.html">Toyotas next minivan will let you shout at your kids without turning around</a></h3>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 17th Jan 2014</li>
                                <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 31</a></li>
                                <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                            </ul>
                            <div class="entry-content">
                                <p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, repudiandae.</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 bottommargin">
                        <button class="button button-3d nomargin" type="button" >More videoes</button>
                        @for($i = 1; $i <= 4; $i++)
                            <div class="mpost clearfix" style="margin-top: 10px;padding-top: 10px;">
                                <div class="entry-image">
                                    <a href="#"><img src="{{ asset('images/magazine/'.$i.'.jpg') }}" alt="" style="object-fit: cover;"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#">UK government weighs Tesla's Model S for its ??5 million electric vehicle fleet</a></h4>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    @include('layouts.footer')

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="js/functions.js"></script>

</body>
</html>