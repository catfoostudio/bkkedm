<?php

namespace App\Http\Controllers;

use App\Models\Activatecode;
use App\Models\Hunter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RedeemController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {


        if($this->isAdminRequest()){


            return view('redeem.cfadmin.index', [

            ]);
        }
        else{

            return view('redeem.index', [

            ]);
        }
    }

    public function redeem(Request $request)
    {
        $validate = [
            'g-recaptcha-response' => 'required|captcha',
            'redeem_code' => [
                'required',
                Rule::exists('activatecodes','code')->where('status', 1),
            ],
        ];

        $this->validate($request, $validate);

        $activatecode = Activatecode::where('status',1)->where('code', $request->input('redeem_code'))->get()->first();

        if($this->isAdminRequest()) {
            return view('redeem.cfadmin.confirm', [
                'activatecode' => $activatecode,
            ]);
        }
        else{

            $hunter = Hunter::where('prize_id',$activatecode->prize_id)->where('user_id',Auth::user()->id)->where('status',2)->get();

            if(empty(count($hunter))){
                return redirect()->back()->withErrors(['redeem_code'=>'error'])->withInput();
            }

            return view('redeem.confirm', [
                'activatecode' => $activatecode,
            ]);
        }

    }

    public function confirm(Request $request, $id)
    {
        $activatecode = Activatecode::find($id);
        $activatecode->status = 2;
        $activatecode->save();

        $hunter = Hunter::where('prize_id',$activatecode->prize->id)->where('user_id', \Auth::user()->id)->first();
        $hunter->activatecode_id = $id;
        $hunter->status = 2;
        $hunter->save();

        if($this->isAdminRequest()) {

            return redirect('cfadmin/redeem')->with('alert-success', 'Redeem successful!');
        }
        else{
            return redirect('redeem')->with('alert-success', 'Redeem successful!');
        }
    }
}
