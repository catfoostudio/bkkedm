<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //

    public function sender()
    {
        return $this->hasOne('App\User','id', 'sender_id');
    }

    public function receipt()
    {
        return $this->hasOne('App\User','id', 'receipt_id');
    }

}
