<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Podcast;
use App\Models\Post;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postsAllBuilder = Post::where('status',1)->where('topstory_flag' , 1)->orderBy('published_at', 'desc');
        $firstAllStory = $postsAllBuilder->first();
        $nextAllThreeStories = $postsAllBuilder->skip(1)->take(4)->get();

        $postsMusicBuilder = Post::where('type','music')->where('status',1)->where('topstory_flag' , 1)->orderBy('topstory_order');
        $firstMusicStory = $postsMusicBuilder->first();
        $nextMusicThreeStories = $postsMusicBuilder->skip(1)->take(4)->get();

        $postsLifestyleBuilder = Post::where('type','lifestyle')->where('status',1)->where('topstory_flag' , 1)->orderBy('topstory_order');
        $firstLifestyleStory = $postsLifestyleBuilder->first();
        $nextLifestyleThreeStories = $postsLifestyleBuilder->skip(1)->take(4)->get();

        $featuredEvents = Event::where('featured_flag' , 1)->where('status',1)->orderBy('featured_order')->get();
        $events = Event::where('status' , 1)->whereDate('eventdate', '>=' , Carbon::now())->orderBy('eventdate')->take(5)->get();

        $videosBuilder = Video::where('topstory_flag' , 1)->where('status',1)->orderBy('topstory_order');
        $firstVideo = $videosBuilder->first();
        $nextVideosThreeStories = $videosBuilder->skip(1)->take(3)->get();

        $postsMusicBuilder = Post::where('type','music')->where('status',1)->where('topstory_flag' , 1)->orderBy('topstory_order');
        $postsMusic = $postsMusicBuilder->take(8)->get();
        $firstMusicStory = $postsMusicBuilder->skip(3)->first();
        $nextMusicThreeStories = $postsMusicBuilder->skip(4)->take(4)->get();
//        dd($nextMusicThreeStories);

        $postsLifestyleBuilder = Post::where('type','lifestyle')->where('status',1)->where('topstory_flag' , 1)->orderBy('topstory_order');
        $postsLifestyle = $postsLifestyleBuilder->take(6)->get();
        $firstLifestyleStory = $postsLifestyleBuilder->skip(1)->first();
        $nextLifestyleThreeStories = $postsLifestyleBuilder->skip(2)->take(4)->get();

        return view('index', [
            'firstAllStory' => $firstAllStory,
            'nextAllThreeStories' => $nextAllThreeStories,
            'firstMusicStory' => $firstMusicStory,
            'nextMusicThreeStories' => $nextMusicThreeStories,
            'firstLifestyleStory' => $firstLifestyleStory,
            'nextLifestyleThreeStories' => $nextLifestyleThreeStories,
            'featuredEvents' => $featuredEvents,
            'events' => $events,
            'firstVideo' => $firstVideo,
            'nextVideosThreeStories' => $nextVideosThreeStories,

            'postsMusic' => $postsMusic,
            'postsLifestyle' => $postsLifestyle,
        ]);
    }
}
