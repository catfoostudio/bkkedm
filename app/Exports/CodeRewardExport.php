<?php

namespace App\Exports;

use App\Models\Prize;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CodeRewardExport  implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(int $id)
    {
        $this->prizeId = $id;
    }

    public function query()
    {
        return Prize::find($this->prizeId)->activatecodes()->orderBy('created_at', 'desc');
    }

    public function map($code): array
    {
        return [
            $code->statustext,
            $code->code,
        ];
    }

    public function headings(): array
    {
        return [
            'Status',
            'Code',
        ];
    }
}
