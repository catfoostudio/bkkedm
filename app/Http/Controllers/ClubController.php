<?php

namespace App\Http\Controllers;

use App\Models\Club;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->isAdminRequest()){
            $view = 'club.cfadmin.index';

            if(Auth::user()->hasRole('club')){
                $clubs = Club::where('id', Auth::user()->club_id)->orderBy('updated_at')->get();
            }
            else{
                $clubs = Club::orderBy('updated_at')->get();
            }

        }
        else{
            $view = 'club.index';
            $clubs = Club::where('status', 1)->orderBy('updated_at')->get();
        }

        return view($view, [
            'clubs' => $clubs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('club.cfadmin.create', [
//            'posts' => $posts,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'logo' => 'required',
            'location' => 'required',
        ];

        $this->validate($request, $validate);

        $club = Club::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'location' => $input['location'],
            'original_image' => $input['filepath'],
            'logo_original_image' => $input['logo'],
            'type' => $input['type'],
            'facebook_id' => $input['facebook_id'],
            'tel' => $input['tel'],
            'status' => $input['status'],
            'thumbnail_image' => '',
            'logo_image' => '',
        ]);

        if(!empty($input['tags'])){
            $club->tags = explode(',', $input['tags']);
        }

        $path = str_replace(' ', '%20', $input['filepath']);
        $time = time();
        $img = Image::make(asset($path));
        $img->fit(845 , 370);
        $filename = $time.'_club_'.$club->id.'.jpg';
        $filepath = public_path('images/clubs').'/'.$filename;
        $img->save($filepath);
        $club->thumbnail_image = $filename;

        $path = str_replace(' ', '%20', $input['logo']);
        $time = time();
        $img = Image::make(asset($path));
        $img->fit(240 , 240);
        $filename = $time.'_clublogo_'.$club->id.'.jpg';
        $filepath = public_path('images/clubs').'/'.$filename;
        $img->save($filepath);
        $club->logo_image = $filename;

        $club->save();

        return redirect('cfadmin/club')->with('alert-success', 'Club #'.$club->id.' has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $club = Club::find($id);

        if(empty($club)){
            return abort(404);
        }

        if($this->isAdminRequest()){
            $view = 'club.cfadmin.detail';


            if(Auth::user()->hasRole('club')){
                if($id != Auth::user()->club_id){
                    return abort(404);
                }
            }

            return view($view, [
                'club' => $club,
            ]);
        }
        else {

            if($club->status == 0){
                return abort(404);
            }

            $view = 'club.detail';

            return view($view, [
                'club' => $club,
            ]);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();

        $validate = [
            'name' => 'required',
            'content' => 'required',
            'filepath' => 'required',
            'logo' => 'required',
            'location' => 'required',
        ];

        $this->validate($request, $validate);

        $club = Club::find($id);

        if($input['filepath'] != $club->original_image){
            $path = str_replace(' ', '%20', $input['filepath']);
            $time = time();
            $img = Image::make(asset($path));
            $img->fit(845 , 560);
            $filename = $time.'_club_'.$club->id.'.jpg';
            $filepath = public_path('images/clubs').'/'.$filename;
            $img->save($filepath);
            $club->thumbnail_image = $filename;
            $club->original_image = $input['filepath'];
            $club->save();
        }

        if($input['logo'] != $club->logo_original_image){
            $path = str_replace(' ', '%20', $input['logo']);
            $time = time();
            $img = Image::make(asset($path));
            $img->fit(240 , 240);
            $filename = $time.'_clublogo_'.$club->id.'.jpg';
            $filepath = public_path('images/clubs').'/'.$filename;
            $img->save($filepath);
            $club->logo_image = $filename;
            $club->logo_original_image = $input['logo'];
            $club->save();
        }

        $club->name = $input['name'];
        $club->location = $input['location'];
        $club->content = $input['content'];
        $club->type = $input['type'];
        $club->tel = $input['tel'];
        $club->facebook_id = $input['facebook_id'];
        $club->status = $input['status'];
        if(!empty($input['tags'])){
            $club->tags = explode(',', $input['tags']);
        }
        else{
            $club->syncTags([]);
        }
        $club->save();

        return redirect('cfadmin/club')->with('alert-success', 'Club #'.$club->id.' has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $club = Club::find($id);
        $club->status = 0;
        $club->save();
        return redirect('cfadmin/club')->with('alert-danger', 'Club #'.$club->id.' has been inactivated!');
    }

    public function active($id)
    {
        //
        $club = Club::find($id);
        $club->status = 1;
        $club->save();
        return redirect('cfadmin/club')->with('alert-success', 'Club #'.$club->id.' has been activated!');
    }
}
