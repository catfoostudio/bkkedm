@foreach($lottos as $lotto)
    <div class="entry clearfix">
        <div class="entry-image" style="margin-bottom: 10px;">
            <a href="{{ url('lotto/'.$lotto->id) }}">
                <img class="image_fade" style="object-fit: cover"
                                                          src="{{ asset('images/lottos/'.$lotto->thumbnail_image) }}" alt="Image"></a>
        </div>
        <div class="entry-title" style="height: 51px;overflow: hidden;">
            <h2><a href="{{ url('lotto/'.$lotto->id) }}">{{$lotto->name}}</a></h2>
        </div>
        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;">
            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($lotto->start)->format('dS M Y') }}</li>
            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($lotto->end)->format('dS M Y') }}</li>
            @if(!empty($lotto->reward))<li><i class="icon-gift"></i>{{ $lotto->reward }}</li>@endif
        </ul>
    </div>
@endforeach