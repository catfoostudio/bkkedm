@extends('layouts.main')
@section('content')
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding-bottom: 0px;padding-top: 30px;">
            <div class="container clearfix">
                <h3>REWARD</h3>
                <div id="prizes" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($prizes as $prize)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('reward/'.$prize->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/prizes/'.$prize->thumbnail_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('reward/'.$prize->id) }}">{{ $prize->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($prize->start)->format('dS M Y') }}</li>
                                <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($prize->end)->format('dS M Y') }}</li>
                                @if(!empty($prize->reward))<li><i class="icon-gift"></i>{{ $prize->reward }}</li>@endif
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData('present')">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
            </div>
        </div>
    </section><!-- #content end -->
    @if(count($pastprizes) > 0)
    <div class="section dark" style="padding-bottom: 0px;padding-top: 40px;margin: 0px 0;">
        <div class="container clearfix">

            <h3>PAST PRIZES</h3>
            <div id="pastprizes" class="post-grid clearfix" data-layout="fitRows">
                @foreach($pastprizes as $pastprize)
                    <div class="entry clearfix">
                        <div class="entry-image" style="margin-bottom: 10px;">
                            <a href="{{ url('prize/'.$pastprize->id) }}">
                                <img class="image_fade" style="object-fit: cover" src="{{ asset('images/prizes/'.$pastprize->thumbnail_image) }}" alt="Image">
                            </a>
                        </div>
                        <div class="entry-title" style="height: 51px;overflow: hidden;">
                            <h2><a href="{{ url('prize/'.$pastprize->id) }}">{{ $pastprize->name }}</a></h2>
                        </div>
                        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($pastprize->start)->format('dS M Y') }}</li>
                            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($pastprize->end)->format('dS M Y') }}</li>
                            @if(!empty($pastprize->reward))<li><i class="icon-gift"></i>{{ $pastprize->reward }}</li>@endif
                        </ul>
                    </div>
                @endforeach
            </div>
            <!-- Pagination
                      ============================================= -->
            <div class="row mb-3">
                <div class="col-12" align="center">
                    <button class="button button-3d button-rounded button-black" onclick="loadMoreData('past')">Load more</button>
                </div>
            </div>
            <!-- .pager end -->
        </div>

    </div>
    @endif
@endsection
@section('script')
    <script>
        var pageNumber = 2;
        var pastPageNumber = 2;

        function loadMoreData(type){
            if(type == 'present'){
                if(pageNumber <= {{ $prizes->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "loadprize?page=" +pageNumber,
                        success : function(data){
                            pageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#prizes').append(data.html);
                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }
            else
            {
                if(pastPageNumber <= {{ $pastprizes->lastPage() }}){
                    $.ajax({
                        type : 'GET',
                        url: "loadpastprize?page=" +pastPageNumber,
                        success : function(data){
                            pastPageNumber +=1;
                            if(data.length == 0){
                                // :( no more articles
                            }else{
                                $('#pastprizes').append(data.html);

                            }
                        },error: function(data){
                            console.log('fail to load more event.');
                        },
                    })
                }
            }

        }
    </script>
@endsection
