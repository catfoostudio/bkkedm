@extends('layouts.main')
@section('content')
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding-bottom: 0px;padding-top: 30px;">
            <div class="container clearfix">
                <h3>CONTESTS</h3>
                <div id="posts" class="post-grid clearfix" data-layout="fitRows">
                    @foreach($contests as $contest)
                        <div class="entry clearfix">
                            <div class="entry-image" style="margin-bottom: 10px;">
                                <a href="{{ url('contest/'.$contest->id) }}">
                                    <img class="image_fade" style="object-fit: cover" src="{{ asset('images/contests/'.$contest->thumbnail_image) }}" alt="Image">
                                </a>
                            </div>
                            <div class="entry-title" style="height: 51px;overflow: hidden;">
                                <h2><a href="{{ url('contest/'.$contest->id) }}">{{ $contest->name }}</a></h2>
                            </div>
                            <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                                <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($contest->start)->format('dS M Y') }}</li>
                                <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($contest->end)->format('dS M Y') }}</li>
                                <li><i class="icon-gift"></i>{{ $contest->reward }}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination
                          ============================================= -->
                <div class="row mb-3">
                    <div class="col-12" align="center">
                        <button class="button button-3d button-rounded button-black" onclick="loadMoreData()">Load more</button>
                    </div>
                </div>
                <!-- .pager end -->
            </div>
        </div>
    </section><!-- #content end -->
    @if(count($pastcontests) > 0)
    <div class="section dark" style="padding-bottom: 0px;padding-top: 40px;margin: 0px 0;">
        <div class="container clearfix">

            <h3>PAST CONTESTS</h3>
            <div id="posts" class="post-grid clearfix" data-layout="fitRows">
                @foreach($pastcontests as $pastcontest)
                    <div class="entry clearfix">
                        <div class="entry-image" style="margin-bottom: 10px;">
                            <a href="{{ url('contest/'.$pastcontest->id) }}">
                                <img class="image_fade" style="object-fit: cover" src="{{ asset('images/contests/'.$pastcontest->thumbnail_image) }}" alt="Image">
                            </a>
                        </div>
                        <div class="entry-title" style="height: 51px;overflow: hidden;">
                            <h2><a href="{{ url('prize/'.$pastcontest->id) }}">{{ $pastcontest->name }}</a></h2>
                        </div>
                        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;height: 35px;">
                            <li><i class="icon-time"></i>Start: {{ \Carbon\Carbon::parse($pastcontest->start)->format('dS M Y') }}</li>
                            <li><i class="icon-time"></i>End: {{ \Carbon\Carbon::parse($pastcontest->end)->format('dS M Y') }}</li>
                            <li><i class="icon-gift"></i>{{ $pastcontest->reward }}</li>
                        </ul>
                    </div>
                @endforeach
            </div>
            <!-- Pagination
                      ============================================= -->
            <div class="row mb-3">
                <div class="col-12" align="center">
                    <button class="button button-3d button-rounded button-black" onclick="loadMoreData()">Load more</button>
                </div>
            </div>
            <!-- .pager end -->
        </div>

    </div>
    @endif
@endsection
@section('script')
    <script>
        var pageNumber = 2;
        function loadMoreData(){
            $.ajax({
                type : 'GET',
                url: "prize?page=" +pageNumber,
                success : function(data){
                    pageNumber +=1;
                    if(data.length == 0){
                        // :( no more articles
                    }else{
                        $('#posts').append(data.html);
                    }
                },error: function(data){
                    console.log('fail to load more event.');
                },
            })
        }
    </script>
@endsection
