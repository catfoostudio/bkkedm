
@foreach($events as $event)
    <div class="entry clearfix">
        <div class="entry-image" style="margin-bottom: 10px;">
            <a href="{{ url('event/'.$event->id) }}">
                <img class="image_fade" style="object-fit: cover"
                                                          src="{{ asset('images/events/posters/'.$event->poster_image) }}" alt="Image"></a>
        </div>
        <div class="entry-title" style="height: 51px;overflow: hidden;">
            <h2><a href="{{ url('event/'.$event->id) }}">{{$event->name}}</a></h2>
        </div>
        <ul class="iconlist" style="font-size: 12px;margin-bottom: 10px;">
            <li><i class="icon-time"></i>{{ \Carbon\Carbon::parse($event->eventdate)->format('dS M Y') }}</li>
            <li><i class="icon-map-marker"></i>{{ $event->location }}</li>
        </ul>
    </div>
@endforeach